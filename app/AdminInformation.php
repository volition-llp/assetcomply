<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminInformation extends Model
{
    protected $table= 'admin_information';

    protected $fillable = ['name','email','ph','username'];
}
