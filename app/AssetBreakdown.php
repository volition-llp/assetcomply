<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetBreakdown extends Model
{
    protected $table= 'asset_breakdown';
}
