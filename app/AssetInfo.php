<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetInfo extends Model
{
   protected function serializeDate(\DateTimeInterface $date) {
        return $date->getTimestamp();
   }
   
   protected $table='asset_info';

   protected $fillable = [ 
                            'sub_category',
                            'category',
                            'company',
                            'asset_id',
                            'asset_type',
                            'asset_name',
                            'brand',
                            'serial_number',
                            'model',
                            'description',
                            'warranty_date',
                            'condition',
                            'vendor_id',
                            'po_number',
                            'invoice_number',
                            'invoice_date',
                            'purchase_price',
                            'purchase_date',
                            'capitalization_price',
                            'end_of_life',
                            'depreciation_rate',
                            'depreciation_method',
                            'depreciation_for_the_year',
                            'total_depreciation',
                            'ownership',
                            'capitalization_date',
                            'scrap_value',
                            'department' ,
                            'location',
                            'units',
                            'status',
                            'emp_id',
                            'date'
                        ];

}