<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetMovementHistory extends Model
{
    protected function serializeDate(\DateTimeInterface $date) {
        return $date->getTimestamp();
   }
    protected $table = "asset_movement_history"; 
}
