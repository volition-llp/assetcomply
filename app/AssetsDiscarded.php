<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetsDiscarded extends Model
{
    protected $table='asset_discarded';
}
