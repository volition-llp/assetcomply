<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AssetDiscardedListExport implements FromCollection, WithHeadings
{
    

    protected $company_id;
    protected $city;
    protected $state;
    protected $country;
    protected $from_date;
    protected $to_date;
    protected $discarded_by;
    protected $location_id;

    public function __construct($company_id, $city, $state, $country, $from_date, $to_date,
                                $discarded_by,
                                $location_id) 
    {
        $this->company_id = $company_id;
        $this->city = $city;
        $this->state = $state;
        $this->country = $country;
        $this->from_date = $from_date;
        $this->to_date = $to_date;
        $this->discarded_by = $discarded_by;
        $this->location_id = $location_id;
        
    }

    public function headings(): array {
        return [          
                            'Asset Id',
                            'Asset Name',
                            'Brand',
                            'Model',
                            'Serial Number',
                            'Category',
                            'Sub Category',
                            'City',
                            'State',
                            'Country',
                            'Location Name',
                            'Discarded Date',
                        ];
      }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {   
        $selected_cols = [  
                            'asset_info.asset_id',
                            'asset_info.asset_name',
                            'asset_info.brand',
                            'asset_info.model',
                            'asset_info.serial_number',
                            'categories.name as category',
                            'subcat.name as sub_category',
                            'locations.city',
                            'locations.state',
                            'locations.country',
                            'locations.location_name',
                            'asset_discarded.date'
                            ];

        $asset_data = $this->getDataFromDB($selected_cols,
                            $this->company_id,
                            $this->city,
                            $this->state,
                            $this->country,
                            $this->from_date,
                            $this->to_date,
                            $this->discarded_by,
                            $this->location_id
                        );
                             

            return $asset_data;
      

    }

    public function getDataFromDB($selected_cols,$company_id, $city, $state, $country, $from_date, 
                                    $to_date,
                                    $discarded_by,
                                    $location_id) {

        try{
           
            $asset_data = DB::table('asset_discarded')
                                  ->select($selected_cols)
                                  ->where('asset_discarded.company_id',$company_id)
                                  ->where(function($query) use ($location_id){
                                           if($location_id != ''){
                                               $query->where('asset_discarded.location_id',$location_id);
                                           }
                                  })
                                  ->where(function($query) use ($discarded_by){
                                           if($discarded_by != ''){
                                               $query->where('asset_discarded.discarded_by',$discarded_by);
                                           }
                                  })
                                  ->where(function($query) use ($city){
                                           if($city != ''){
                                               $query->where('locations.city','LIKE','%'.$city.'%');
                                           }
                                  })
                                  ->where(function($query) use ($state){
                                           if($state != ''){
                                               $query->where('locations.state','LIKE','%'.$state.'%');
                                           }
                                  })
                                  ->where(function($query) use ($country){
                                           if($country != ''){
                                               $query->where('locations.country','LIKE','%'.$country.'%');
                                           }
                                  })
                                  ->where(function($query) use ($from_date,$to_date){
                                    if($from_date != '' && $to_date != ''){
                                        $query->whereBetween('asset_discarded.date',[$from_date,$to_date]);
                                    }
                                  })
                                  ->join('asset_info','asset_info.asset_id','=','asset_discarded.asset_id')
                                  ->join('categories','categories.category_id','=','asset_info.category')
                                  ->leftJoin('categories as subcat','subcat.category_id','=','asset_info.sub_category')
                                  ->join('locations','locations.loc_id','=','asset_discarded.location_id')
                                  ->orderBy('asset_discarded.date','desc')
                                  ->get();

            return $asset_data;

          } catch (\Exception $e) {
            Log::critical($e->getMessage());
            
          }
           
    }
}
