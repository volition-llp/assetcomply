<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AssetBreakdownListExport implements FromCollection, WithHeadings
{
    

  
    protected $company_id;
    protected $asset_filters_arr;
    protected $location_filters_arr;
    protected $location_id;

    public function __construct($company_id, $asset_filters_arr, $location_filters_arr, $location_id) 
    {
        $this->company_id = $company_id;
        $this->asset_filters_arr = $asset_filters_arr;
        $this->location_filters_arr = $location_filters_arr;
        $this->location_id = $location_id;
        
    }

    public function headings(): array {
        return [          
                            'Asset Id',
                            'Asset Name',
                            'Brand',
                            'Serial Number',
                            'Location Name',
                            'City',
                            'State',
                            'Country',
                            'Warranty Date',
                            'Vendor',
                        ];
      }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {   
        $selected_cols = ['asset_breakdown.asset_id',
                                    'asset_info.asset_name',
                                    'asset_info.brand',
                                    'asset_info.serial_number',
                                    'locations.location_name',
                                    'locations.city',
                                    'locations.state',
                                    'locations.country',
                                    'asset_info.warranty_date',
                                    'vendors.vendor_name',

                                    ];

        $asset_maintenance_data = $this->getDataFromDB($selected_cols,
                            $this->company_id,
                            $this->asset_filters_arr,
                            $this->location_filters_arr,
                            $this->location_id
                        );
                             

            return $asset_maintenance_data;
      

    }

    public function getDataFromDB($selected_cols,$company_id, $asset_filters_arr, $location_filters_arr, $location_id) {

        try{
           
            $asset_maintenance_data = DB::table('asset_breakdown')
                                                        ->join('asset_info', 'asset_info.asset_id','=','asset_breakdown.asset_id')
                                                        ->join('locations', 'locations.loc_id','=','asset_breakdown.location_id')
                                                        ->join('vendors', 'vendors.id','=','asset_info.vendor_id')
                                                        ->select($selected_cols)
                                                        ->where('asset_breakdown.company_id',$company_id)
                                                        ->where(function($query) use ($location_id){
                                                            if($location_id != ''){
                                                                $query->where('asset_breakdown.location_id',$location_id);
                                                            }
                                                            
                                                        })
                                                        ->whereNested(function($query) use ($asset_filters_arr) {
                                                            foreach ($asset_filters_arr as $key => $value)
                                                                {
                                                                    if($value != ''){
                                                                        $query->where('asset_info.'.$key,'LIKE','%'.$value.'%');
                                                                    }
                                                                }

                                                        })
                                                        ->whereNested(function($query) use ($location_filters_arr) {
                                                            foreach ($location_filters_arr as $key => $value)
                                                                {
                                                                    if($value != ''){
                                                                        $query->where('locations.'.$key,'LIKE','%'.$value.'%');
                                                                    }
                                                                }

                                                        })
                                                        ->orderBy('asset_breakdown.created_at','desc')
                                                        ->get();

            return $asset_maintenance_data;

          } catch (\Exception $e) {
            Log::critical($e->getMessage());
            
          }
           
    }
}
