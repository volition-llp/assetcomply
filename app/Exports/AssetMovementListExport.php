<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AssetMovementListExport implements FromCollection, WithHeadings
{
    

    protected $company_id;
    protected $movement_by;
    protected $from_date;
    protected $to_date;

    public function __construct($company_id,$movement_by,$from_date,$to_date) 
    {
        $this->company_id = $company_id;
        $this->movement_by = $movement_by;
        $this->from_date = $from_date;
        $this->to_date = $to_date;
        
    }

    public function headings(): array {
        return [            
                            'From Location',
                            'To Location',
                            'Asset Id',
                            'Asset Name',
                            'Remarks',
                            'Date',
                            'Email Notification(Status)',
                            'Movement By',
                            'Vendor Name',
                            'Category',
                            'Sub Category',
                            'Brand',
                            'Model',
                            'Warranty Date',
                            'Purchase Price',
                            'Purchase Date',
                            'Capitalization Price',
                            'Capitalization Date',
                            'Depreciation Method',
                            'Depreciation Rate',
                            'Depreciation for the Year',
                            'Total Depreciation',
                            'Sub User ID',
                        ];
      }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {   
        $selected_cols = [
                                'locations.location_name as from_location',
                                'subloc.location_name as to_location',
                                'asset_info.asset_id',
                                'asset_info.asset_name',
                                'asset_movement_history.remarks',
                                'asset_movement_history.date',
                                'asset_movement_history.email_notification',
                                'asset_movement_history.movement_by',
                                'vendors.vendor_name',
                                'categories.name as category',
                                'subcat.name as sub_category',
                                'asset_info.brand',
                                'asset_info.model',
                                'asset_info.warranty_date',
                                'asset_info.purchase_price',
                                'asset_info.purchase_date',
                                'asset_info.capitalization_price',
                                'asset_info.capitalization_date',
                                'asset_info.depreciation_method',
                                'asset_info.depreciation_rate',
                                'asset_info.depreciation_for_the_year',
                                'asset_info.total_depreciation',
                                'asset_info.emp_id',
                            

                            ];

                            $asset_data = $this->getDataFromDB($selected_cols,$this->company_id,$this->movement_by,$this->from_date,$this->to_date);
                             

            return $asset_data;
      

    }

    public function getDataFromDB($selected_cols,$company_id,$movement_by,$from_date,$to_date) {

        try{
           
            $asset_data = DB::table('asset_movement_history')->where('asset_movement_history.company_id',strtolower($company_id))
                                    ->join('asset_info' , 'asset_info.asset_id', '=', 'asset_movement_history.asset_id')
                                    ->join('categories' , 'categories.category_id', '=', 'asset_info.category')
                                    ->leftJoin('categories as subcat' , 'subcat.category_id', '=', 'asset_info.sub_category')
                                    ->join('vendors' , 'vendors.id', '=', 'asset_info.vendor_id')
                                    ->join('locations' , 'locations.loc_id', '=', 'asset_movement_history.from_location')
                                    ->join('locations as subloc' , 'subloc.loc_id', '=', 'asset_movement_history.to_location')
                                    ->where(function($query) use ($from_date,$to_date){
                                        if($from_date != '' && $to_date != ''){
                                     
                                         $query->whereBetween('asset_movement_history.date', [$from_date, $to_date]);
                                     
                                        } 
                                         
                                     })
                                    ->where(function($query) use ($movement_by){
                                    if($movement_by != ''){
                                 
                                     $query->where('asset_movement_history.movement_by', $movement_by);
                                 
                                    } 
                                     
                                 })
                                ->select($selected_cols)
                                ->orderBy('asset_movement_history.created_at', 'desc')
                                ->get();

            return $asset_data;

          } catch (\Exception $e) {
            Log::critical($e->getMessage());
            
          }
           
    }
}
