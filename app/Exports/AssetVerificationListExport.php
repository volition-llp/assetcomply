<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AssetVerificationListExport implements FromCollection, WithHeadings
{
    
    protected $company_id;
    protected $city;
    protected $state;
    protected $country;
    protected $from_date;
    protected $to_date;
    protected $asset_map_uuid;
    protected $location_id;

    public function __construct(  $company_id,
                                        $city,
                                        $state,
                                        $country,
                                        $from_date,
                                        $to_date,
                                        $asset_map_uuid,
                                        $location_id) 
    {
        $this->company_id = $company_id;
        $this->city = $city;
        $this->state = $state;
        $this->country = $country;
        $this->from_date = $from_date;
        $this->to_date = $to_date;
        $this->asset_map_uuid = $asset_map_uuid;
        $this->location_id = $location_id;
        
    }

    public function headings(): array {
        return [            
                            'Asset ID',
                            'Asset Name',
                            'Category',
                            'Sub Category',
                            'City',
                            'State',
                            'Country',
                            'Site/Location name',
                            'Date',
                            'Verified Status'
                        ];
      }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {   
        $selected_cols = [  
                            'asset_info.asset_id',
                            'asset_info.asset_name',
                            'categories.name as category',
                            'subcat.name as sub_category',
                            'locations.city',
                            'locations.state',
                            'locations.country',
                            'locations.location_name',
                            'assets_verification.verified_status',
                            'assets_verification.dt'
                ];

                        $asset_data = $this->getDataFromDB($selected_cols,
                                                            $this->company_id,
                                                            $this->city,
                                                            $this->state,
                                                            $this->country,
                                                            $this->from_date,
                                                            $this->to_date,
                                                            $this->asset_map_uuid,
                                                            $this->location_id);
                             

            return $asset_data;
      

    }

    public function getDataFromDB($selected_cols,$company_id,
                                                $city,
                                                $state,
                                                $country,
                                                $from_date,
                                                $to_date,
                                                $asset_map_uuid,
                                                $location_id) {

        $asset_data = DB::table('assets_verification')
                                ->select($selected_cols)
                                ->where('assets_verification.company_id',$company_id)
                                ->where(function($query) use ($location_id){
                                        if($location_id != ''){
                                            $query->where('assets_verification.location_id',$location_id);
                                        }
                                })
                                ->where(function($query) use ($asset_map_uuid){
                                        if($asset_map_uuid != ''){
                                            $query->where('assets_verification.asset_map_id',$asset_map_uuid);
                                        }
                                })
                                ->where(function($query) use ($city){
                                        if($city != ''){
                                            $query->where('locations.city','LIKE','%'.$city.'%');
                                        }
                                })
                                ->where(function($query) use ($state){
                                        if($state != ''){
                                            $query->where('locations.state','LIKE','%'.$state.'%');
                                        }
                                })
                                ->where(function($query) use ($country){
                                        if($country != ''){
                                            $query->where('locations.country','LIKE','%'.$country.'%');
                                        }
                                })
                                ->where(function($query) use ($from_date,$to_date){
                                if($from_date != '' && $to_date != ''){
                                    $query->whereBetween('assets_verification.dt',[$from_date,$to_date]);
                                }
                                })
                                ->join('asset_info','asset_info.asset_id','=','assets_verification.asset_id')
                                ->join('categories','categories.category_id','=','asset_info.category')
                                ->leftJoin('categories as subcat','subcat.category_id','=','asset_info.sub_category')
                                ->join('locations','locations.loc_id','=','assets_verification.location_id')
                                ->orderBy('assets_verification.created_at','desc')
                                ->get();
           

            return $asset_data;
    }
}
