<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;

class AssettypeListExport implements FromCollection, WithHeadings, ShouldAutoSize, 
WithTitle, 

WithEvents
{
    public function headings(): array {
        return [            
                            'AssetType ID',
                            'Name'
                        ];
      }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {   
        $selected_cols = [  
                            'asset_type.asset_type_id',
                            'asset_type.name'
                        ];

                        $locations_data = $this->getDataFromDB($selected_cols);
                             

            return $locations_data;
           
      

    }

    public function getDataFromDB($selected_cols) {

        $locations_data = DB::table('asset_type')
                                    ->select($selected_cols)
                                    ->where('delete_status',0)
                                    ->get();
           

            return $locations_data;
    }

    public function title(): string
    {   
        return "Asset Type";
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:B1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            }
        ];
    }


}
