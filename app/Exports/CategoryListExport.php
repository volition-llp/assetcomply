<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets; 
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;




class CategoryListExport implements FromCollection, ShouldAutoSize, WithHeadings, WithTitle ,WithEvents
{
    
    public function headings(): array { 
        return [            
                            'Category ID',
                            'Category Name',
                            'Parent ID',
                            'Asset Type ID',
                            'Asset Type Name'
                        ];
      }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {   
        $selected_cols = [  
                            'categories.category_id',
                            'categories.name',
                            'categories.parent_id',
                            'categories.asset_type_id',
                            'asset_type.name as asset_type_name'
                        ];

                        $categories = $this->getDataFromDB($selected_cols);
                             

            return $categories;
      

    }

    public function getDataFromDB($selected_cols) {

        $categories_data = DB::table('categories')
                                    ->select($selected_cols)
                                    ->join('asset_type','categories.asset_type_id', '=', 'asset_type.asset_type_id')
                                    ->get();
           

            return $categories_data;
    }

    public function title(): string
    {   
        return "Category";
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:E1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            }
        ];
    }


}
