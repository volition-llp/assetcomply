<?php

namespace App\Exports;

use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\depriciation;


class DepreciationMethodsListExport implements FromCollection, WithHeadings, ShouldAutoSize,
WithTitle,

WithEvents
{
    
    public function headings(): array {
        return [            
                            'Depreciation Name',
                        ];
      }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {   
        $selected_cols = [  
                            'depriciation.depriciation_name'
                            
                        ];

                        $locations_data = $this->getDataFromDB($selected_cols);
                             

            return $locations_data;
           
      

    }

    public function getDataFromDB($selected_cols) {

        $locations_data = DB::table('depriciation')
                                    ->select($selected_cols)
                                    ->get();
           

            return $locations_data;
    }

    public function title(): string
    {   
        return "Depreciation";
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            }
        ];
    }
}
