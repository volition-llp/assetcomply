<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;

class LocationListExport implements FromCollection, WithHeadings
{
    
    protected $location_id;
    protected $company_id;

    public function __construct($location_id, $company_id) 
    {
        $this->location_id = $location_id;
        $this->company_id = $company_id;
        
    }

    public function headings(): array {
        return [            
                            'Location ID',
                            'Site',
                            'City',
                            'State',
                            'Country'
                        ];
      }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {   
        $selected_cols = [  
                            'locations.loc_id',
                            'locations.location_name',
                            'locations.city',
                            'locations.state',
                            'locations.country',
                        ];

                        $locations_data = $this->getDataFromDB($selected_cols,$this->company_id,$this->location_id);
                             

            return $locations_data;
      

    }

    public function getDataFromDB($selected_cols,$company_id,$location_id) {

        $locations_data = DB::table('locations')
                                    ->select($selected_cols)
                                    ->where('company_id',strtolower($company_id))
                                    ->where('delete_status',0)
                                    ->where(function($query) use ($location_id){
                                        if($location_id != ''){
                                            $query->where('loc_id',$location_id);
                                        }
                                        
                                    })
                                    ->get();
           

            return $locations_data;
    }
}
