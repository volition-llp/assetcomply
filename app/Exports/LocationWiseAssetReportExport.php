<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;

class LocationWiseAssetReportExport implements FromCollection, WithHeadings
{
    
    protected $location_id;
    protected $company_id;
    protected $from_date;
    protected $to_date;
    protected $emp_id;
    protected $country;
    protected $city;
    protected $state;

    public function __construct($location_id,
                                $company_id,
                                $from_date,
                                $to_date,
                                $emp_id,
                                $country,
                                $city,
                                $state) 
    {
        $this->location_id = $location_id;
        $this->company_id = $company_id;
        $this->from_date = $from_date;
        $this->to_date = $to_date;
        $this->emp_id = $emp_id;
        $this->country = $country;
        $this->city = $city;
        $this->state = $state;
        
    }

    public function headings(): array {
        return [            'Asset Name',
                            'Asset id',
                            'Asset Type',
                            'Category',
                            'Sub Category',
                            'Location',
                            'City',
                            'State',
                            'Country',
                            'Brand',
                            'Serial Number',
                            'Model',
                            'Unit',
                            'Description',                                           
                            'Warranty Date',                                           
                            'Condition',                                           
                            'Vendor Name',                                           
                            'PO Number',                                           
                            'Invoice Number',                                           
                            'Invoice Date',                                           
                            'Purchase Price',                                           
                            'Purchase Date',                                           
                            'Capitalization Price',                                           
                            'End of Life',                                           
                            'Depreciation Method',                                           
                            'Depreciation for the Year',                                           
                            'Total Depreciation',                                           
                            'Capitalization Date',                                           
                            'Scrap Value',                                           
                            'Department'
                        ];
      }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {   
        $selected_cols = [  'asset_info.asset_name',
                            'asset_info.asset_id',
                            'asset_type.name as asset_type_name',
                            'categories.name as category',
                            'subcat.name as sub_category',
                            'locations.location_name',
                            'locations.city',
                            'locations.state',
                            'locations.country',
                            'asset_info.brand',
                            'asset_info.serial_number',
                            'asset_info.model',
                            'asset_info.unit',
                            'asset_info.description',                                           
                            'asset_info.warranty_date',                                           
                            'asset_info.condition',                                           
                            'vendors.vendor_name',                                           
                            'asset_info.po_number',                                           
                            'asset_info.invoice_number',                                           
                            'asset_info.invoice_date',                                           
                            'asset_info.purchase_price',                                           
                            'asset_info.purchase_date',                                           
                            'asset_info.capitalization_price',                                           
                            'asset_info.end_of_life',                                           
                            'asset_info.depreciation_method',                                           
                            'asset_info.depreciation_for_the_year',                                           
                            'asset_info.total_depreciation',                                           
                            'asset_info.capitalization_date',                                           
                            'asset_info.scrap_value',                                           
                            'asset_info.department'
                        ];

                        $asset_data = $this->getDataFromDB($selected_cols,
                                                            $this->location_id,
                                                            $this->company_id,
                                                            $this->from_date,
                                                            $this->to_date,
                                                            $this->emp_id,
                                                            $this->country,
                                                            $this->city,
                                                            $this->state);
                             

            return $asset_data;
      

    }

    public function getDataFromDB($selected_cols,
                                    $location_id,
                                    $company_id,
                                    $from_date,
                                    $to_date,
                                    $emp_id,
                                    $country,
                                    $city,
                                    $state) {

        $asset_data = DB::table('asset_info')
                                    ->where('asset_info.company',strtolower($company_id))
                                    ->join('locations', 'asset_info.location', '=', 'locations.loc_id')
                                    ->join('categories', 'asset_info.category', '=', 'categories.category_id')
                                    ->leftJoin('categories AS subcat' , 'asset_info.sub_category', '=', 'subcat.category_id')
                                    ->join('vendors', 'asset_info.vendor_id', '=', 'vendors.id')
                                    ->join('asset_type', 'asset_info.asset_type', '=', 'asset_type.asset_type_id')
                                    ->select($selected_cols)
                                    ->where(function($query) use ($from_date,$to_date){
                                        if($from_date != '' && $to_date != ''){  
                                                $query->whereBetween('asset_info.date', [$from_date, $to_date]);
                                            }
                                    })
                                    ->where(function($query) use ($emp_id){
                                        if($emp_id != ''){
                                            $query->where('asset_info.emp_id',$emp_id);
                                        }
                                    })
                                    ->where(function($query) use ($location_id){
                                        if($location_id != ''){
                                        $query->where('asset_info.location',$location_id);
                                        }
                                }) 

                                ->where(function($query) use ($country){
                                    if($country != ''){
                                    $query->where('locations.country',$country);
                                    }
                            })
                            ->where(function($query) use ($city){
                                if($city != ''){
                                    $query->where('locations.city',$city);
                                }
                            }) 
                            ->where(function($query) use ($state){
                                if($state != ''){
                                $query->where('locations.state',$state);
                                }
                            })   
                            ->orderBy('asset_info.date', 'desc')
                            ->get();

            return $asset_data;
    }
}
