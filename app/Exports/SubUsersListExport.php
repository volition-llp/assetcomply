<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SubUsersListExport implements FromCollection, WithHeadings
{
    

    protected $company_id;
    protected $location_id;
    protected $usertype;

    public function __construct($company_id,$location_id,$usertype) 
    {
        $this->company_id = $company_id;
        $this->location_id = $location_id;
        $this->usertype = $usertype;
        
    }

    public function headings(): array {
        return [            
                            'Username',
                            'Name',
                            'Address',
                            'Phone Number',
                            'Email',
                            'Location',
                            'City',
                            'State',
                            'Country',
                            'UserType'
                        ];
      }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {   
        $selected_cols = [  
                            'sub_users.username',
                            'sub_users.name',
                            'sub_users.address',
                            'sub_users.phone_number',
                            'sub_users.email',
                            'locations.location_name',
                            'locations.city',
                            'locations.state',
                            'locations.country',
                            'sub_users.user_type',
                        ];

                        $sub_user_list = $this->getDataFromDB($selected_cols,$this->company_id,$this->location_id,$this->usertype);
                             

            return $sub_user_list;
      

    }

    public function getDataFromDB($selected_cols,$company_id,$location_id,$usertype) {

        $sub_user_list = DB::table('sub_users')
                                            ->select($selected_cols)
                                            ->where('sub_users.company',strtolower($company_id))

											->where(function($query) use($usertype) {
                                                   if($usertype != ''){
                                                    $query->where('sub_users.user_type',$usertype);
                                                   }
                                            })
											->where(function($query) use($location_id) {
                                                   if($location_id != ''){
                                                    $query->where('sub_users.location',$location_id);
                                                   }
                                            })

											->join('locations', 'sub_users.location', '=', 'locations.loc_id')
											->orderBy('sub_users.created_at','desc')
											->get();
           

            return $sub_user_list;
    }
}
