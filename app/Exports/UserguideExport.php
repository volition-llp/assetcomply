<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Exports\CategoryListExport;
use App\Exports\DepreciationMethodsListExport;
use App\Exports\AssettypeListExport;
use App\Exports\VendorsListExport;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;


class UserguideExport implements WithMultipleSheets
{
    /**
    * @return \Illuminate\Support\Collection
    */
    /*public function collection()
    {
        //
    }*/

    private $company_id;
    private $added_by;

    public function __construct($company_id,$added_by)
    {
        $this->company_id = $company_id;
        $this->added_by = $added_by;
    }

    public function sheets(): array
    {	

    	/*$company_id;
    	$added_by;*/
    	

        $sheets = [new CategoryListExport,new DepreciationMethodsListExport,new AssettypeListExport,new VendorsListExport($this->company_id,$this->added_by)];

        

        return $sheets;
    }


}
