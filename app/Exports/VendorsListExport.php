<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;


class VendorsListExport implements FromCollection, ShouldAutoSize, WithHeadings, WithTitle ,WithEvents
{
    

    protected $company_id;
    protected $added_by;

    public function __construct($company_id,$added_by) 
    
    {
        $this->company_id = $company_id;
        $this->added_by = $added_by;
        
    } 

    public function headings(): array {
        return [            
                            'Vendor ID',
                            'Vendor Name',
                            'Address',
                            'Primary Contact Person',
                            'Primary Contact Number',
                            'Email',
                            'Country',
                            'State',
                            'City',
                            'Postal Code',
                            'PAN',
                            'GST'
                        ];
      }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {   
        $selected_cols = [  
                            'id',
                            'vendor_name',
                            'address',
                            'primary_person',
                            'primary_number',
                            'email',
                            'country',
                            'state',
                            'city',
                            'postal_code',
                            'pan',
                            'gst_number',
                        ];

                        $locations_data = $this->getDataFromDB($selected_cols,$this->company_id,$this->added_by);
                             

            return $locations_data;
      

    }

    public function getDataFromDB($selected_cols,$company_id,$added_by) {

        $locations_data = DB::table('vendors')
                                    ->select($selected_cols)
                                    ->where('company_id',strtolower($company_id))
                                    ->where('added_by',$added_by)
                                    ->get();
           

            return $locations_data;
    }

    public function title(): string
    {   
        return "Vendors";
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:L1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            }
        ];
    }


}
