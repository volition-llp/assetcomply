<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\AdminInformation;
use Illuminate\Support\Facades\Hash;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;


class AdminController extends Controller
{
    public function adminRegistration(Request $request) 
        {
            AdminInformation::create([
                'name' => 'Asset Comply',
                'email' => 'asset@gmail.com',
                'ph' => 1234569875,
                'username' => 'asset_admin'
            ]);

            User::create([
                'name' => 'Asset Admin',
                'email' => 'asset_admin',
                'password' => Hash::make('admin123')
            ]);

            return response()->json(['status' => "1000",'message' =>"Admin is on"]);
        }
    
    public function adminLogin(Request $request) 
        {
            $username=$request->input('username');
		$password=$request->input('password');


		 $validator = Validator::make($request->all(), [
            		    'username' =>'required',
                    'password' =>'required',
			]);

            
        if ($validator->fails()) {
    
    	          	return response()->json(['status' => "901",'message' => $validator->messages()]);
          	
       		}else{

                $login_check = User::where('email', $username)
                                      ->first();

                if(!empty($login_check))
                {
                      if(Hash::check($password,$login_check->password))
                      {
                                $check_proper_username = AdminInformation::where('username',$username)
                                                            ->first();
                                
                                if(!empty($check_proper_username)){

                                     $token = $login_check->createToken('Admin Auth Token')->accessToken;

                                     Log::info("Logged In ". $username);

                                     return response()->json(['status' => "900",
                                                                'message' => "login successfull",
                                                                'token' => "Bearer ".$token,
                                                                'username'=>$username]);
                                }else{

                                     return response()->json(['status' => "906",'message' => "Username & Password is invalid"]); 
                                }
                                 

                        }
                        else
                        {
                            return response()->json(['status' => "906",'message' => "Wrong username or password"]); 
                        }

                      
                  }else{

                       return response()->json(['status' => "906",'message' => "Username & Password is invalid"]); 
                  }
                }
            
        }

    
}
