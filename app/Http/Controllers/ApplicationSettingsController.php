<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 

use App\Locations;
use App\Department;
use App\Unit;
use App\VendorCustomer;
use App\User;
use App\Userdetails;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use App\SubUser;
use App\SubUserSettings;
use Mail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\VerifyUser;
use App\Mail\SubUserRegisterMail;
use Carbon\Carbon;
use App\UserType;
use App\QRBarCodeFields;
use App\SysConfig;
use	App\UserTypeAccess;

class ApplicationSettingsController extends Controller
{
			
			/*Asset Location settings for a company*/

			public function setLocationForCompany(Request $request)
					{
						$validator = Validator::make($request->all(), [
									'loc_id' =>'required|unique:locations,loc_id',
									'state' => 'required',
									'city'  => 'required',
									'country'  => 'required',
									'location_name' => 'required',
									'company_id' => 'required|exists:userdetails,company', 
									'username' => 'required|exists:userdetails,username',                 
						]);
		
							if($validator->fails()) {
						
								//pass validator errors as errors object for ajax response
								$message=$validator->errors();

									return response()->json(['status'=>"901",'message' => $message]);
							}

							else{
								try{  

									$check_both_compny_username = Userdetails::where('company',strtolower($request->input('company_id')))
																			->where('username',$request->input('username'))
																			->first();
										if(empty($check_both_compny_username))
										{
											return response()->json(['status'=>"901",'message' => "Mismatched Username and Company"]);


										}else{

											$address = new Locations();
											$address->loc_id=$request->input('loc_id');
											$address->state=$request->input('state');
											$address->city=$request->input('city');
											$address->country = $request->input('country');
											$address->location_name=$request->input('location_name');
											$address->company_id= strtolower($request->input('company_id')); 
											$address->added_by=$request->input('username');
											$address->delete_status= 0;
											$address->save();

											Log::info('location saved');

											return response()->json(['status'=>"900",'message' => "Address saved"]);
										}
									

								}catch (\Exception $e) {

									Log::error($e->getMessage());
									return response()->json(['status' => "902",'message' => "Exception while saving Location"],422);
								}

								

							}


				}

		//  Location List
		public function listLocationsByCompany(Request $request)
		{
			$validator = Validator::make($request->all(), [
				'company_id' => 'required|exists:userdetails,company',
				'username' => 'required'                
			]);

			$search_filters =  $request->input('search_filters');
			$company_id =$request->input('company_id');
			$username =$request->input('username');

				if($validator->fails()) {
			
					$message=$validator->errors();
					// $validator->messages();
					return response()->json(['status'=>"901",'message' => $message]);
				}

			$locations_data = Locations::where('company_id', strtolower($company_id))
										->whereNested(function($query) use ($search_filters) {
												foreach ($search_filters as $key => $value)
													{
														if($value != ''){
															$query->where($key,'LIKE','%'.$value.'%');
														}
													}
										}, 'and');

			$locations_data=$locations_data->where('delete_status',0)
											->where('added_by',$username)
											->orderBy('created_at', 'desc')
											->paginate(10);

			return response()->json(['status'=>"900",'data' => $locations_data]);
		}

		public function listLocationsByCompanyWithoutPagination($company_id)
		{
			$locations_data = Locations::where('company_id', strtolower($company_id))
											->where('delete_status',0)
											->get();
											
			return response()->json(['status'=>"900",'data' => $locations_data]);
		}

		//  Location Delete

		public function deleteSingleLocation($location_id,$company_id)
		{
			try{

			$loc_validate = Locations::where('loc_id', $location_id)
							->where('company_id', strtolower($company_id))
							->first();

					if(empty($loc_validate)) {
						return response()->json(['status'=>"901",'message' => "This is not a proper location"]);
					}else{

						$check_location_dependency_status = $this->checkLocationDependency($location_id,$company_id);

					if($check_location_dependency_status){

						return response()->json(['status'=>"901",
													'message' => "Location already in used. First delete those data"
												]);
					}
						$location_soft_delete = Locations::where('loc_id', $location_id)
												->where('company_id', strtolower($company_id))
												->delete();

						return response()->json(['status'=>"900",
													'message' => "Location deleted",
													'location' => $location_id]);
		         	}

				}catch (\Exception $e) {
							Log::error($e->getMessage());
							return response()->json(['status' => "902",
									'message' => "Exception while saving Location"],422);
			    }
		}

		public function checkLocationDependency($location_id,$company_id) {
			
			$assets_verification_check = \DB::table('assets_verification')
											->where('location_id',$location_id)
											->where('company_id',$company_id)
											->count();

			$asset_discarded_check = \DB::table('asset_discarded')
											->where('location_id',$location_id)
											->where('company_id',$company_id)
											->count();
			$asset_info_check = \DB::table('asset_info')
											->where('location',$location_id)
											->where('company',$company_id)
											->count();
			$asset_maintenance_check = \DB::table('asset_maintenance')
											->where('location_id',$location_id)
											->where('company_id',$company_id)
											->count();
			$asset_movement_history_check = \DB::table('asset_movement_history')
											->where('to_location',$location_id)
											->orWhere('from_location',$location_id)
											->where('company_id',$company_id)
											->count();
			$sub_users_check = \DB::table('sub_users')
											->where('location',$location_id)
											->where('company',$company_id)
											->count();
			$support_ticket_check = \DB::table('support_ticket')
											->where('location',$location_id)
											->where('company',$company_id)
											->count();
			$verified_asset_end_mapping_check = \DB::table('verified_asset_end_mapping')
											->where('location_id',$location_id)
											->where('company_id',$company_id)
											->count();
			$flag =0;
			    if ($assets_verification_check>0 || $asset_discarded_check>0 || $asset_info_check>0
					|| $asset_maintenance_check>0 || $asset_movement_history_check>0 || $sub_users_check>0
					|| $support_ticket_check>0 || $verified_asset_end_mapping_check>0)
				{
					$flag=1;
				}

			return $flag;

		}


		// Location Update 
		public function updateSingleLocation(Request $request)
		{ 
			$validator = Validator::make($request->all(), [
									'location_id' =>'required',
									'company_id' => 'required',
									'state' => 'required',
									'city'  => 'required',
									'location_name' => 'required',              
						    ]);
		
					if($validator->fails()) {
						
						//pass validator errors as errors object for ajax response
						$message=$validator->errors();

						return response()->json(['status'=>"901",'message' => $message]);
					}else{
						
						$check_both_compny_username = Userdetails::where('company',strtolower($request->input('company_id')))
																			->first();
						if(empty($check_both_compny_username))
						{
							return response()->json(['status'=>"901",'message' => "Mismatched Username and Company"]);


						}else{

							$location_id = $request->input('location_id');
							$company_id=$request->input('company_id');
							$country=$request->input('country');
							$state=$request->input('state');
							$city=$request->input('city');
							$location_name=$request->input('location_name');

							$location_update = Locations::where('loc_id', $location_id)
															->where('company_id', $company_id)
																->update(['location_name'=>$location_name,
																		'state'=>$state,
																		'city'=>$city,
																		'country' => $country
																		]);

							return response()->json(['status'=>"900",'message' => "Location updated",'location' => $location_name]);
						}
							
					}

			
		}

		public function addUserTypeAccessForSubUsers(Request $request)
		{
			$validator = Validator::make($request->all(), [
										'user_type' => 'required',
										'company_id' => 'required|exists:userdetails,company',
										'access_list' => 'required'
									]);

			$company_id = strtolower($request->input('company_id'));
			$user_type = $request->input('user_type');
			$access_list = $request->input('access_list');

				if($validator->fails()){ 

					return response()->json(['status' => "901",'message' => $validator->messages()]);
		
				}else{

					    $check_usertype_access_record = UserTypeAccess::where('company_id',$company_id)
																	->where('user_type',$user_type)
																	->first();

							if(empty($check_usertype_access_record)){

								// insert data
							 try{
									foreach($access_list as $access_data)
									{
										$save_user_type_access = new UserTypeAccess();
										$save_user_type_access->company_id = $company_id;
										$save_user_type_access->user_type = $user_type;

										$save_user_type_access->menu_name = strtolower($access_data['menu_name']);
										$save_user_type_access->view = $access_data['view'];
										$save_user_type_access->add = $access_data['add'];
										$save_user_type_access->edit = $access_data['edit'];
										$save_user_type_access->delete = $access_data['delete'];
										$save_user_type_access->export = $access_data['export'];
										$save_user_type_access->save();
									}	

									return response()->json(['status' => "900",
														'message' => "User Access Type is added",
														'data' => array('UserTypeName'=>$user_type)
														]);

								}catch (\Exception $e) {

									Log::error("UserType Access Module Exception: ".$e->getMessage());
									return response()->json(['status' => "902",
																   'message' => 'Internal Server Error'],422);
							   }
								


							}else{

								return response()->json(['status' => "901",
														'message' => "User Access Type is exists"
														]);
							} 
				}


			
		}

		public function getUsertypeAccessSingleRecord(Request $request){
			   $company_id = $request->input('company_id');
			   $usertype = $request->input('usertype');
			   $username = $request->input('username');
			   $menu_name = $request->input('menu_name');

			   $validator = Validator::make($request->all(), [
											'company_id'  =>'required',
											'usertype' =>'required',
											'username' =>'required',
											'menu_name' =>'required'
			   ]);

			   if($validator->fails()) {
				   return response()->json(['status' => "901",'message' => $validator->errors()]);
			   }
			   try{
				$get_usertype_data = UserTypeAccess::where('user_type',$usertype)
													->where('menu_name',$menu_name)
													->where('company_id',$company_id)
													->first();
			   }catch (\Exception $e) {
				//    Log::critical("Error in Single record from UserAccesstype: " . $e->getMessage());
				   return response()->json(['status' => "902",'message' => "Internal Server Error"]);
			   }
			  
					return response()->json(['status' => "900",'message' => "Success",'data'=>$get_usertype_data]);
		}

		public function updateUserTypeAccessForSubUsers(Request $request)
		{
			$validator = Validator::make($request->all(), [
											'user_type' => 'required',
											'company_id' => 'required|exists:userdetails,company',
											'access_list' => 'required'
										]);

				$company_id = strtolower($request->input('company_id'));
				$user_type = $request->input('user_type');
				$access_list = $request->input('access_list');
				$id = $request->input('id');

				if($validator->fails()){

				return response()->json(['status' => "901",'message' => $validator->messages()]);

				}else{

					//update
					try{
						foreach($access_list as $access_data)
						{	
							
							if(!empty($access_data['id'])){	
								$update_user_type_access = UserTypeAccess::where('company_id',$company_id)
																	->where('id',$access_data['id'])
																	->update([

																		'user_type' => strtolower($user_type),
																		'menu_name' => strtolower($access_data['menu_name']),
																		'view'=> $access_data['view'],
																		'add' => $access_data['add'],
																		'edit' => $access_data['edit'],
																		'delete' => $access_data['delete'],
																		'export' => $access_data['export'],
																	]);
							}else{
								$save_user_type_access = new UserTypeAccess();
								$save_user_type_access->company_id = $company_id;
								$save_user_type_access->user_type = $user_type;

								$save_user_type_access->menu_name = strtolower($access_data['menu_name']);
								$save_user_type_access->view = $access_data['view'];
								$save_user_type_access->add = $access_data['add'];
								$save_user_type_access->edit = $access_data['edit'];
								$save_user_type_access->delete = $access_data['delete'];
								$save_user_type_access->export = $access_data['export'];
								$save_user_type_access->save();
							}
							
						}	

						return response()->json(['status' => "900",
											'message' => "User Access Type is updated",
											'data' => array('UserTypeName'=>$user_type)
											]);

					}catch (\Exception $e) {

						Log::error("UserType Access Module Exception: ".$e->getMessage());
						return response()->json(['status' => "902",
													'message' => "Internal Server Error "],422);
				}
			}
		}

		public function listOfUserTypeAccess($company_id)
		{
			$validate_company = Userdetails::where('company',$company_id)->first();

						if(empty($validate_company)){

						    return response()->json(['status' => "901",'message' =>"Company ID is Invalid"]);

						}else{
							$user_type_data = UserTypeAccess::where('company_id',$company_id)
																->get();

							
							$access_menu = array();
							foreach($user_type_data as $utd){

								$access_menu[$utd['user_type']][$utd['menu_name']] = 
													array([
													 "view"=>$utd['view'],	
													 "add"=> $utd['add'],
													 "edit" => $utd['edit'],
													 "delete" => $utd['delete'],
													 "export" => $utd['export']]);

							}
						
							return response()->json(['status' => "900",
							'message' => "Records Found",
							'data' =>$access_menu
							]);
						}


		}

		public function usertypeListOfUserTypeAccess($company_id)
		{
			$validate_company = Userdetails::where('company',$company_id)->first();

						if(empty($validate_company)){

						    return response()->json(['status' => "901",'message' =>"Company ID is Invalid"]);

						}else{
							$user_type_data = UserTypeAccess::select('user_type')
							                                    ->distinct()
							                                    ->where('company_id',$company_id)
																->get();

							
							$access_menus = array();
							foreach($user_type_data as $utd){

								$access_menus[] = $utd['user_type'];

							}
						
							return response()->json(['status' => "900",
							'message' => "Records Found",
							'data' =>$access_menus
							]);
						}


		}

		public function listOfUserTypeAccessWithViewAccess($company_id)
		{
			$validate_company = Userdetails::where('company',$company_id)->first();

						if(empty($validate_company)){

						    return response()->json(['status' => "901",'message' =>"Company ID is Invalid"]);

						}else{

							$user_type_data = UserTypeAccess::where('company_id',$company_id)
							                                    ->where('view',1)
																->get();

							
							$access_menu = array();
							foreach($user_type_data as $utd){

							   
								$access_menu[$utd['user_type']][$utd['menu_name']] = 
													array([
													 "view"=>$utd['view'],	
													 "add"=> $utd['add'],
													 "edit" => $utd['edit'],
													 "delete" => $utd['delete'],
													 "export" => $utd['export']]);
							    
							}
						
							return response()->json(['status' => "900",
							'message' => "Records Found",
							'data' =>$access_menu
							]);
						}


		}
		
		public function listOfUserTypeAccessWithViewAccessUserType($company_id,$user_type)
		{
			$validate_company = Userdetails::where('company',$company_id)->first();

						if(empty($validate_company)){

						    return response()->json(['status' => "901",'message' =>"Company ID is Invalid"]);

						}else{

							$user_type_data = UserTypeAccess::where('company_id',$company_id)
															 ->where('user_type',$user_type)
															 ->get();

							
							// $access_menu = array();
							// foreach($user_type_data as $utd){

							//   if($utd['view'] != 0){
							// 	$access_menu[$utd['user_type']][$utd['menu_name']] = 
							// 						array([
							// 						 "view"=>$utd['view'],	
							// 						 "add"=> $utd['add'],
							// 						 "edit" => $utd['edit'],
							// 						 "delete" => $utd['delete'],
							// 						 "export" => $utd['export']]);
							//   }
							// }
						
							return response()->json(['status' => "900",
							'message' => "Records Found",
							'data' => $user_type_data
							]);
						}
		}

		public function deleteUserTypeAccessForSubUsers(Request $request)
		{
			$validator = Validator::make($request->all(), [ //* Validating on incoming data
										'user_type' => 'required|exists:user_type_access,user_type',
										'company_id' => 'required'
										]);
			

			$user_type =  $request->input('user_type');
			$company_id =  $request->input('company_id');

			$check_subusertype_master = SubUser::where('user_type',$user_type)->get();

			if($check_subusertype_master->isEmpty()){
				
					$usertype_del = UserTypeAccess::where('company_id',$company_id)
											->where('user_type',$user_type)
											->get();
						$array_ids = array();

						foreach($usertype_del as $data){
							$array_ids[] = $data->id;
						}

						$del_utype_data = UserTypeAccess::destroy($array_ids);
						
						return response()->json(['status' => "900",
										'message' => "Records Deleted"
										]);
			}else{
				return response()->json(['status' => "901",
										'message' => "This Usertype already associated with a Sub User!"
										]);
			}
			

		}

		//! create sub-customer

		public function subUserCreation(Request $request)
		{
			$subuser_username = $request->input('subuser_username');
			$company = strtolower($request->input('company'));
			$name = $request->input('name');
			$address = $request->input('address');
			$phone_number = $request->input('phone_number');
			$email = $request->input('email');
			$location = $request->input('location');
			$user_type = $request->input('user_type'); //*branch_user, field_user etc
			$added_by = $request->input('added_by'); //* master client username

			$validator = Validator::make($request->all(), [ //* Validating on incoming data
				'subuser_username' => 'required|unique:sub_users,username|unique:users,email',
				'company' => 'required|exists:userdetails,company',
				'name' => 'required',
				'address' => 'required',
				'phone_number' => 'required|digits:10',
				'email' => 'required|email',
				'location' => 'required|exists:locations,loc_id',
				'added_by' => 'required|exists:userdetails,username',
				'user_type' => 'required|exists:user_type_access,user_type'
			]);

		 
			if($validator->fails()){

				return response()->json(['status' => "901",'message' => $validator->messages()]);

			}else{
						$random = Str::random(10);
						$pass=rand(1,999).$random.rand(1,999);
						$temp_password=str_shuffle($pass);

						try{

						//   $user_username_checking = User::where('email',$username)->first();
						//   $sub_user_username_checking = SubUser::where('username',$username)->first();
						//   $sub_usersetting_username_checking = SubUserSettings::where('username',$username)->first();
						//   $accmn = implode(',',$access_menu);

								$subuser_personal_data = new SubUser();
								$subuser_personal_data->username = $subuser_username;
								$subuser_personal_data->name=$name;
								$subuser_personal_data->address = $address;
								$subuser_personal_data->phone_number = $phone_number;
								$subuser_personal_data->email = $email;
								$subuser_personal_data->location = $location;
								$subuser_personal_data->company = $company;
								$subuser_personal_data->user_type = $user_type;
								$subuser_personal_data->added_by = $added_by;
								$subuser_personal_data->save();


								$user_login_data = new User();
								$user_login_data->name = $name;
								$user_login_data->email = $subuser_username;
								$user_login_data->password = Hash::make($temp_password);
								$user_login_data->save();
								

								\Mail::to($email)->send(new SubUserRegisterMail($subuser_username,$name,$temp_password));

								Log::info("Email send to this ".$email);
								Log::info("Sub User Registration Successfull");
								return response()->json(['status' => "900",'message' => "Registration Successfull & Pending for Email Verification"]);

						}
						catch (\Exception $e) {

							Log::critical($e->getMessage());
									return response()->json(['status' => "902",'message' => "Exception while saving Sub User Details"],422);
						}

			}
		}


		public function listOfSubUsers($username,$company_id)
		{
				
				$validator = Validator::make([$username,$company_id], [ //* Validating on incoming data
							'username' => 'exists:userdetails,username',
							'company_id' => 'exists:userdetails,company'
						]);
				
			if($validator->fails()){

				return response()->json(['status' => "901",'message' => $validator->messages()]);

			}else{
					
				$sub_user_list = SubUser::where('company',strtolower($company_id))
											->where('added_by',$username)
											->orderBy('created_at','desc')
											->paginate(10);
			}
				return response()->json(['status' => "900", 
										'message' => 'Success',
										'data' => $sub_user_list]);
		}

		public function listOfSubUsersLocationWise($location_id,$username,$company_id)
		{
			$validator = Validator::make([$location_id,$username,$company_id], [ //* Validating on incoming data
							'username' => 'exists:userdetails,username',
							'company_id' => 'exists:userdetails,company'
						]);
					if($validator->fails()){

							return response()->json(['status' => "901",'message' => $validator->messages()]);
			 
					}else{
								
							$sub_user_list = SubUser::where('company',strtolower($company_id))
														->where('added_by',$username)
														->where('location',$location_id)
														->join('user_type_access', 'sub_users.user_type', '=', 'user_type_access.user_type')
														->orderBy('sub_users.created_at','desc')
														->paginate(10);
					}
			return response()->json(['status' => "900", 
									'message' => 'Success',
									'data' => $sub_user_list]);
		}

		public function updateSubUsersInfo(Request $request)
		{
				$subuser_username = $request->input('subuser_username');
				$company = strtolower($request->input('company'));
				

				// Editable input section
				$name = $request->input('name');
				$address = $request->input('address');
				$phone_number = $request->input('phone_number');
				$email = $request->input('email');
				$location = $request->input('location');
				$user_type = $request->input('user_type'); 

				
	
				$validator = Validator::make($request->all(), [ //* Validating on incoming data
									'subuser_username' => 'required|exists:sub_users,username',
									'company' => 'required|exists:userdetails,company',
									'name' => 'required',
									'address' => 'required',
									'phone_number' => 'required|digits:10',
									'email' => 'required|email',
									'location' => 'required|exists:locations,loc_id',
									'user_type' => 'required|exists:user_type_access,user_type'
								]);

			
				if($validator->fails()){

					return response()->json(['status' => "901",'message' => $validator->messages()]);

				}else{

					$sub_user_data = SubUser::where('username',$subuser_username)
											->where('company',$company)
											->update([
												'name' => $name,
												'address' => $address,
												'phone_number' => $phone_number,
												'email' => $email,
												'location' => $location,
												'user_type' => $user_type
												]);
					return response()->json(['status' => "900",'message'=> 'Success']);
				}

		}


		public function setFieldsForBarcodeQRcode(Request $request)
		{
			$company_id = $request->input('company_id');
			$fields = $request->input('fields');
			$added_by = $request->input('username');

			
			$validator = Validator::make($request->all(), [ //* Validating on incoming data
							'fields' => 'required',
							'company_id' => 'required|exists:userdetails,company',
							'username' => 'required|exists:userdetails,username'
						]);


				if($validator->fails()){

				return response()->json(['status' => "901",'message' => $validator->messages()]);

				}else{

					try{
						
						if(in_array("asset_id", $fields)){

						
							$check_oldfields_value = QRBarCodeFields::where('company_id',$company_id)
																	->where('added_by',$added_by)
																	->first();
							if(empty($check_oldfields_value))
							{
								$qrbarcode_fields_data = new QRBarCodeFields();
								$qrbarcode_fields_data->fields = $fields;
								$qrbarcode_fields_data->company_id = $company_id;
								$qrbarcode_fields_data->added_by = $added_by;
								$qrbarcode_fields_data->save();

								// $prev_id = $qrbarcode_fields_data->id;

								
								// foreach($fields as $fld){
								// 	$qrbarcode_fields_mapped_data = new QRBarcodeFieldMapping();
								// 	$qrbarcode_fields_mapped_data->qr_barcode_id = $prev_id;
								// 	$qrbarcode_fields_mapped_data->field = $fld;
								// 	$qrbarcode_fields_mapped_data->save();
									
									
								// }
								
								

							}else{

								$qrbarcode_fields_data = QRBarCodeFields::where('company_id',$company_id)
																		->where('added_by',$added_by)
																		->update(['fields'=>$fields]);
							}
								

							return response()->json(['status' => "900",'message'=> 'Success']);
						}else{

							return response()->json(['status' => "902",'message' => "You need to include Asset Id must"]);
						}


						}catch (\Exception $e) {

							Log::error($e->getMessage());
							return response()->json(['status' => "902",'message' => "Exception Occured"],422);
						}
					}
			
		}

		public function getFieldsForBarcodeQRcode(Request $request)
		{
			

			$validator = Validator::make($request->all(), [ //* Validating on incoming data
								'company_id' => 'required|exists:userdetails,company',
								'username' => 'required|exists:userdetails,username'
							]);

				$company_id = $request->input('company_id');
				$added_by = $request->input('username');

					if($validator->fails()){

					return response()->json(['status' => "901",'message' => $validator->messages()]);

					}else{

						try{
							

								$qrbarcode_fields = QRBarCodeFields::where('company_id',$company_id)
																		->where('added_by',$added_by)
																		->first()
																		->fields;

							return response()->json(['status' => "900",'message'=> 'Success',
													'data'=>$qrbarcode_fields]);
						}catch (\Exception $e) {

							Log::error($e->getMessage());
							return response()->json(['status' => "902",'message' => "Exception Occured"],422);
						}
					}
		}


		public function systemConfiguration(Request $request)
		{
			$update_arr = $request->input('update_arr');  
			// "asset_scan_output" 
			// "time_zone" 
			// "title_position_in_scan_output" 
			// "height_barqrcode_output"
			// "width_barqrcode_output" 
			$company_id =$request->input('company_id');

			$validator = Validator::make($request->all(), [ //* Validating on incoming data
				'update_arr' => 'required',
				'company_id' => 'required|exists:userdetails,company',
				'logoimg'=>'nullable|mimes:jpeg,jpg,png|max:10000'
			]);


			if($validator->fails()){

					return response()->json(['status' => "901",'message' => $validator->messages()]);

			}else{

				try{

					$sys_config_check = SysConfig::where('company_id',$company_id)
													->count();
													
						if($sys_config_check == 1){

							$update_latest_setting = SysConfig::where('company_id',$company_id)
																->update($update_arr);

						/**/
						if($request->hasFile('logoimg')) {              
                
                    $logo = $request->file('logoimg');
                    $directory_logo = 'company/logo/';
                    $filename = $company_id .'.'. $logo->getClientOriginalExtension();


                    $path_catimg = $logo->move($directory_logo, $filename);

                    $update_latest_setting = SysConfig::where('company_id',$company_id)
																->update(['company_logo'=>$filename]);


           			 }

						/**/

							return response()->json(['status' => "900",
													'message' =>"Setting configuration is updated"]);
					
						}else{
							return response()->json(['status' => "901",
													'message' =>"Default setting is not there"]);

						}										

					}catch (\Exception $e) {
		
						  return response()->json(['status' => "902",'message' => $e->getMessage()],422);
					}
			}
		}

		public function getSystemConfigurationForCompany(Request $request)
		{
			$company_id =$request->input('company_id');

			$validator = Validator::make($request->all(), [ //* Validating on incoming data
				'company_id' => 'required|exists:userdetails,company'
			]);


			if($validator->fails()){

			return response()->json(['status' => "901",'message' => $validator->messages()]);

			}else{

				try{
					$sysConfig = SysConfig::where('company_id',$company_id)
											->get();
		
						return response()->json(['status' => "900",
												 'message' =>"Records Found",
												 'data' => $sysConfig]);
					}
					catch (\Exception $e) {
		
						  return response()->json(['status' => "902",'message' => "Exception Occured"],422);
					}
			}
		}
}
