<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use App\AssetInfo;
use App\AssetLog;
use Illuminate\Support\Facades\Storage;
use App\Imports\AssetImport;
use Maatwebsite\Excel\Facades\Excel;
use App\AssetMovementHistory;
use App\Mail\AssetMovementNotification;
use App\Userdetails;
use App\Locations;
use App\QRBarCodeFields;
use App\AssetBreakdown;
use App\AssetsVerification;
use App\AssetsDiscarded;
use App\TempAssetInfo;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Mail;

use Illuminate\Support\Facades\Schema;

class AssetController extends Controller
{
    public function addSingleAsset(Request $request)
    {
      $asset_id = $request->input('asset_id');
      $asset_type   =$request->input('asset_type');
      $asset_name = $request->input('asset_name');
  		$category  =$request->input('category');
  		$sub_category =$request->input('sub_category');
  		$brand 	  =$request->input('brand');
  		$model   =$request->input('model');
  		$serial_number	   =$request->input('serial_number');
  		$vendor_id	   =$request->input('vendor_id');
  		$description   =$request->input('description');
  		$warranty_date 	   =$request->input('warranty_date');
  		$condition   =$request->input('condition');
  		$po_number   =$request->input('po_number');

  		$invoice_number   =$request->input('invoice_number');
  		$invoice_date   =$request->input('invoice_date');
  		$purchase_price   =$request->input('purchase_price');
  		$purchase_date   =$request->input('purchase_date');
  		$capitalization_price  =$request->input('capitalization_price');
  		$end_of_life   =$request->input('end_of_life');
      $depreciation_rate   =$request->input('depreciation_rate');
      
      $depreciation_method   =$request->input('depreciation_method');
  		$depreciation_for_the_year   =$request->input('depreciation_for_the_year');
  		$total_depreciation   =$request->input('total_depreciation');
      $ownership   =$request->input('ownership');
  		$capitalization_date   =$request->input('capitalization_date');
  		$scrap_value   =$request->input('scrap_value');
  		$department   =$request->input('department');
  		$date   =  Carbon::now(); 

  		$location   =$request->input('location');
  		$unit   =$request->input('unit');
  		$emp_id   =$request->input('emp_id');
      $company   = strtolower($request->input('company'));

    /*  $tz=date_default_timezone_get();

      return print_r(timezone_open());die();*/
      
/*      $dtgt=str_replace('+00:00', 'Z', gmdate('c', strtotime($warranty_date)));
*/      $dtgt=date("Y-m-d\TH:i:s.000\Z", strtotime($warranty_date));

     /* print_r($dtgt);die();*/

/*      return print_r($warranty_date=Carbon::create($warranty_date, $tz));
*/

		$validator = Validator::make($request->all(), [
                    'asset_id'  =>'required|unique:asset_info,asset_id',
                    'asset_type' => 'required|exists:asset_type,asset_type_id',
                    'asset_name'  =>'required',
                    'category'  => 'required',
                    'sub_category'  =>'nullable',
                    'brand'  =>'required',
                    'model'  =>'required',
                    'description'  =>'required',
                    'location'  =>'required',
                    'unit'  =>'required',
                    'vendor_id'  =>'required',
                    'emp_id'  =>'required',
                    'company'  => 'required',

                    'warranty_date'  => 'nullable|date',
                    'serial_number'  => 'nullable',
                    'condition'  =>'nullable',
                    'po_number'   => 'nullable',
                    'invoice_number'  =>'nullable',
                    'invoice_date'  =>'nullable|date',
                    'purchase_price'  => 'nullable|numeric',
                    'purchase_date'  =>'nullable|date',
                    'capitalization_price'  =>'nullable|numeric',
                    'end_of_life'  => 'nullable|numeric',
                    'depreciation_rate'  =>'nullable|numeric',
                    'depreciation_for_the_year' =>'nullable|numeric',
                    'depreciation_method' =>'nullable',
                    'total_depreciation'  =>'nullable|numeric',
                    'capitalization_date'  =>'nullable|date',
                    'scrap_value'  => 'nullable|numeric',
                    'department'  =>'nullable',  
                    'ownership'  =>'nullable',
                    'image'=>'nullable|image|mimes:jpg,png,jpeg|max:10000'              

			]);

           
            if ($validator->fails()) {
    
    	          	return response()->json(['status' => "901",'message' => $validator->messages()]);
          	
       		}else {

       			try
                {

                  $p_dt = Carbon::parse($purchase_date);
                  $today_dt = $date;

                  if($p_dt->gt($today_dt)){
                    
                    $errs = array('purchase_date'=>"Purchase date will not greater than today's date");

                    return response()->json(['status' => "901",'message' => $errs ]);
                  
                  }

                  //  Fetching the fields for QR & Barcode
                  $fetching_fields_data = QRBarCodeFields::where('company_id',$company)
                                                            ->first(); 

                    if(empty($fetching_fields_data))
                    {


                          return response()->json(['status' => "907",
                                                  'message' => "First Set QR & Barcode Fields"]);


                    }else{


                      // Asset ID checking 
                        $asset_id_check = AssetInfo::where('asset_id',$asset_id)
                                                     ->where('company',$company)
                                                     ->first();

                        if(empty($asset_id_check)){

                            $asset_data = new AssetInfo();
                            $asset_data->asset_id    = $asset_id;
                            $asset_data->asset_type    = $asset_type;
                            $asset_data->asset_name    = $asset_name;
                            $asset_data->category    = $category;
                            $asset_data->sub_category    = $sub_category;
                            $asset_data->brand    = $brand;
                            $asset_data->serial_number    = $serial_number;
                            $asset_data->model    = $model;
                            $asset_data->description    = $description;
                            $asset_data->warranty_date    = $warranty_date;
                            $asset_data->condition    = $condition;
                            $asset_data->vendor_id    = $vendor_id;
                            $asset_data->po_number    = $po_number;
                            $asset_data->invoice_number    = $invoice_number;
                            $asset_data->invoice_date    = $invoice_date;
                            $asset_data->purchase_price    = $purchase_price;
                            $asset_data->purchase_date    = $purchase_date;
                            $asset_data->capitalization_price    = $capitalization_price;
                            $asset_data->end_of_life    = $end_of_life;
                            $asset_data->depreciation_rate    = $depreciation_rate;

                            $asset_data->depreciation_method    = $depreciation_method;
                            $asset_data->depreciation_for_the_year    = $depreciation_for_the_year;
                            $asset_data->total_depreciation    = $total_depreciation;
                            $asset_data->ownership    = $ownership;

                            $asset_data->capitalization_date    = $capitalization_date;
                            $asset_data->scrap_value    = $scrap_value;
                            $asset_data->department    = $department;
                            $asset_data->location    = $location;
                            $asset_data->unit    = $unit;
                            $asset_data->status    =1;
                            $asset_data->emp_id    = $emp_id;
                            $asset_data->date = $date;
                            $asset_data->company = $company;

                            if ($request->hasFile('image')) {


                              $file=$request->file('image');

                              $directory = 'asset/image/';
                              $filename = time() .'.'. $file->getClientOriginalExtension();
                              $path = $file->move($directory, $filename);

                              $asset_data->image = url($directory.$filename);
                              // code...
                            }else{
                              $asset_data->image = "default.png";
                            }
                            $asset_data->save();

                            $fields_data = $fetching_fields_data->fields;
                            // fetching field values
                            $asset_data_accordingto_fields = AssetInfo::select($fields_data)
                                                                    ->where('asset_id',$asset_id)
                                                                    ->where('company',$company)
                                                                    ->get();

                            Log::info("Asset Inserted");
                            return response()->json(['status' => "900",
                                                      'message' => "Asset submitted successfully",
                                                      'data' => $asset_data_accordingto_fields
                                                        ]);

                   }else {

                      Log::error("Asset ID will be unique");
                      return response()->json(['status' => "907",'message' => "Asset ID will be unique"]);

                    }
                  }
                }
                catch (\Exception $e) {

                    Log::critical("Asset Module Exception".$e->getMessage());
                    return response()->json(['status' => "902",'message' => "Internal Server Error "],422);
                }
       			
       		}

    }

    public function editAsset(Request $request)
    {
      $asset_id = $request->input('asset_id');
      $asset_type   =$request->input('asset_type');
      $asset_name = $request->input('asset_name');
  		$category  =$request->input('category');
  		$sub_category =$request->input('sub_category');
  		$brand 	  =$request->input('brand');
  		$model   =$request->input('model');
  		$serial_number	   =$request->input('serial_number');
  		$vendor_id	   =$request->input('vendor_id');
  		$description   =$request->input('description');
  		$warranty_date 	   =$request->input('warranty_date');
  		$condition   =$request->input('condition');
  		$po_number   =$request->input('po_number');

  		$invoice_number   =$request->input('invoice_number');
  		$invoice_date   =$request->input('invoice_date');
  		$purchase_price   =$request->input('purchase_price');
  		$purchase_date   =$request->input('purchase_date');
  		$capitalization_price  =$request->input('capitalization_price');
  		$end_of_life   =$request->input('end_of_life');
      $depreciation_rate   =$request->input('depreciation_rate');
      // $accumulated_depreciation =$request->input('accumulated_depreciation');
      $depreciation_method   =$request->input('depreciation_method');
  		$depreciation_for_the_year   =$request->input('depreciation_for_the_year');
  		$total_depreciation   =$request->input('total_depreciation');
      $ownership   =$request->input('ownership');
  		$capitalization_date   =$request->input('capitalization_date');
  		$scrap_value   =$request->input('scrap_value');
  		$department   =$request->input('department');
  		// $date   =$request->input('date');

  		$location   =$request->input('location');
  		$unit   =$request->input('unit');
  		$emp_id   =$request->input('emp_id');
      $company   = strtolower($request->input('company'));

                    $validator = Validator::make($request->all(), [
                                    
                                    'asset_type' => 'required|exists:asset_type,asset_type_id',
                                    'asset_name'  =>'required',
                                    'category'  => 'required',
                                    'sub_category'  =>'',
                                    'brand'  =>'required',
                                    'model'  =>'required',
                                    'description'  =>'required',
                                    'location'  =>'required',
                                    'unit'  =>'required',
                                    'vendor_id'  =>'required',
                                    'emp_id'  =>'required',
                                    'company'  => 'required',

                                    'warranty_date'  => 'nullable|date',
                                    'serial_number'  => 'nullable',
                                    'condition'  =>'nullable',
                                    'po_number'   => 'nullable',
                                    'invoice_number'  =>'nullable',
                                    'invoice_date'  =>'nullable|date',
                                    'purchase_price'  => 'nullable|numeric',
                                    'purchase_date'  =>'nullable|date',
                                    'capitalization_price'  =>'nullable|numeric',
                                    'end_of_life'  => 'nullable|numeric',
                                    'depreciation_rate'  =>'nullable|numeric',
                                    'depreciation_for_the_year' =>'nullable|numeric',
                                    'depreciation_method' =>'nullable',
                                    'total_depreciation'  =>'nullable|numeric',
                                    'capitalization_date'  =>'nullable|date',
                                    'scrap_value'  => 'nullable|numeric',
                                    'department'  =>'nullable',  
                                    'ownership'  =>'nullable'              

                                  ]);

           
            if ($validator->fails()) {
    
    	          	return response()->json(['status' => "901",'message' => $validator->messages()]);
          	
       		}else {

           
       			try
                {
                  //  Fetching the fields for QR & Barcode
                  $fetching_fields_data = QRBarCodeFields::where('company_id',$company)
                                                            ->first(); 

                    if(empty($fetching_fields_data))
                    {


                          return response()->json(['status' => "907",
                                                  'message' => "First Set QR & Barcode Fields"]);


                    }else{


                            $asset_data = AssetInfo::where('asset_id',$asset_id)
                                                        ->where('company',$company)
                                                        ->update([
                                                        'asset_type' => $asset_type,
                                                        'asset_name' => $asset_name,
                                                        'category' => $category,
                                                        'sub_category' => $sub_category,
                                                        'brand' => $brand,
                                                        'model' => $model,
                                                        'unit' => $unit,
                                                        'location' => $location,
                                                        'vendor_id' => $vendor_id,
                                                        'description' => $description,
                                                        'emp_id' => $emp_id,
                                                        'status' => 1,
                          
                                                        'serial_number' => $serial_number,
                                                        'warranty_date' => $warranty_date,
                                                        'condition' => $condition,
                                                        'po_number' => $po_number,
                                                        'invoice_number' => $invoice_number,
                                                        'invoice_date' => $invoice_date,
                                                        'purchase_price' => $purchase_price,	
                                                        'purchase_date' => $purchase_date,	
                                                        'capitalization_price' => $capitalization_price,	
                                                        'end_of_life' => $end_of_life,	
                                                        'depreciation_rate' => $depreciation_rate,	
                                                        'depreciation_method' => $depreciation_method,	
                                                        'depreciation_for_the_year' => $depreciation_for_the_year,	
                                                        'total_depreciation' => $total_depreciation,	
                                                        'ownership' => $ownership,	
                                                        'capitalization_date' => $capitalization_date,	
                                                        'scrap_value' => $scrap_value,	
                                                        'department' => $department
                                                      ]);
                           

                            $fields_data = $fetching_fields_data->fields;

                            /*insert into logg table*/
                            $logg_status="edited";
                                $logg_created_by=$emp_id;
                                $logg_validity_time="10d";

                              $this->assetEditLog($asset_id,$logg_status,$logg_created_by,$logg_validity_time);

                            // fetching field values
                            $asset_data_accordingto_fields = AssetInfo::select($fields_data)
                                                                    ->where('asset_id',$asset_id)
                                                                    ->where('company',$company)
                                                                    ->get();

                            Log::info("Asset updated");
                            return response()->json(['status' => "900",
                                                      'message' => "Asset updated successfully",
                                                      'data' => $asset_data_accordingto_fields
                                                        ]);
                  }
                }
                catch (\Exception $e) {

                    Log::error("Asset Module Exception",$e->getMessage());
                    return response()->json(['status' => "902",'message' => "Internal Server Error "],422);
                }
       			
       		}

    }

   
    public function deleteAsset(Request $request)
    { 
      $validator = Validator::make($request->all(), [
                                      'asset_id'  =>'required',
                                      'company_id'  =>'required',
                                      'username'  =>'required'
                                ]);

        $asset_id = $request->input('asset_id');
        $company_id = $request->input('company_id');
        $username = $request->input('username');

        if ($validator->fails()) {
    
          return response()->json(['status' => "901",'message' => $validator->messages()]);
    
        }else {

                try
                {     
                    //check presence of asset in other tables
                    $check_in_verification_tab = AssetsVerification::where('asset_id',$asset_id) 
                                                                    ->where('company_id',$company_id)
                                                                    ->first();

                    $check_in_maintenance_tab = AssetBreakdown::where('asset_id',$asset_id) 
                                                                  ->where('company_id',$company_id)
                                                                  ->first();
                    $check_in_assetsdiscarded_tab = AssetsDiscarded::where('asset_id',$asset_id) 
                                                                  ->where('company_id',$company_id)
                                                                  ->first();

                    $check_in_movement_tab = AssetMovementHistory::where('asset_id',$asset_id) 
                                                                  ->where('company_id',$company_id)
                                                                  ->first(); 

                    if(empty($check_in_verification_tab) || empty($check_in_maintenance_tab) || empty($check_in_movement_tab) || empty($check_in_assetsdiscarded_tab)){

                        $status_asset_data = AssetInfo::where('asset_id',$asset_id)
                                                        ->where('company',$company_id)
                                                        ->where('emp_id',$username)
                                                        ->first();
                          if(empty($status_asset_data)){
                          
                            return response()->json(['status' => "901",
                                                    'message' => "Asset information is wrong"
                                                ]);
                          }else{


                            /*insert into logg*/

                            $status_asset_data = AssetInfo::where('asset_id',$asset_id)
                                                        ->where('company',$company_id)
                                                        ->where('emp_id',$username)
                                                        ->get();
                                      $logg_status="deleted";
                                      $logg_created_by=$username;
                                      $logg_validity_time="10d";

                              $this->assetDeleteLog($status_asset_data,$logg_status,$logg_created_by,$logg_validity_time);
                            /**/

                            $status_asset = AssetInfo::where('asset_id',$asset_id)
                                                        ->where('company',$company_id)
                                                        ->where('emp_id',$username)
                                                        ->delete();

                                                        

                                                        
                            return response()->json(['status' => "900",
                                                  'message' => "Assets deleted successfully"
                                                ]);
                          }
                      
                    }else{

                      return response()->json(['status' => "901",
                                                 'message' => "This asset is already in used. You can't delete it."
                                              ]);

                    }
                      
                 }catch (\Exception $e) {

                            Log::error("Asset Module Exception",$e->getMessage());
                            return response()->json(['status' => "902",
                                                          'message' => "Internal Server Error "]);
                    }
              }
    }
     
    public function listOfAssetsByLikewise(Request $request) {

      $validator = Validator::make($request->all(), [
                                      'filters_arr'  =>'required'
                                  ]);

      if ($validator->fails()) {
    
         return response()->json(['status' => "901",'message' => $validator->messages()]);
                              
      }
                       $filters_arr = $request->input('filters_arr');  

                       $selected_cols = array('asset_info.asset_id',
                                'locations.location_name as location_name',
                                'locations.loc_id as location_id',
                                'locations.city as location_city',
                                'locations.state as location_state',                                );

                                if ($validator->fails()) {

                                return response()->json(['status' => "901",'message' => $validator->messages()]);

                                }else {

                                try
                                {
                                   $asset_data = AssetInfo::query()
                                                           ->whereNested(function($query) use ($filters_arr) {
                                                             foreach ($filters_arr as $key => $value)
                                                                 {
                                                                     if($value != ''){
                                                                         $query->where($key,'LIKE','%'.$value.'%');
                                                                     }
                                                                 }
                                                           }, 'and');

                                   $asset_data = $asset_data->join('locations', 'asset_info.location', '=', 'locations.loc_id')
                                                         ->where('asset_info.status',1)
                                                         ->select($selected_cols)
                                                         ->take(10)
                                                         ->get();
                                  
                                       return response()->json($asset_data);
                                }catch (\Exception $e) {

                                    Log::error("Asset Module Exception",$e->getMessage());
                                    return response()->json(['status' => "902",
                                                                    'message' => "Internal Server Error "],422);
                                }
             }
    }

    public function listOfAssetsByClauses(Request $request)
    { 


        $validator = Validator::make($request->all(), [
                                        
                                        'company_id'  =>'required',
                                        'sort_type' =>'required|in:asc,desc',
                                       
                                  ],['sort_type.required' => "Short type must be either 'asc' or 'desc' ",
                                  'sort_type.in' => "Short type must be either 'asc'  or 'desc' ",]);

        if ($request->column_name == Null) {

          $column_name="asset_id";
          // code...
        }
        else{

          if (Schema::hasColumn('asset_info', $request->column_name)) {

            $column_name=$request->input('column_name');

          }else{

            return response()->json(['status' => "901",'message' => ["column_name" =>"Please provide a valid column_name"] ]);
          }


            

        }

      
        $sort_type=$request->input('sort_type');

        $filters_arr = $request->input('filters_arr');  
        $company = $request->input('company_id');  

        $selected_cols = array('asset_info.asset_id',
                              'asset_info.image',
                              'asset_info.asset_type',
                              'asset_info.asset_name',
                              'asset_info.company',
                              'asset_info.category',
                              'asset_info.sub_category',
                              'asset_info.brand',
                              'asset_info.model',
                              'asset_info.unit',
                              'asset_info.date',
                              'asset_info.location',
                              'asset_info.vendor_id',
                              'asset_info.description',
                              'asset_info.emp_id',
                              'asset_info.serial_number',
                              
                              'asset_info.capitalization_price',
                              'asset_info.capitalization_date',
                              'asset_info.condition',
                              'asset_info.department',
                              'asset_info.depreciation_for_the_year',
                              'asset_info.depreciation_method',
                              'asset_info.total_depreciation',
                              'asset_info.end_of_life',
                              'asset_info.invoice_number',
                              'asset_info.invoice_date',
                              'asset_info.po_number',
                              'asset_info.purchase_date',
                              'asset_info.purchase_price',
                              'asset_info.scrap_value',
                              'asset_info.warranty_date',

                              'asset_type.name as asset_type_name',

                              'vendors.vendor_name as vendor_name',
                              'locations.location_name as location_name',
                              'locations.city as location_city',
                              'locations.state as location_state',

                              'categories.name as category_name',
                              'subcat.name as subcategory_name'
                            );

                          if ($validator->fails()) {
    
                            return response()->json(['status' => "901",'message' => $validator->messages()]);
                      
                          }else {
          
                                try
                                {
                                   $asset_data = AssetInfo::query()
                                                          ->whereNested(function($query) use ($filters_arr) {
                                                            foreach ($filters_arr as $key => $value)
                                                                {
                                                                    if($value != ''){
                                                                        $query->where($key,'LIKE',$value);
                                                                    }
                                                                }
                                                          }, 'and');

                                   $asset_data = $asset_data->join('asset_type','asset_info.asset_type','=','asset_type.asset_type_id')
                                                        ->join('categories', 'asset_info.category', '=', 'categories.category_id')

                                                        ->leftJoin('categories AS subcat' , 'asset_info.sub_category', '=', 'subcat.category_id')
                                                        
                                                        ->leftJoin('vendors', 'asset_info.vendor_id', '=', 'vendors.id')
                                                        ->join('locations', 'asset_info.location', '=', 'locations.loc_id')
                                                        ->where('asset_info.status',1)
                                                        ->where('asset_info.company',$company)
                                                        ->select($selected_cols)
                                                        ->orderBy("asset_info" .".".$column_name,$sort_type)
                                                        // ->orderBy("asset_info.created_at","desc")
                                                      //   ->whereNested(function($query) use ($column_name) {
                                                            
                                                      //           if($value != ''){
                                                      //               $query->orderBy("asset_info".$column_name,$sort_type);
                                                      //           }else{
                                                      //             $query->orderBy("asset_info.created_at","desc");
                                                      //           }
                                                    
                                                      // })
                                                        ->paginate(10);

                                                   /*     return count($asset_data);*/
                                                   
                                    return response()->json(['status' => "900",
                                                            'message' => "Assets listing successfully",
                                                             'data' => $asset_data
                                                            ]);

                                }catch (\Exception $e) {

                                     Log::error("Asset Module Exception".$e->getMessage());
                                     return response()->json(['status' => "902",
                                                                    'message' => $e->getMessage()],422);
                                }
                      }
    }

    public function getOneRecordOfAsset(Request $request)
    { 
        $company_id = strtolower($request->input('company_id'));
        
        $location_id = $request->input('location_id');
        $asset_id = $request->input('asset_id');
        
		              $validator = Validator::make($request->all(), [
                                'company_id'  =>'required|exists:userdetails,company', 
                                'location_id'  =>'required|exists:asset_info,location', 
                                'asset_id'  =>'required|exists:asset_info,asset_id' 
                          ]);
                          if ($validator->fails()) {
    
                            return response()->json(['status' => "901",'message' => $validator->messages()]);
                      
                          }else {
          
                                try
                                {
                                   $asset_data = AssetInfo::where('company',$company_id)
                                                                ->where('location',$location_id)
                                                                ->where('asset_id',$asset_id)
                                                                ->where('status',1)
                                                                ->orderBy('created_at','desc')
                                                                ->paginate(10);
                                                   
                                    return response()->json(['status' => "900",
                                                            'message' => "Assets listing successfully",
                                                             'data' => $asset_data
                                                            ]);

                                }catch (\Exception $e) {

                                          Log::error("Asset Module Exception",$e->getMessage());
                                          return response()->json(['status' => "902",
                                                                    'message' => "Internal Server Error "],422);
                                }
                      }
    }

    // * Asset Movement Tracking

    public function assetMovementInputs(Request $request)
    {
        $asset_id = $request->input('asset_id');
        $from_location =$request->input('from_location');
        $to_location =$request->input('to_location');
        $remarks =$request->input('remarks');
        $date  =$request->input('date');
        $email_notification   = $request->input('email_notification');
        $movement_by   =$request->input('movement_by');
        $company_id   = strtolower($request->input('company_id'));


        $validator = Validator::make($request->all(), [
                              'asset_id'  =>'required|exists:asset_info,asset_id',
                              'from_location' => 'required|exists:locations,loc_id',
                              'to_location'  =>'required|exists:locations,loc_id',
                              'remarks'  => 'required',
                              'date'  =>'required',
                              'movement_by'  => 'required',
                              'company_id'  => 'required|exists:userdetails,company'   
                            ]);

              
                if ($validator->fails()) {

                      return response()->json(['status' => "901",'message' => $validator->messages()]);
                
              }else {

                  try{
                      

                      // ? Update location in Asset Info Table
                        $fetch_previous_data = AssetInfo::where('asset_id', $asset_id)
                                                          ->where('company', $company_id)
                                                          ->where('status',1)
                                                          ->first();

                        $asset_data = AssetInfo::where('asset_id', $asset_id)
                                                ->where('company', $company_id)
                                                ->where('status',1)
                                                ->update(['location'=>$to_location]);


                      //? Insert this movement transaction to another table

                        $movement_data = new AssetMovementHistory();
                        $movement_data->asset_id    = $asset_id;
                        $movement_data->company_id = $company_id;
                        $movement_data->from_location    = $from_location;
                        $movement_data->to_location = $to_location;
                        $movement_data->remarks    = $remarks;
                        $movement_data->date = $date;
                        $movement_data->email_notification    = $email_notification;
                        $movement_data->movement_by = $movement_by;
                        $movement_data->save();

                      if($email_notification){
                      //? Send mail notification to admin email_notification
                        $asset_name = $fetch_previous_data['asset_name'];
                        $asset_id = $fetch_previous_data['asset_id'];
                        $category = $fetch_previous_data['category'];
                        $sub_category = $fetch_previous_data['sub_category'];

                            //? fetching from and to location name
                            $location_from_data = Locations::where('loc_id',$from_location)
                                                              ->first();
                            $location_to_data = Locations::where('loc_id',$to_location)
                                                              ->first();
                            $from_location_name = $location_from_data['location_name'].",".$location_from_data['city'].",".$location_from_data['state'];
                            $to_location_name = $location_to_data['location_name'].",".$location_to_data['city'].",".$location_to_data['state'];

                            //? fetching admin mail address
                        $master_client = Userdetails::where('company', $company_id)
                                                      ->first();

                        $master_client_mail = $master_client['email'];



                        \Mail::to($master_client_mail)->send(new AssetMovementNotification(
                                                            $asset_name,    
                                                            $asset_id,
                                                            $category,
                                                            $sub_category,
                                                            $from_location_name,
                                                            $to_location_name
                                                          ));
                         }


                          Log::info($asset_id."Asset Moved");
                          return response()->json(['status' => "900",'message' => "Asset moved",
                                             'asset_id' => $asset_id]);
                      }catch (\Exception $e) {

                        Log::error($e->getMessage());
                        return response()->json(['status' => "902",'message' => "Internal Server Error "],422);
                    }
              }

    }


    public function assetMovementInputsUpdate(Request $request)
    {
      $asset_id = $request->input('asset_id');
      $from_location =$request->input('from_location');
      $to_location =$request->input('to_location');
      $remarks =$request->input('remarks');
      $date  =$request->input('date');

      $id  =$request->input('id');

      
      $movement_by   =$request->input('movement_by');
      $company_id   = strtolower($request->input('company_id'));


      $validator = Validator::make($request->all(), [
                            'asset_id'  =>'required|exists:asset_info,asset_id',
                            'from_location' => 'required|exists:locations,loc_id',
                            'to_location'  =>'required|exists:locations,loc_id',
                            'remarks'  => 'required',
                            'date'  =>'required',
                            'id'    => 'required|exists:asset_movement_history,id',
                            'movement_by'  => 'required',
                            'company_id'  => 'required|exists:userdetails,company'   
                          ]);

            
              if ($validator->fails()) {

                    return response()->json(['status' => "901",'message' => $validator->messages()]);
              
            }else {

                  try{

                      $asset_movement_history_update = AssetMovementHistory::find($id);
                      $asset_movement_history_update->asset_id    = $asset_id;
                      $asset_movement_history_update->from_location    = $from_location;
                      $asset_movement_history_update->to_location    = $to_location;
                      $asset_movement_history_update->remarks    = $remarks;
                      $asset_movement_history_update->date    = $date;
                      $asset_movement_history_update->save();

                     
                      return response()->json(['status' => "900",'message' => "Asset movement updated",
                                         'asset_id' => $asset_id]);
                  }catch(\Exception $e){
                    Log::error($e->getMessage());
                    return response()->json(['status' => "902",'message' => "Internal Server Error "],422);
                  }
              }

    }

    public function assetFieldList()
    {
      $asset_fields = array('Asset Id'=>'asset_id',
                            'Asset Type'=>'asset_type',
                            'Asset Name' => 'asset_name',
                            'Company' => 'company',
                            'Category' => 'category',
                            'Sub Category' => 'sub_category',
                            'Brand' => 'brand',
                            'Model' => 'model',
                            'Unit' => 'unit',
                            'Date' => 'date',
                            'Location' => 'location',
                            'Vendor Id' => 'vendor_id',
                            'Description' => 'description',
                            'Employee Id' => 'emp_id',

                            'Serial Number' => 'serial_number',
                            'Warranty Date' => 'warranty_date',
                            'Condition' => 'condition',
                            'PO Number' => 'po_number',
                            'Invoice Number' => 'invoice_number',
                            'Invoice Date' => 'invoice_date',
                            'Purchase Price' => 'purchase_price',	
                            'Purchase Date' => 'purchase_date',	
                            'Capitalization Price' => 'capitalization_price',	
                            'End of Life' => 'end_of_life',	
                            'Depreciation Rate' => 'depreciation_rate',	
                            'Depreciation Method' => 'depreciation_method',	
                            'Depreciation for the Year' => 'depreciation_for_the_year',	
                            'Total Depreciation' => 'total_depreciation',	
                            'Ownership' => 'ownership',	
                            'Capitalization Date' => 'capitalization_date',	
                            'Scrap Value' => 'scrap_value',	
                            'Department' => 'department'
                          );
                            
          return response()->json(['status' => "900",
                                    'data' => $asset_fields]);
    }

    public function deleteSingleAssetFromTempTable(Request $request) {

         $uuid = $request->input('uuid');

         $validator = Validator::make($request->all(), [
                                  'uuid'  =>'required|uuid|exists:temp_asset_info,uuid',  
                                ]);

          if($validator->fails()){
            return response()->json(['status' => "901",'message' => $validator->errors()]);
          }else{

              try{
                     $del_temp_asset = TempAssetInfo::where('uuid',$uuid)
                                                      ->delete();

                     return response()->json(['status' => "900",'message' => "Asset Delete"]);         
              }catch(\Exception $e){
                    Log::critical($e->getMessage());
                    return response()->json(['status' => "902",'message' => "Internal Server Error "],422);
              }
          }
    }

    public function deleteAssetsFromTempTable(Request $request) {

 
      $added_by = $request->input('added_by');
      $company_id = $request->input('company_id');

      $validator = Validator::make($request->all(), [
                               'company_id'  =>'required|exists:userdetails,company', 
                             ]);

       if($validator->fails()){
         return response()->json(['status' => "901",'message' => $validator->errors()]);
       }else{

           try{
                  $del_temp_asset = TempAssetInfo::where('company',$company_id)
                                                  ->where('added_by',$added_by)
                                                   ->delete();

                  return response()->json(['status' => "900",'message' => "All temporary records deleted"]);         
           }catch(\Exception $e){
                 Log::critical($e->getMessage());
                 return response()->json(['status' => "902",'message' => "Internal Server Error "],422);
           }
       }
 }


 /*mail for asset warenty*/
  public function sentmailAlert()
  {
    

    $fromDate = Carbon::now();

    $toDate = Carbon::now()->addDay(5);
       

    $getall=AssetInfo::whereBetween('warranty_date', [$fromDate, $toDate])->get();

    foreach($getall as $item) {
      
        
        $company=Userdetails::where('company',$item->company)->first();
        $email=$company->email;

        Mail::send('customer.pages.customerKycApproveMail',$data,function($message) use ($name,$email){
                             $message->to($email)->subject('Asset warenty is going to be expired');
            });


    }

    



  }


/*asset logg for edit*/
  public function assetEditLog($asset_id,$logg_status,$logg_created_by,$logg_validity_time){

                $asetlog = new AssetLog;
                $asetlog->asset_id=$asset_id;
                $asetlog->logg_status=$logg_status;
                $asetlog->emp_id=$logg_created_by;
                $asetlog->logg_validity_time=$logg_validity_time;
                /*d for days*/
                $asetlog->save();
  }


  /* asset logg for delete*/
  public function assetDeleteLog($status_asset_data,$logg_status,$logg_created_by,$logg_validity_time){

                $asset_data = new AssetLog;

                foreach ($status_asset_data as $item) {

                  /**/
                          $asset_data->asset_id    = $item->asset_id;
                            $asset_data->asset_type    = $item->asset_type;
                            $asset_data->asset_name    = $item->asset_name;
                            $asset_data->category    = $item->category;
                            $asset_data->sub_category    = $item->sub_category;
                            $asset_data->brand    = $item->brand;
                            $asset_data->serial_number    = $item->serial_number;
                            $asset_data->model    = $item->model;
                            $asset_data->description    = $item->description;
                            $asset_data->warranty_date    = $item->warranty_date;
                            $asset_data->condition    = $item->condition;
                            $asset_data->vendor_id    = $item->vendor_id;
                            $asset_data->po_number    = $item->po_number;
                            $asset_data->invoice_number    = $item->invoice_number;
                            $asset_data->invoice_date    = $item->invoice_date;
                            $asset_data->purchase_price    = $item->purchase_price;
                            $asset_data->purchase_date    = $item->purchase_date;
                            $asset_data->capitalization_price    = $item->capitalization_price;
                            $asset_data->end_of_life    = $item->end_of_life;
                            $asset_data->depreciation_rate    = $item->depreciation_rate;

                            $asset_data->depreciation_method    = $item->depreciation_method;
                            $asset_data->depreciation_for_the_year    = $item->depreciation_for_the_year;
                            $asset_data->total_depreciation    = $item->total_depreciation;
                            $asset_data->ownership    = $item->ownership;

                            $asset_data->capitalization_date    = $item->capitalization_date;
                            $asset_data->scrap_value    = $item->scrap_value;
                            $asset_data->department    = $item->department;
                            $asset_data->location    = $item->location;
                            $asset_data->unit    = $item->unit;
                            $asset_data->status    =1;
                            $asset_data->emp_id    = $item->emp_id;
                            $asset_data->date = $item->date;
                            $asset_data->company = $item->company;

                  /**/
                  
                }
                
                $asset_data->logg_status=$logg_status;
                $asset_data->emp_id=$logg_created_by;
                $asset_data->logg_validity_time=$logg_validity_time;
                /*d for days*/
                $asset_data->save();
  }
  /**/

  /*auto delete from asset logg table*/
/*  public function autoDeleteAssetLogg(){


    $getData=AssetLog::where('logg_status',"deleted")->get();

    foreach ($getData as $item) {

      $loggtime=$item->logg_validity_time;
      $loggcreated_at=$item->created_at;

      $loggtime=substr($loggtime,-1);
      # code...
    }

  }*/


  /*list for asset logg*/
  public function assetLoggList(Request $request)
  {
    # code...

    $validator = Validator::make($request->all(), [
                               'asset_id'  =>'required', 
                             ]);

       if($validator->fails()){
         return response()->json(['status' => "901",'message' => $validator->errors()]);
       }else{

        $assetLogg=AssetLog::where('asset_id',$request->asset_id)->orderBy('created_at','desc')->get();

                 return response()->json(['status' => "900",'message' =>"Records found",'data'=>$assetLogg]);



       }


  }




}
