<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use App\AssetInfo;
use Illuminate\Support\Facades\Storage;
use App\Imports\AssetImport;
use Maatwebsite\Excel\Facades\Excel;
use App\AssetMovementHistory;
use App\Mail\AssetMovementNotification;
use App\Userdetails;
use App\Locations;
use App\QRBarCodeFields;
use App\AssetBreakdown;
use App\AssetsVerification;
use App\TempAssetInfo;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AssetFinanceController extends Controller
{
    public function getGrossBlockData(Request $request) {

           $year_start = $request->input('year_start');
           $year_end = $request->input('year_end');
           $company_id = $request->input('company_id');

        //    $asset_data = DB::table('asset_info')
        //                             ->where('company', $company_id)
        //                             ->get()
        //                             ->groupBy('category')->map(function ($group) {
        //                                 return $group->map(function ($value) {
        //                                       return [ "id" => $value->id,
        //                                        "name" => $value->name
        //                                      ];
        //                                 });
        //                            });  

           $asset_data = DB::select('CALL `showAssets`('."'".$year_start."'".',
                                                      '."'".$year_end."'".',
                                                      '."'".$company_id."'".'                                         
                                                      
                                                      )');
/*
BEGIN
SELECT SUM(asset_info.purchase_price) as amt, asset_info.category FROM asset_info
JOIN assets_verification ON asset_info.asset_id=assets_verification.asset_id
WHERE asset_info.capitalization_date BETWEEN dt1 AND dt2
AND
asset_info.company = company_id
GROUP BY category; 
END
*/
                                    

          return response()->json(['status' => "900",
                                    'message' => "Asset submitted successfully",
                                    'data' => $asset_data
                                      ]);

    }
}
