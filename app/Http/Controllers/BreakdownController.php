<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AssetBreakdown;
use App\AssetInfo;
use Validator;
use App\Userdetails;
use App\Mail\AssetBreakdownMail;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class BreakdownController extends Controller
{
    public function raiseMaintenanceAssetIssueTicket(Request $request)
    {
      $breakdown_dt = Carbon::now();
      $asset_id = $request->input('asset_id');      
      $location_id = $request->input('location_id');
      $problem_stat = $request->input('problem_stat');
      $issued_by = $request->input('issued_by');
      $company_id = $request->input('company_id');
   

      $validator = Validator::make($request->all(), [
                        'asset_id'  => 'required|exists:asset_info,asset_id',
                        'company_id'  => 'required|exists:userdetails,company',
                        'problem_stat'  =>'required',
                        'issued_by'  =>'required',  //* Who is opening the issue
                        'location_id'  => 'required',
                        'img_before_fixed'  =>'nullable|mimes:jpeg,jpg|max:3000', // No PNG is acceptable  / 1000 KB = 1 MB          
                    ]);


                if ($validator->fails()) {

                    return response()->json(['status' => "902",'message' => $validator->messages()]);

                }else {
                        $asset_validation = AssetInfo::where('asset_id', $asset_id)
                                                        ->where('location', $location_id)
                                                        ->where('company', strtolower($company_id))
                                                        ->where('status',1)
                                                        ->first();
                        if(!empty($asset_validation)){

                            if($request->hasFile('img_before_fixed')){

                                $tempimg = $request->file('img_before_fixed'); 
                                $img_name = time() . '.' . $tempimg->getClientOriginalExtension();
                                $tempimg->move('asset_breakdown',$img_name);
                                

                            }else{
                            
                                $img_name = '';
                                    
                            }
                            
                            $new_asset_support_ticket = new AssetBreakdown();
                            $new_asset_support_ticket->breakdown_dt = $breakdown_dt;
                            $new_asset_support_ticket->asset_id = $asset_id;
                            $new_asset_support_ticket->company_id = $company_id;
                            $new_asset_support_ticket->issued_by = $issued_by;
                            $new_asset_support_ticket->location_id = $location_id;
                            $new_asset_support_ticket->problem_stat = $problem_stat;
                            $new_asset_support_ticket->img_before_fixed = $img_name;
                            $new_asset_support_ticket->issue_status = 0;
                            $new_asset_support_ticket->save();


                                // Mail Asset Maintenance ticket information to admin customer
                                $customer_data = Userdetails::where('company',$company_id)->first();
                                
                                $name=$customer_data->firstname.' '.$customer_data->lastname;
                                $email=$customer_data->email;
                                $mail_type = "Asset Breakdown Ticket";
                                \Mail::to($email)->send(new AssetBreakdownMail(
                                                            $name,
                                                            $mail_type,
                                                            $new_asset_support_ticket->id,
                                                            $problem_stat,
                                                            $issued_by));
                                


                                return response()->json(['status' => "900",
                                                        'message' => "Issue Raised",
                                                        'ticket_code' => $new_asset_support_ticket->id ]);
                        }else{

                            return response()->json([
                            'status' => "901",
                            'message' => "Asset is not matching with location and company"]);
                        }
                    }
        }

    public function getImageUrl($fileName){
        $path = url('/asset_breakdown/'.$fileName);
        return $path;        
    }
    public function updateMaintenanceAssetIssueTicket(Request $request)
     {
        $asset_id = $request->input('asset_id');
        $problem_stat = $request->input('problem_stat');
        $location_id = $request->input('location_id');

        $company_id = $request->input('company_id');
        $ticket_id = $request->input('ticket_id');

        $validator = Validator::make($request->all(), [
                            'asset_id'  => 'required|exists:asset_info,asset_id',
                            'company_id'  => 'required|exists:userdetails,company',
                            'problem_stat'  =>'required',
                            'location_id'  => 'required',
                            'ticket_id'  => 'required|exists:asset_maintenance,id',
                            ]);


                if ($validator->fails()) {

                    return response()->json(['status' => "901",'message' => $validator->messages()]);

                }else {
                        //* Validate the Asset 
                        $asset_validation = AssetInfo::where('asset_id', $asset_id)
                                                        ->where('location', $location_id)
                                                        ->where('company', strtolower($company_id))
                                                        ->where('status',1)
                                                        ->first();
                        if(!empty($asset_validation)){

                            if(!empty($asset_validation)){

                                //* Get previous stored attached image
                                $previous_recorded_data = AssetBreakdown::where('id',$ticket_id)
                                                                            ->first();
                                $previous_attached_img_name = $previous_recorded_data['img_before_fixed'];

                                if($request->hasFile('img_before_fixed')){
                                
                                $path = public_path()."/asset_breakdown/".$previous_attached_img_name;
                                unlink($path);

                                $tempimg = $request->file('img_before_fixed'); 
                                $img_name = time() . '.' . $tempimg->getClientOriginalExtension();
                                $tempimg->move('asset_breakdown',$img_name);
                                

    
                                }else{
                                
                                $img_name = '';
                                        
                                }
                                
                                    
                                $update_asset_support_ticket = AssetBreakdown::find($ticket_id);
                                $update_asset_support_ticket->asset_id = $asset_id;
                                $update_asset_support_ticket->location_id = $location_id;
                                $update_asset_support_ticket->problem_stat = $problem_stat;
                                $update_asset_support_ticket->img_before_fixed = $img_name;
                                $update_asset_support_ticket->issue_status = 0;
                                $update_asset_support_ticket->save();


                                // Mail Asset Maintenance ticket information to admin customer
                                $customer_data = Userdetails::where('company',$company_id)->first();
                                
                                $name=$customer_data->firstname.' '.$customer_data->lastname;
                                $email=$customer_data->email;
                                $mail_type = "Update of Asset Maintenance Ticket";
                                $issued_by = "";
                                \Mail::to($email)->send(new AssetBreakdownMail(
                                                            $name,
                                                            $mail_type,
                                                            $update_asset_support_ticket->id,
                                                            $problem_stat,
                                                            $issued_by));
                                


                                return response()->json(['status' => "900",
                                                        'message' => "Issue Updated",
                                                        'ticket_code' => $update_asset_support_ticket->id ]);


                        }else{

                            return response()->json([
                                'status' => "901",
                                'message' => "Asset is not matching with location and company"]);
                        }
                    }

                }
            }
}