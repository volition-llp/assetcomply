<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\HeadingRowImport;
use App\Imports\AssetBulkImport;
use App\AssetInfo;
use App\TempAssetInfo;
use Validator;
use App\QRBarCodeFields;


class BulkAssetController extends Controller
{
    public function bulkAssetUpload(Request $request)
    {
		$file_csv = $request->file('asset_file');
		$added_by = app('request')->header('added_by');
		$company_id = app('request')->header('company_id');
		
		$validator = Validator::make($request->all(), [
						'asset_file' => 'required|max:10000|mimes:xlsx'
	  				 ]);
		
					   if($validator->fails()){

								return response()->json(['status' => "902",
														 'message' => $validator->messages()]);
		
						}


    	if($request->hasFile('asset_file')){

    		$ext = $file_csv->getClientOriginalExtension();

    		 	$headings = (new HeadingRowImport)->toArray($file_csv);
    		 	$rows = (new AssetBulkImport($added_by,$company_id))->toArray($file_csv);

    		 	      if(count($headings[0][0]) == "30"){ //checking count of headings
    		 									
    		 	       	 if(count($rows[0]) <= '1000' && count($rows[0]) > '1'){
						
						  $importclass = new AssetBulkImport($added_by,$company_id);
    		 	       	  \Excel::import($importclass, $file_csv);
							  $res =  $importclass->geterrors();
							 
								 return response()->json(['status' => "900",
															'message' => "Records Saved",
															'data' => $res
															]);

    		 	       	 }else{

							return response()->json(['status' => "901",
							'message' => "Your limit of record is 1000 records & minimum you have to insert 1 record."
							]);
    		 	       	 }

    		 	      }else {

						return response()->json(['status' => "901",
						'message' => "Number of Headers are not matching"
						]);
    		 	      }

    		}

	}
	

	public function bulkAssetRecords(Request $request)
	{
		$type = $request->input('type');

		// filter_data consist of company_id and location_id(optional) and emp_id
		$filter_data = $request->input('filter_data');
		$asset_id = $request->input('asset_id');
		
		$validator = Validator::make($request->all(), [
										'type' => 'required',
										'filter_data' => 'required',
										'asset_id' => 'required'
									]);

		
		if ($validator->fails()) {

            return response()->json(['status' => "901",'message' => $validator->messages()]);
              
        }else {

			   //checking bulk req for qr code

			   if(strtolower($type) == "qrcode"){

				   
				$asset_data_forQrcode = $this->getAssetRecordsForQRcode($filter_data,$asset_id);

				  
				return response()->json(['status' => "900",
											'message' => "Records Found",
											'type' => "qrcode",
											'data' => $asset_data_forQrcode
											]);

			   }else{

					return response()->json(['status' => "901",
											'message' => "No Records Found"
                                            ]);
			   	}
			
		}
		
	}

	public function getAssetRecordsForQRcode($filter_data,$asset_id)
	{
		$select_cols_arr = QRBarCodeFields::where('company_id',$filter_data['company'])
											->select('fields')
											->first();

									// for associative array(key-value pair)
		$asset_data = AssetInfo::whereNested(function($query) use ($filter_data) {
								foreach ($filter_data as $key => $value)
								{
								    if($value != ''){
										$query->where($key,$value);
								     }
								}
								}, 'and');
						   
		$asset_data = $asset_data->whereIn('asset_id',$asset_id);
		$asset_data = $asset_data->where('status',1);
								   //* for putting normal array in where clause

		$asset_data = $asset_data->select($select_cols_arr['fields'])
								  ->get();
								  
		return $asset_data;
								  
								  
	}


	public function getWrongEntryAssetInfo(Request $request){

		$company_id = $request->input('company_id');
		$emp_id = $request->input('emp_id');
		// $location = $request->input('location');
		
		$validator = Validator::make($request->all(), [
										'emp_id' => 'required',
										'company_id' => 'required'
									]);

				
				if ($validator->fails()) {

					return response()->json(['status' => "901",'message' => $validator->messages()]);
					
				}

		$wrong_asset_details = TempAssetInfo::where('emp_id',$emp_id)
											 ->where('company',$company_id)
											 ->orderBy('created_at','desc')
											 ->paginate(10);

		return response()->json(['status' => "900", 'message' =>"Records Found",
								 'data' => $wrong_asset_details]);
	}
}
