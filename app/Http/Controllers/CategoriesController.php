<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;

class CategoriesController extends Controller
{
    public function addCategory(Request $request)
    {
    	 $category_id = $request->input('category_id');
    	 $name = $request->input('name');
    	 $parent_id = $request->input('parent_id');
    	 $asset_type = $request->input('asset_type');
    	 $added_by   =$request->input('added_by');

    	 $validator = Validator::make($request->all(), [

    	 		'category_id' => 'required',
    	 		'name' => 'required',
    	 		'parent_id' => 'required',
    	 		'added_by' => 'required',
    	 		'asset_type' => 'required'
    	 ]);


    	 if($validator->fails()) {

    	 	return response()->json(['status' => "901",'message' => $validator->messages()]);
    	 }else{

    	 	$cat_id_checking = Categories::where('category_id', $category_id)->first();

    	 	    if(empty($cat_id_checking)){
    	 	    	$category_data = new Categories();
    	 	    	$category_data->category_id = $category_id;
    	 	    	$category_data->name = $name;
    	 	    	$category_data->parent_id = $parent_id;
    	 	    	$category_data->asset_type_id = $asset_type;
    	 	    	$category_data->added_by = $added_by;
    	 	    	$category_data->delete_status = 0;
    	 	    	$category_data->save();

    	 	    	return response()->json(['status' => "900",'message' =>	"Category created successfully"]);

    	 	    }else{

    	 	    	return response()->json(['status' => "905",'message' => "PLease provide valid Category Id"]);
    	 	    }

    	 }
    }

    public function listOfCategoriesWithSubcategory()
    {
       $categories_list = Categories::where('delete_status',0)->get();
       $cat_list = array();
       $parent_name;
         foreach($categories_list as $category)
         {
         	if($category->parent_id == "null"){
         		$parent_name[$category->category_id] = $category->name;

         	}
         }

         foreach($categories_list as $category)
         {
         	foreach($parent_name as $key => $val){
         	     if($key == $category->parent_id){

         	     	$cat_list[$val][] = $category->name;

         	     }
         	}
         	
         }


       return response()->json(['status' => "900",'message' => $cat_list]);	
    }

    public function listOfCategoriesWithParent()
    {
       $categories_list = Categories::where('delete_status',0)->get();
     
       $parent_name;
         foreach($categories_list as $category)
         {
         	if($category->parent_id == "null"){
         		$parent_name[] = array(
         								"id"=>  $category->category_id,
         								"name"=> $category->name
         							);
         	}
         }

        return response()->json(['status' => "900",'data' => $parent_name]);
    }

    public function listOfCategoriesWithSingleParent(Request $request)
    {
    	$parent_id   =$request->input('parent_id');

    	 $validator = Validator::make($request->all(), [

    	 		'parent_id' => 'required',
    	 ]);

	    if($validator->fails()) {

	    	return response()->json(['status' => "901",'message' => $validator->messages()]);
	    }else{

	    	$categories_list = Categories::where('delete_status',0)
	    								 ->where('parent_id',$parent_id)
	    								 ->get();

	        return response()->json(['status' => "900",'data' => $categories_list]);
	    } 
	       
	}


	public function listOfCategories()
    {
    	
			$categories_list = Categories::where('delete_status',0)
										->orderBy('created_at', 'desc')
	    								 ->get();

	        return response()->json(['status' => "900",'data' => $categories_list]);
     }
}
