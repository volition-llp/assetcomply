<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;

class DocumentIdController extends Controller
{
    public function saveDocumentIdPattern(Request $request)
    {
        $name = $request->input('name');
        $format =$request->input('format');
        $details =$request->input('details');


        $validator = Validator::make($request->all(), [
                              'name'  =>'required',
                              'format' => 'required',
                              'details'  =>'required' 
                            ]);
              
                if ($validator->fails()) {

                      return response()->json(['status' => "901",'message' => $validator->messages()]);
              }

              $save_document_id_header = DB::table('docno_header')->insertGetId([
                                                    'name' => $name,
                                                    'format' => $format
                                                ]);
              $docid_header_id = $save_document_id_header;
              
            foreach ($details as $dts){
                DB::table('docno_values')->insert([
                   "doc_id" => $docid_header_id,
                   "order" => $dts['order'],
                   "type" => $dts['type'],
                   "value" => $dts['value'],
                   "current" => $dts['current'],
                ]);
            }
            
            Log::info("Asset Inserted");
            return response()->json(['status' => "900",
                                                      'message' => "Saved successfully"
                                                        ]);

    }
}
