<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Insurance;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Mail;

class InsuranceController extends Controller
{
    //

    public function createNewInsurance(Request $request)
    {
    	
    	try {
    		

    		$validator = Validator::make($request->all(), [
                    'insurance_id'  =>'required|unique:insurances,insurance_id',
                    'policy_no' => 'required',
                    'covered_by'  =>'required',
                    'start_date'  => 'required',
                    'end_date'  => 'required',
                    'insured_value'  => 'required',
                    'premium_amount'  => 'required',
                    'remind_before_days'  => 'required',
                    'frequency_of_reminder'  => 'required',
                    'document_upload'  => 'required|file|mimes:pdf,jpg,png',
                    'payment_remark'  => 'required',
                    'username'  => 'required',
                    'company'  => 'required',
                ]);


				if ($validator->fails()) {
					return response()->json(['status' => "901",'message' => $validator->messages()]);


				}else{

					$saveData = new Insurance;
					$saveData->insurance_id=$request->insurance_id;
					$saveData->policy_no=$request->policy_no;
					$saveData->covered_by=$request->covered_by;
					$saveData->start_date=$request->start_date;
					$saveData->end_date=$request->end_date;
					$saveData->insured_value=$request->insured_value;
					$saveData->premium_amount=$request->premium_amount;
					$saveData->remind_before_days=$request->remind_before_days;
					$saveData->frequency_of_reminder=$request->frequency_of_reminder;
					$saveData->payment_remark=$request->payment_remark;
					$saveData->record_added_by=$request->username;
					$saveData->company=$request->company;

					if($request->hasFile('document_upload')){

						 $insurance_image = $request->file('document_upload');
                    $directory = 'insurance/documents';
                    $filename = $request->insurance_id .'.'. $insurance_image->getClientOriginalExtension();


                    $path_catimg = $insurance_image->move($directory, $filename);

                    $saveData->document_upload = $filename;

					}

					$saveData->save();
					Log::info("Insurance created");
                     return response()->json(['status' => "900",
                                              'message' => "Insurance created"                                                      
                                                        ]);
					
				}

    
    	          	
    	} catch (Exception $e) {

    		return response()->json(['status' => "500",'message' => "Internal server error"]);
    		
    	}




    }

    public function deleteInsurance(Request $request)
    {
    	
    	try {
    		

    		$validator = Validator::make($request->all(), [
                    'insurance_id'  =>'required|exists:insurances,insurance_id',
                    
                ]);


				if ($validator->fails()) {
					return response()->json(['status' => "901",'message' => $validator->messages()]);


				}else{

					$deleteData =Insurance::where('insurance_id',$request->insurance_id)->delete();
					
					Log::info("Insurance deleted");
                     return response()->json(['status' => "900",
                                              'message' => "Insurance deleted"                                                      
                                                        ]);
					
				}

    
    	          	
    	} catch (Exception $e) {

    		return response()->json(['status' => "500",'message' => "Internal server error"]);
    		
    	}




    }

    /*insurance list*/
    public function insuranceList(Request $request)
    {
    	# code...

    	$validator = Validator::make($request->all(), [
                    
                    'company'  =>'required|exists:insurances,company',
                    'username'  =>'nullable|exists:insurances,record_added_by',
                    
                ]);


				if ($validator->fails()) {
					return response()->json(['status' => "901",'message' => $validator->messages()]);


				}else{

                    $search_filters=$request->search_filters;

					$InsuranceList=Insurance::select('insurance_id',                               'policy_no',                                       
                                                        'covered_by',
                                                        'start_date',
                                                        'end_date',
                                                        'insured_value',
                                                        'premium_amount',
                                                        'remind_before_days',
                                                        'frequency_of_reminder',
                                                        'document_upload',
                                                        'payment_remark',
                                                        'record_added_by as username',
                                                        'company')

                                            ->where('company',$request->company)
                                            ->whereNested(function($query) use ($search_filters) {
                                                foreach ($search_filters as $key => $value)
                                                    {
                                                        if($value != ''){
                                                            $query->where($key,'LIKE','%'.$value.'%');
                                                        }
                                                    }
                                        })->orderBy('created_at', 'desc')
                                                ->paginate(10);

                    return response()->json(['status' => "200",'message' => "Records found",'data'=>$InsuranceList]);


				}
    	
    }

    //*edit insurance*/
    public function editInsurance(Request $request)
    {
    
        $validator = Validator::make($request->all(), [
                    'insurance_id'  =>'required|exists:insurances,insurance_id',
                    'policy_no' => 'required',
                    'covered_by'  =>'required',
                    'start_date'  => 'required',
                    'end_date'  => 'required',
                    'insured_value'  => 'required',
                    'premium_amount'  => 'required',
                    'remind_before_days'  => 'required',
                    'frequency_of_reminder'  => 'required',
                   
                    'payment_remark'  => 'required',
                    'username'  => 'required',
                    'company'  => 'required',
                ]);


                if ($validator->fails()) {
                    return response()->json(['status' => "901",'message' => $validator->messages()]);


                }else{

                    $upedateData=Insurance::where('insurance_id',$request->insurance_id)
                    ->update(['policy_no'=>$request->policy_no, 'covered_by' =>$request->covered_by,'start_date' =>$request->start_date,'end_date'=>$request->end_date,'insured_value' =>$request->insured_value,
                        'premium_amount' => $request->premium_amount,'remind_before_days'  =>$request->remind_before_days,'frequency_of_reminder' =>$request->frequency_of_reminder,'payment_remark' =>$request->payment_remark]);

                    return response()->json(['status' => "200",'message' => "records updated"]);

                }



    }

    

    /*view document*/
    public function viewOrEditDoc(Request $request)
    {

         $validator = Validator::make($request->all(), [
                    'insurance_id'  =>'required|exists:insurances,insurance_id',
                    'document_upload'  => 'nullable|file|mimes:pdf,jpg,png',
                    
                ]);


                if ($validator->fails()) {
                    return response()->json(['status' => "901",'message' => $validator->messages()]);


                }
                if($request->hasFile('document_upload')){

                         $insurance_image = $request->file('document_upload');
                    $directory = 'insurance/documents';
                    $filename = $request->insurance_id .'.'. $insurance_image->getClientOriginalExtension();


                    $path_catimg = $insurance_image->move($directory, $filename);

                    $saveData=Insurance::where('insurance_id',$request->insurance_id)
                                ->update(['document_upload'=>$filename]);

                    return url("insurance/documents/".$filename);

                    }
                    else{

                        $getfile=Insurance::where('insurance_id',$request->insurance_id)->first();

                        return url("insurance/documents/".$getfile->document_upload);
                    }
        

    }


    




}

