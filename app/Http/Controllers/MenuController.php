<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menus;
use Validator;

class MenuController extends Controller
{
    public function addMenuItems(Request $request)
    {
        $menu_name = $request->input('menu_name');
        $menu_slug = $request->input('menu_slug');
        $menu_url = $request->input('menu_url');
        $parent_menu = $request->input('parent_menu');

        $rules = [
                    'menu_name' =>'required',
                    'menu_slug' =>'required',                    
                    'menu_url' =>'required',
                    'parent_menu' =>'required'
                ];
       
        
            $response = array('response' => '', 'success'=>false);
            $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                
                    return response()->json(['status' => "901",'message' => $validator->messages()]);

                }else{

                    $check_same_menu = Menus::where('menu_slug',$menu_slug)
                                              ->first();
                            if(empty($check_same_menu)){

                                $menu_data = new Menus();
                                $menu_data->menu_name = $menu_name;
                                $menu_data->menu_slug = $menu_slug;
                                $menu_data->menu_url = $menu_url;
                                $menu_data->parent_menu = $parent_menu;
                                $menu_data->activate_status = 1;
                                $menu_data->save();

                                return response()->json(['status' => "900",'message' => "Menu added"]);

                            }else {

                                return response()->json(['status' => "901",'message' => "Menu Slug value will be different"]);

                            }

                }
    
    
    
    }

    public function menuItemList()
    {
        $menu_list = Menus::where('activate_status',1)
                            ->get();

    //    $cat_list = array();
    //    $parent_name;
    //      foreach($menu_list as $category)
    //      {
    //      	// if($category->parent_menu == "null"){
    //      		$parent_name[$category->menu_slug] = $category->menu_name;

    //      	// }
    //      }
         
    
    //      foreach($menu_list as $category)
    //      {
    //      	foreach($parent_name as $key => $val){
    //      	     if($key == $category->parent_menu){

    //      	     	$cat_list[$key][] = $category->menu_name;

    //      	     }
    //          }         	
    //      }

         foreach($menu_list as $menu)
         {
         	// if($category->parent_menu == "null"){
                 $menu_lists[] = array('name' => $menu->menu_name,
                                       'slug' => $menu->menu_slug,
                                        'url' => $menu->menu_url,
                                        'parent_menu' =>$menu->parent_menu);

         	// }
         }

        return response()->json(['status' => "900",
                                  'message'=>'Menu list retrived',
                                  'data'=>$menu_lists]);
    }
}
