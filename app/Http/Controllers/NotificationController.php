<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Userdetails;
use App\AssetInfo;
use App\SubUser;

class NotificationController extends Controller
{
    public function getUserInformationFromDB($company_id)
    {

        $get_user_info = Userdetails::where('company', strtolower($company_id))
                                        ->where('payment_status',1)
                                        ->first();
        return $get_user_info;
    }
    public function checkCountOfAssets(Request $request)
    {
        $company_id = $request->input('company_id');
        $get_user_info = $this->getUserInformationFromDB($company_id);
        
        $count_of_assets = $get_user_info->no_of_assets; //from reg table

        $get_assets_count = AssetInfo::where('company', $company_id)->where('status',1)->count(); //from asset table

        if($count_of_assets >= $get_assets_count){

            return response()->json(['status' => '901']);
        
        }else{
            return response()->json(['status' => "900",
                                        'message' => "You exceed your Assets Limit"]);
        }
    }

    public function checkCountOfSubUsers(Request $request)
    {
        $company_id = $request->input('company_id');

        $get_user_info = $this->getUserInformationFromDB($company_id);

     
        $count_of_subusers = $get_user_info->no_of_subusers;

       
        $get_subusers_count = SubUser::where('company', $company_id)->count();

        if($count_of_subusers  >= $get_subusers_count){

            return response()->json(['status' => "901"]); 

        }else{
            return response()->json(['status' => "900",
            'message' => "You exceed your Sub Users Limit"]);
        }
    }
}
