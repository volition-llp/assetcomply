<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use App\AssetInfo;
use Illuminate\Support\Facades\Storage;
use App\Imports\AssetImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Locations;
use App\AssetsVerification;
use App\VerifiedAssetEndMapping;
use Webpatser\Uuid\Uuid;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Userdetails;
use App\Notifications\AssetVerificationScheduleNotify;


class ProcessManagementController extends Controller
{
    public function initializeAssetVerification(Request $request)
    {
        $validator = Validator::make($request->all(), [
                                    'company_id'  =>'required|exists:userdetails,company',
                                    'location_id' => 'required|exists:locations,loc_id'

                                ]);
            
            // $from_dt = Carbon::parse('first day of April '.$year)->isoFormat('YYYY');
            // $to_dt = Carbon::parse('last day of March '.$nextYr)->add('1 year')->isoFormat('YYYY');
           

            $company_id =$request->input('company_id');
            $location_id = $request->input('location_id');  
                          
            $verification_name = $request->verification_name;                    
            $proposed_start_date = $request->proposed_start_date;                    
            $proposed_end_date = $request->proposed_end_date;


                    
        
        // check the year already verified or not

            $check_loc_verified_or_not = VerifiedAssetEndMapping::where('location_id',$location_id)
                                                                  ->where('company_id',$company_id)
                                                                  ->where('verified_status',2)
                                                                  ->first();
            $check_loc_scheduled_or_not = VerifiedAssetEndMapping::where('location_id',$location_id)
                                                                  ->where('company_id',$company_id)
                                                                  ->where('verified_status',0)
                                                                  ->first();
            if ($validator->fails()) {
    
                return response()->json(['status' => "901",'message' => $validator->errors()]);
                                                            
            }else {
                                              
               try{                                                      
                        if(empty($check_loc_verified_or_not) && empty($check_loc_scheduled_or_not)){

                                $data_from_asset_info = AssetInfo::select('asset_id')
                                                                ->where('company',$company_id)
                                                                ->where('location',$location_id)
                                                                ->get();
                                                                
                                $verified_asset_end_mapping_uuid=Uuid::generate()->string;


                                    // 1 = verified
                                    // 0 = not verified
                                $proposed_end_date=Carbon::parse($proposed_end_date);
                                $proposed_start_date=Carbon::parse($proposed_start_date);




                                foreach($data_from_asset_info as $assetdata)
                                {
                                    $asset_data_into_verification_phase = new AssetsVerification();
                                    $asset_data_into_verification_phase->uuid = Uuid::generate()->string;
                                    $asset_data_into_verification_phase->asset_id = $assetdata->asset_id;
                                    $asset_data_into_verification_phase->asset_map_id = $verified_asset_end_mapping_uuid;
                                    $asset_data_into_verification_phase->dt = "0";
                                    $asset_data_into_verification_phase->location_id = $location_id;
                                    $asset_data_into_verification_phase->company_id = $company_id;
                                    $asset_data_into_verification_phase->verified_status = 0;
                                   
                                    $asset_data_into_verification_phase->save();
                                }
                                    // 2 = verification on going
                                    // 1 = verification ended
                                    // 0 = Not Started/scheduled

                                    // setting up the Scheduled verification process
                                    $change_asset_verification_status = new VerifiedAssetEndMapping();
                                    $change_asset_verification_status->uuid = $verified_asset_end_mapping_uuid;
                                    $change_asset_verification_status->dt = Carbon::now();
                                    $change_asset_verification_status->location_id = $location_id;
                                    $change_asset_verification_status->company_id = $company_id;
                                    $change_asset_verification_status->verified_status = 0;
                                    
                                    $change_asset_verification_status->verification_name=$verification_name;
                                    $change_asset_verification_status->proposed_start_date=$proposed_start_date;
                                    $change_asset_verification_status->proposed_end_date=$proposed_end_date;

                                    $change_asset_verification_status->save();

                                    $user_details= Userdetails::where('company',$company_id)->first();
                                    $schedule_info = VerifiedAssetEndMapping::where('uuid',$verified_asset_end_mapping_uuid)->first();
                                    
                                    $user_details->notify(new AssetVerificationScheduleNotify($schedule_info));
                                    // $user->notifyAt(new AssetVerificationScheduleNotify($schedule_info), Carbon::parse($schedule_info->proposed_start_date));

                                    return response()->json(['status' => "900",
                                                            'message' => "Verification is scheduled",
                                                            'asset_map_id'=>$verified_asset_end_mapping_uuid]);

                        }else{

                            $prev_uuid = VerifiedAssetEndMapping::select('uuid')
                                                                  ->where('location_id',$location_id)
                                                                  ->where('company_id',$company_id)
                                                                  ->where('verified_status',2)
                                                                  ->first();

                            return response()->json(['status' => "901",
                                                     'prev_asset_map_id' => $prev_uuid->uuid,
                                                    'message' => "Previous year verification is not done fully/scheduled."]);
                        }   




                }catch (\Exception $e) {
                    Log::critical("Asset Module Exception: ".$e->getMessage());
                    return response()->json(['status' => "902",'message' => "Exception Occured"]);
                }

        }
    }
    
    public function listOfActiveVerificationSchedule(Request $request) {
             $location_id = $request->input('location_id');
             $company_id = $request->input('company_id');

             try{

                $validator = Validator::make($request->all(), [
                      'company_id'  =>'required|exists:userdetails,company',
                      'location_id' => 'required|exists:locations,loc_id'
                ]);

                
                if ($validator->fails()) {

                    return response()->json(['status' => "901",'message' => $validator->errors()]);
                                                
                }


                 $schedule_verification_data = VerifiedAssetEndMapping::where('company_id',$company_id)
                                                                        ->where('location_id',$location_id)
                                                                        ->where('verified_status',0)
                                                                        ->orwhere('verified_status',2)
                                                                        ->first();
                return response()->json(['status' => "900",
                                          'schedule_verification_data' => $schedule_verification_data,
                                          'message' => "Records Found"]);




                }catch (\Exception $e) {

                    Log::error("List of Asset Verification Module Exception",$e->getMessage());
                    return response()->json(['status' => "902",'message' => "Internal Server Error "]);
                }
    }
    
    
    public function assetVerificationProcessStatusUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
                                        'company_id'  =>'required|exists:assets_verification,company_id',
                                        'location_id' => 'required|exists:assets_verification,location_id',
                                        'asset_id' => 'required|exists:assets_verification,asset_id',
                                        'asset_verify_map_uuid' => 'required|exists:verified_asset_end_mapping,uuid'
                                    ]);

                    
                $asset_id = $request->input('asset_id');
                $company_id =$request->input('company_id');
                $location_id = $request->input('location_id');                    
                $dt = Carbon::now()->toDateString();                    
                $asset_verify_map_uuid = $request->input('asset_verify_map_uuid');                    

                if ($validator->fails()) {

                        return response()->json(['status' => "901",'message' => $validator->errors()]);
                                                    
                }else{

                        try{
                                        $asset_verify_recheck = AssetsVerification::where('location_id',$location_id)
                                                                        ->where('company_id',$company_id)
                                                                        ->where('asset_map_id',$asset_verify_map_uuid)
                                                                        ->where('asset_id',$asset_id)
                                                                        ->where('verified_status',1)
                                                                        ->first();

                                        if(empty($asset_verify_recheck)){
                                            // 1 = verified
                                        // 0 = not verified
                                        $asset_verification = AssetsVerification::where('location_id',$location_id)
                                                                                    ->where('company_id',$company_id)
                                                                                    ->where('asset_id',$asset_id)
                                                                                    ->where('asset_map_id',$asset_verify_map_uuid)
                                                                                    ->update(['verified_status'=>1,'dt'=>$dt]);

                                        $update_verification_process = VerifiedAssetEndMapping::where('location_id',$location_id)
                                                                                    ->where('company_id',$company_id)
                                                                                    ->where('uuid',$asset_verify_map_uuid)
                                                                                    ->update(['verified_status'=>2]);

                                            return response()->json(['status' => "900",'message' => "Asset Verified (".$asset_id.")"]);
                                        }else{
                                            return response()->json(['status' => "903",'message' => "Asset Verified (".$asset_id.") already"]);
                                       
                                        }
                                        
                                    

                            
                       
                        }catch (\Exception $e) {
                            Log::error("Asset Verification Module Exception",$e->getMessage());
                            return response()->json(['status' => "902",'message' => "Internal Server Error "]);
                        }
                       
                }
    }

    public function checkOngoingAssetVerificationStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
                                    'company_id'  =>'required|exists:assets_verification,company_id',
                                    'location_id' => 'required|exists:assets_verification,location_id',
                                ]);

                        $company_id =$request->input('company_id');
                        $location_id = $request->input('location_id');                    
                                           

             if ($validator->fails()) {

                 return response()->json(['status' => "901",'message' => $validator->errors()]);
             }else{

                $asset_verify_process_check = VerifiedAssetEndMapping::where('location_id',$location_id)
                                                                     ->where('company_id',$company_id)
                                                                     ->first();
                    if(empty($asset_verify_process_check)){

                        
                            return response()->json(['status' => "901",
                                                        'message' => "Asset information is mismatched"]); 
                        
                     
                    }else{

                        if($asset_verify_process_check->verified_status == 1){
                                    return response()->json(['status' => "900",
                                                'message' => "This Year Verification is ended",
                                                'verification_status'=>1]);  
                        }else{
                            return response()->json(['status' => "900",
                                            'message' => "Verification is on-going",
                                            'verification_status'=>2]); 
                        }   
                    }
                    
             }
    }

    public function totalAssetInVerificationQueue(Request $request) {

            $validator = Validator::make($request->all(), [
                                'company_id'  =>'required|exists:assets_verification,company_id',
                                'location_id' => 'required|exists:assets_verification,location_id',
                                'asset_verify_map_uuid' => 'required|exists:verified_asset_end_mapping,uuid'
                                ]);

            $company_id =$request->input('company_id');
            $location_id = $request->input('location_id'); 
            $asset_verify_map_uuid = $request->input('asset_verify_map_uuid');  
            

            $total_verified_assets = AssetsVerification::where('location_id',$location_id)
                                                        ->where('company_id',$company_id)
                                                        ->where('asset_map_id',$asset_verify_map_uuid)
                                                        ->where('verified_status',1)
                                                        ->count();

            $total_assets_for_verification = AssetsVerification::where('location_id',$location_id)
                                                                ->where('company_id',$company_id)
                                                                ->where('asset_map_id',$asset_verify_map_uuid)
                                                                ->count();

            return response()->json(['status' => "900",'message' => "Records Found",
                                     'total_verified_assets'=>$total_verified_assets,
                                     'total_assets_for_verification'=>$total_assets_for_verification]);


    }

    public function endAssetVerificationProcessStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
                                    'company_id'  =>'required|exists:assets_verification,company_id',
                                    'location_id' => 'required|exists:assets_verification,location_id',
                                    'asset_verify_map_uuid' => 'required|exists:verified_asset_end_mapping,uuid'
                                
                                    ]);

             $company_id =$request->input('company_id');
             $location_id = $request->input('location_id');                    
             $dt = Carbon::now()->toDateString(); 
             $asset_verify_map_uuid = $request->input('asset_verify_map_uuid');                   

            if ($validator->fails()) {

                return response()->json(['status' => "901",'message' => $validator->errors()]);
                                                
            }else{

                $asset_verification = VerifiedAssetEndMapping::where('location_id',$location_id)
                                                          ->where('company_id',$company_id)
                                                          ->where('uuid',$asset_verify_map_uuid)
                                                          ->update(['verified_status'=>1,'dt'=>$dt]);

                return response()->json(['status' => "900",'message' => "Verification Ended Successfully"]);
                                       
            }
    }


    public function discardAssetByLocation(Request $request) {

        $validator = Validator::make($request->all(), [
                        'company_id'  =>'required|exists:asset_info,company',
                        'asset_id' => 'required|exists:asset_info,asset_id',
                        'discarded_by' => 'required',
                        'location_id' => 'required|exists:locations,loc_id'
                    
                        ]);

            $company_id =$request->input('company_id');                    
            $dt = Carbon::now()->toDateTimeString(); 
            $asset_id = $request->input('asset_id');                   
            $discarded_by = $request->input('discarded_by');                   
            $location_id = $request->input('location_id');                   

            if ($validator->fails()) {

                         return response()->json(['status' => "901",'message' => $validator->errors()]);
                                    
            }else{
                    try{
                    
                    $check_asset_already_discarded = DB::table('asset_discarded')
                                                            ->where('asset_id', $asset_id)
                                                            ->where('company_id', $company_id)
                                                            ->first();
                    if(empty($check_asset_already_discarded)){ 
                        if($location_id != null){

                            $discarded_asset = DB::table('asset_info')
                                                    ->where('asset_id', $asset_id)
                                                    ->where('company', $company_id)
                                                    ->update(['status' => 0]);

                            $insert_discarded_transaction = DB::table('asset_discarded')->insert([
                                                        'uuid' => Uuid::generate()->string,
                                                        'asset_id'  => $asset_id,
                                                        'company_id'  => $company_id,
                                                        'date'  => $dt,
                                                        'location_id'  =>  $location_id ,
                                                        'discarded_by'  => $discarded_by ,
                                                        'created_at' => Carbon::now()->toDateTimeString(),
                                                        'updated_at' => Carbon::now()->toDateTimeString()
                                                    ]);
                        }else{

                            $discarded_asset = DB::table('asset_info')
                                                    ->where('asset_id', $asset_id)
                                                    ->where('company', $company_id)
                                                    ->update(['status' => 0]);

                            $insert_discarded_transaction = DB::table('asset_discarded')->insert([
                                                        'uuid' => Uuid::generate()->string,
                                                        'asset_id'  => $asset_id,
                                                        'company_id'  => $company_id,
                                                        'date'  => $dt,
                                                        'location_id'  => null ,
                                                        'discarded_by'  => $discarded_by ,
                                                        'created_at' => Carbon::now()->toDateTimeString(),
                                                        'updated_at' => Carbon::now()->toDateTimeString()
                                                    ]);
                        }

                        return response()->json(['status' => "900",'message' => "Asset is discarded"]);
                    }else{
                        return response()->json(['status' => "901",'message' => "Asset is already discarded"]);
                    }
                            

                        
                    }catch (\Exception $e) {
                        Log::critical("Asset Module Exception".$e->getMessage());
                        return response()->json(['status' => "902",'message' => "Exception Occured"]);
                    }
                                     
            }
    }
}
