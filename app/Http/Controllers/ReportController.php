<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AssetInfo;
use App\AssetsVerification;
use App\Locations;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use App\Exports\LocationWiseAssetReportExport;
use App\VerifiedAssetEndMapping;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class ReportController extends Controller
{
    public function locationWiseAssetReport(Request $request)
    {
        $location_id = $request->input('location_id');
        $company_id = $request->input('company_id');
        $page_size = $request->input('page_size');

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $emp_id = $request->input('emp_id');

        $country = $request->input('country');
        $city = $request->input('city');
        $state = $request->input('state');
        
        $validator = Validator::make($request->all(), [
                              'location_id'  =>'nullable|exists:locations,loc_id',
                              'company_id' => 'required|exists:userdetails,company',
                              'page_size' => 'required'
                            ]);

              
                if ($validator->fails()) {

                      return response()->json(['status' => "901",'message' => $validator->messages()]);
                
                }else {
                    
                    try{
                        
                  
                        $asset_data = DB::table('asset_info')
                                                ->where('company',strtolower($company_id))
                                                ->join('locations', 'asset_info.location', '=', 'locations.loc_id')
                                                ->join('categories', 'asset_info.category', '=', 'categories.category_id')
                                                ->leftJoin('categories AS subcat' , 'asset_info.sub_category', '=', 'subcat.category_id')
                                                ->join('vendors', 'asset_info.vendor_id', '=', 'vendors.id')
                                                ->join('asset_type', 'asset_info.asset_type', '=', 'asset_type.asset_type_id')
                                                ->select(
                                                    'asset_info.asset_name',
                                                    'asset_info.asset_id',
                                                    'asset_type.name as asset_type_name',
                                                    'categories.name as category',
                                                    'subcat.name as sub_category',
                                                    'locations.location_name',
                                                    'locations.city',
                                                    'locations.state',
                                                    'locations.country',
                                                    'asset_info.brand',
                                                    'asset_info.date',
                                                    'asset_info.serial_number',
                                                    'asset_info.model',
                                                    'asset_info.description',                                           
                                                    'asset_info.warranty_date',                                           
                                                    'asset_info.condition',                                           
                                                    'vendors.vendor_name',                                           
                                                    'asset_info.po_number',                                           
                                                    'asset_info.invoice_number',                                           
                                                    'asset_info.invoice_date',                                           
                                                    'asset_info.purchase_price',                                           
                                                    'asset_info.purchase_date',                                           
                                                    'asset_info.capitalization_price',                                           
                                                    'asset_info.end_of_life',                                           
                                                    'asset_info.depreciation_method',                                           
                                                    'asset_info.depreciation_for_the_year',                                           
                                                    'asset_info.total_depreciation',                                           
                                                    'asset_info.capitalization_date',                                           
                                                    'asset_info.scrap_value',                                           
                                                    'asset_info.department'                                           
                                                    
                                                    )
                                                ->where(function($query) use ($from_date,$to_date){
                                                    if($from_date != '' && $to_date != ''){  
                                                            $query->whereBetween('asset_info.date', [$from_date, $to_date]);
                                                        }
                                                })
                                                ->where(function($query) use ($emp_id){
                                                     if($emp_id != ''){
                                                        $query->where('asset_info.emp_id',$emp_id);
                                                     }
                                                })
                                                ->where(function($query) use ($location_id){
                                                    if($location_id != ''){
                                                       $query->where('asset_info.location',$location_id);
                                                    }
                                               }) 

                                               ->where(function($query) use ($country){
                                                if($country != ''){
                                                   $query->where('locations.country',$country);
                                                }
                                           })
                                           ->where(function($query) use ($city){
                                               if($city != ''){
                                                  $query->where('locations.city',$city);
                                               }
                                          }) 
                                          ->where(function($query) use ($state){
                                            if($state != ''){
                                               $query->where('locations.state',$state);
                                            }
                                       })   
                                                ->orderBy('asset_info.date', 'desc')
                                                ->paginate($page_size);

                        return response()->json(['status' => "900",'message' => "Success",
                                                'data' => $asset_data]);

                    }catch (\Exception $e) {
                        Log::critical($e->getMessage());
                        return response()->json(['status' => "902",'message' => "Internal Server Error "],422);
                    }
                }
                      
    }

    public function assetVerificationReport(Request $request)
    {
        
        $company_id = $request->input('company_id');
        $location_id = $request->input('location_id');
        $from_date =$request->input('from_date');
        $to_date = $request->input('to_date');
        $page_size = $request->input('page_size');

        
        $validator = Validator::make($request->all(), [
                              'company_id' => 'required|exists:userdetails,company',
                              'page_size' => 'required'
                            ]);

              
                if ($validator->fails()) {

                      return response()->json(['status' => "901",'message' => $validator->messages()]);
                
                }else{
                      
                      try {
                        
                        $selected_cols = [  'verified_asset_end_mapping.uuid',
                                            'verified_asset_end_mapping.dt',
                                            'verified_asset_end_mapping.location_id',
                                            'verified_asset_end_mapping.company_id',
                                            'verified_asset_end_mapping.verified_status',
                                            'locations.city',
                                            'locations.state',
                                            'locations.country',
                                            'locations.location_name'];

                        $asset_data = DB::table('verified_asset_end_mapping')
                                            ->select($selected_cols)
                                            ->where(function($query) use ($location_id){
                                                      if($location_id != ''){
                                                        $query->where('verified_asset_end_mapping.location_id',$location_id);
                                                      }
                                            })
                                            ->where(function($query) use ($from_date, $to_date){
                                                      if($from_date != '' && $to_date != ''){
                                                          $query->whereBetween('verified_asset_end_mapping.dt',[$from_date,$to_date]);
                                                      }
                                            })
                                            ->join('locations','verified_asset_end_mapping.location_id', '=', 'locations.loc_id')
                                            ->where('verified_asset_end_mapping.company_id',$company_id)
                                            ->orderBy('verified_asset_end_mapping.created_at','desc')
                                            ->paginate($page_size);

                        return response()->json(['status' => "900",'message' => "Success",
                                            'data' => $asset_data]);
                      } catch (\Exception $e) {
                        Log::critical($e->getMessage());
                        return response()->json(['status' => "902",'message' => "Internal Server Error "]);
                      }
                }
       
                           
                                                
    }

    public function getEachAssetVerificationReport(Request $request){
              $company_id = $request->input('company_id');
              $city = $request->input('city');
              $state = $request->input('state');
              $country = $request->input('country');
              $from_date = $request->input('from_date');
              $to_date = $request->input('to_date');
              $asset_map_uuid = $request->input('asset_map_uuid');
              $page_size = $request->input('page_size');
              $location_id = $request->input('location_id');

              $validator = Validator::make($request->all(), [
                                                'company_id' => 'required|exists:userdetails,company',
                                                'page_size' => 'required'
                                            ]);


                if ($validator->fails()) {

                        return response()->json(['status' => "901",'message' => $validator->messages()]);
                
                }else{
                                try{
                                        $selected_cols = [  
                                            'asset_info.asset_id',
                                            'asset_info.asset_name',
                                            'categories.name as category',
                                            'subcat.name as sub_category',
                                            'locations.city',
                                            'locations.state',
                                            'locations.country',
                                            'locations.location_name',
                                            'assets_verification.verified_status',
                                            'assets_verification.dt'
                                                ];

                                        $asset_data = DB::table('assets_verification')
                                                          ->select($selected_cols)
                                                          ->where('assets_verification.company_id',$company_id)
                                                          ->where(function($query) use ($location_id){
                                                                   if($location_id != ''){
                                                                       $query->where('assets_verification.location_id',$location_id);
                                                                   }
                                                          })
                                                          ->where(function($query) use ($asset_map_uuid){
                                                                   if($asset_map_uuid != ''){
                                                                       $query->where('assets_verification.asset_map_id',$asset_map_uuid);
                                                                   }
                                                          })
                                                          ->where(function($query) use ($city){
                                                                   if($city != ''){
                                                                       $query->where('locations.city','LIKE','%'.$city.'%');
                                                                   }
                                                          })
                                                          ->where(function($query) use ($state){
                                                                   if($state != ''){
                                                                       $query->where('locations.state','LIKE','%'.$state.'%');
                                                                   }
                                                          })
                                                          ->where(function($query) use ($country){
                                                                   if($country != ''){
                                                                       $query->where('locations.country','LIKE','%'.$country.'%');
                                                                   }
                                                          })
                                                          ->where(function($query) use ($from_date,$to_date){
                                                            if($from_date != '' && $to_date != ''){
                                                                $query->whereBetween('assets_verification.dt',[$from_date,$to_date]);
                                                            }
                                                          })
                                                          ->join('asset_info','asset_info.asset_id','=','assets_verification.asset_id')
                                                          ->join('categories','categories.category_id','=','asset_info.category')
                                                          ->leftJoin('categories as subcat','subcat.category_id','=','asset_info.sub_category')
                                                          ->join('locations','locations.loc_id','=','assets_verification.location_id')
                                                          ->orderBy('assets_verification.created_at','desc')
                                                          ->paginate($page_size);


                                        return response()->json(['status' => "900",'message' => "Success",
                                                                    'data' => $asset_data]);                  

                                  }catch (\Exception $e) {
                                        Log::critical($e->getMessage());
                                        return response()->json(['status' => "902",'message' => "Internal Server Error "]);
                                    }
                 }


    }

    public function assetMovementReport(Request $request) {

        $company_id = $request->input('company_id');
        $from_date =$request->input('from_date');
        $to_date = $request->input('to_date');
        $page_size = $request->input('page_size');
        $movement_by = $request->input('movement_by');

        
        $validator = Validator::make($request->all(), [
                              'company_id' => 'required|exists:userdetails,company',
                              'page_size' => 'required'
                            ]);

              
                if ($validator->fails()) {

                      return response()->json(['status' => "901",'message' => $validator->messages()]);
                
                }else{

                    try{
                        $selected_cols = [
                                           'locations.location_name as from_location',
                                           'subloc.location_name as to_location',
                                           'asset_info.asset_id',
                                           'asset_info.asset_name',
                                           'asset_movement_history.remarks',
                                           'asset_movement_history.date',
                                           'asset_movement_history.email_notification',
                                           'asset_movement_history.movement_by',
                                           'vendors.vendor_name',
                                           'categories.name as category',
                                           'subcat.name as sub_category',
                                           'asset_info.brand',
                                           'asset_info.model',
                                           'asset_info.warranty_date',
                                           'asset_info.purchase_price',
                                           'asset_info.purchase_date',
                                           'asset_info.capitalization_price',
                                           'asset_info.capitalization_date',
                                           'asset_info.depreciation_method',
                                           'asset_info.depreciation_rate',
                                           'asset_info.depreciation_for_the_year',
                                           'asset_info.total_depreciation',
                                           'asset_info.status',
                                           'asset_info.emp_id',
                                          

                                        ];
                        $asset_data = \DB::table('asset_movement_history')->where('asset_movement_history.company_id',strtolower($company_id))
                                                ->join('asset_info' , 'asset_info.asset_id', '=', 'asset_movement_history.asset_id')
                                                ->join('categories' , 'categories.category_id', '=', 'asset_info.category')
                                                ->leftJoin('categories as subcat' , 'subcat.category_id', '=', 'asset_info.sub_category')
                                                ->join('vendors' , 'vendors.id', '=', 'asset_info.vendor_id')
                                                ->join('locations' , 'locations.loc_id', '=', 'asset_movement_history.from_location')
                                                ->join('locations as subloc' , 'subloc.loc_id', '=', 'asset_movement_history.to_location')
                                                ->where(function($query) use ($from_date,$to_date){
                                               if($from_date != '' && $to_date != ''){
                                            
                                                $query->whereBetween('asset_movement_history.date', [$from_date, $to_date]);
                                            
                                               } 
                                                
                                            })
                                            ->where(function($query) use ($movement_by){
                                                if($movement_by != ''){
                                             
                                                 $query->where('asset_movement_history.movement_by', $movement_by);
                                             
                                                } 
                                                 
                                             })
                                            ->select($selected_cols)
                                            ->orderBy('asset_movement_history.created_at', 'desc')
                                            ->paginate($page_size);

                        return response()->json(['status' => "900",'message' => "Success",
                                            'data' => $asset_data]);
                      } catch (\Exception $e) {
                        Log::critical($e->getMessage());
                        return response()->json(['status' => "902",'message' => "Internal Server Error "]);
                      }
                }
    }

    public function getAssetDiscardedReport(Request $request) {

    
        
        $validator = Validator::make($request->all(), [
            'company_id'  =>'required|exists:asset_info,company',
            'discarded_by' => 'nullable',
            'location_id' => 'nullable|exists:locations,loc_id',
            'page_size' => 'required'
        
            ]);

            $company_id =$request->input('company_id');                    
            $from_date =$request->input('from_date');
            $to_date = $request->input('to_date');
            $page_size = $request->input('page_size');                 
            $discarded_by = $request->input('discarded_by');                   
            $location_id = $request->input('location_id');  
            $city = $request->input('city');  
            $state = $request->input('state');  
            $country = $request->input('country');  

            if ($validator->fails()) {

                return response()->json(['status' => "901",'message' => $validator->messages()]);

            }else{

            try{

                $selected_cols = [  
                    'asset_info.asset_id',
                    'asset_info.asset_name',
                    'asset_info.brand',
                    'asset_info.model',
                    'asset_info.serial_number',
                    'categories.name as category',
                    'subcat.name as sub_category',
                    'locations.city',
                    'locations.state',
                    'locations.country',
                    'locations.location_name',
                    'asset_discarded.date'
                        ];

                $asset_data = DB::table('asset_discarded')
                                  ->select($selected_cols)
                                  ->where('asset_discarded.company_id',$company_id)
                                  ->where(function($query) use ($location_id){
                                           if($location_id != ''){
                                               $query->where('asset_discarded.location_id',$location_id);
                                           }
                                  })
                                  ->where(function($query) use ($discarded_by){
                                           if($discarded_by != ''){
                                               $query->where('asset_discarded.discarded_by',$discarded_by);
                                           }
                                  })
                                  ->where(function($query) use ($city){
                                           if($city != ''){
                                               $query->where('locations.city','LIKE','%'.$city.'%');
                                           }
                                  })
                                  ->where(function($query) use ($state){
                                           if($state != ''){
                                               $query->where('locations.state','LIKE','%'.$state.'%');
                                           }
                                  })
                                  ->where(function($query) use ($country){
                                           if($country != ''){
                                               $query->where('locations.country','LIKE','%'.$country.'%');
                                           }
                                  })
                                  ->where(function($query) use ($from_date,$to_date){
                                    if($from_date != '' && $to_date != ''){
                                        $query->whereBetween('asset_discarded.date',[$from_date,$to_date]);
                                    }
                                  })
                                  ->join('asset_info','asset_info.asset_id','=','asset_discarded.asset_id')
                                  ->join('categories','categories.category_id','=','asset_info.category')
                                  ->leftJoin('categories as subcat','subcat.category_id','=','asset_info.sub_category')
                                  ->join('locations','locations.loc_id','=','asset_discarded.location_id')
                                  ->orderBy('asset_discarded.date','desc')
                                  ->paginate($page_size);
             


                return response()->json(['status' => "900",'message' => "Success",
                                             'data' => $asset_data]);
                } catch (\Exception $e) {
                Log::critical($e->getMessage());
                return response()->json(['status' => "902",'message' => "Internal Server Error "]);
                }
            }

    } 


    public function getListofAssetMaintenanceTicketReport(Request $request){
        $company_id = $request->input('company_id');
        $page_size = $request->input('page_size');

        $asset_filters_arr = $request->input('asset_filters_arr');
        $location_filters_arr = $request->input('location_filters_arr');
        $location_id = $request->input('location_id');


        $validator = Validator::make($request->all(), [
                                  'company_id'  => 'required|exists:userdetails,company',
                                  'page_size'  => 'required'
                                  ]);


              if ($validator->fails()) {

                  return response()->json(['status' => "901",'message' => $validator->messages()]);

              }else{
                
                  $selected_cols = ['asset_maintenance.asset_id',
                                    'asset_info.asset_name',
                                    'asset_info.brand',
                                    'asset_info.serial_number',
                                    'locations.location_name',
                                    'locations.city',
                                    'locations.state',
                                    'locations.country',
                                    'asset_info.warranty_date',
                                    'vendors.vendor_name',

                                    ];
                  try{
                          $asset_maintenance_data = DB::table('asset_maintenance')
                                                        ->join('asset_info', 'asset_info.asset_id','=','asset_maintenance.asset_id')
                                                        ->join('locations', 'locations.loc_id','=','asset_maintenance.location_id')
                                                        ->join('vendors', 'vendors.id','=','asset_info.vendor_id')
                                                        ->select($selected_cols)
                                                        ->where('asset_maintenance.company_id',$company_id)
                                                        ->where(function($query) use ($location_id){
                                                            if($location_id != ''){
                                                                $query->where('asset_maintenance.location_id',$location_id);
                                                            }
                                                            
                                                        })
                                                        ->whereNested(function($query) use ($asset_filters_arr) {
                                                            foreach ($asset_filters_arr as $key => $value)
                                                                {
                                                                    if($value != ''){
                                                                        $query->where('asset_info.'.$key,'LIKE','%'.$value.'%');
                                                                    }
                                                                }

                                                        })
                                                        ->whereNested(function($query) use ($location_filters_arr) {
                                                            foreach ($location_filters_arr as $key => $value)
                                                                {
                                                                    if($value != ''){
                                                                        $query->where('locations.'.$key,'LIKE','%'.$value.'%');
                                                                    }
                                                                }

                                                        })
                                                        ->orderBy('asset_maintenance.created_at','desc')
                                                        ->paginate($page_size);

                          return response()->json(['status' => "900",'message' => "Asset maintenance list",
                                                   'data' =>$asset_maintenance_data]);
                  }catch (\Exception $e) {

                           Log::critical("Asset Maintenance Exception".$e->getMessage());
                           return response()->json(['status' => "902",'message' => "Internal Server Error "]);
                  }
              }

  }

}
