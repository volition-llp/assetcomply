<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Exports\LocationWiseAssetReportExport;
use App\Exports\LocationListExport;
use App\Exports\AssettypeListExport;
use App\Exports\VendorsListExport;
use App\Exports\CategoryListExport;
use App\Exports\DepreciationMethodsListExport;
use App\Exports\SubUsersListExport;
use App\Exports\AssetVerificationListExport;
use App\Exports\AssetMovementListExport;
use App\Exports\AssetDiscardedListExport;
use App\Exports\AssetMaintenanceListExport;
use App\Exports\UserguideExport;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Excel;

class ReportsExportController extends Controller
{
    public function exportLocationWiseAssetReport(Request $request) {

        $validator = Validator::make($request->all(), [
                                'location_id'  =>'nullable|exists:locations,loc_id',
                                'company_id' => 'required|exists:userdetails,company'
                            ]);
			
            $location_id = $request->input('location_id');
            $company_id = $request->input('company_id');
    
            $from_date = $request->input('from_date');
            $to_date = $request->input('to_date');
            $emp_id = $request->input('emp_id');
    
            $country = $request->input('country');
            $city = $request->input('city');
            $state = $request->input('state');
            
				if($validator->fails()) {
					return response()->json(['status'=>"901",'message' => $validator->errors()]);
				}

                    try{

                       
                        $data = \Excel::store(new LocationWiseAssetReportExport($location_id,
                                                                                $company_id,
                                                                                $from_date,
                                                                                $to_date,
                                                                                $emp_id,
                                                                                $country,
                                                                                $city,
                                                                                $state), 'location_wise_asset_report.xlsx');
                                  
                        $pathToFile = storage_path('app/location_wise_asset_report.xlsx');

                        return response()->file($pathToFile);

                    }catch (\Exception $e) {
                        Log::critical($e->getMessage());
                        return response()->json(['status' => "902",'message' => "Internal Server Error "]);
                    }
                
    }

    public function exportLocationList(Request $request) {

        $validator = Validator::make($request->all(), [
            'location_id'  =>'nullable|exists:locations,loc_id',
            'company_id' => 'required|exists:userdetails,company'
        ]);

        $location_id = $request->input('location_id');
        $company_id = $request->input('company_id');

        if($validator->fails()) {
            return response()->json(['status'=>"901",'message' => $validator->errors()]);
        }

        try{

                       
                $data = \Excel::store(new LocationListExport($location_id,$company_id), 
                                        'location_list_export.xlsx');
                        
                $pathToFile = storage_path('app/location_list_export.xlsx');

                return response()->file($pathToFile);

        }catch (\Exception $e) {
                Log::critical($e->getMessage());
                return response()->json(['status' => "902",'message' => "Internal Server Error "]);
        }

    }

    public function exportAssetTypeList() {

             try{

                        
                    $data = \Excel::store(new AssettypeListExport(), 
                                            'assettype_list_export.xlsx');
                            
                    $pathToFile = storage_path('app/assettype_list_export.xlsx');

                    return response()->file($pathToFile);

            }catch (\Exception $e) {
                    Log::critical($e->getMessage());
                    return response()->json(['status' => "902",'message' => "Internal Server Error "]);
            }

    }

    /*public function exportVendorsList(Request $request) {

        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:userdetails,company',
            'added_by' => 'required'
        ]);

            
        $company_id = $request->input('company_id');
        $added_by = $request->input('added_by');

        if($validator->fails()) {
            return response()->json(['status'=>"901",'message' => $validator->errors()]);
        }

        try{



                       
                $data = \Excel::store(new UserguideExport($company_id,$added_by), 
                                        'vendor_list_export.xlsx');
                        
                $pathToFile = storage_path('app/vendor_list_export.xlsx');

                return response()->file($pathToFile);

        }catch (\Exception $e) {
                Log::critical($e->getMessage());
                return response()->json(['status' => "902",'message' => "Internal Server Error "]);
        }

    }*/

    public function exportCategoryList() {

        try{

                       
 /*               $data = \Excel::store(new CategoryListExport(), */
                $data = \Excel::store(new UserguideExport(), 
                                        'category_list_export.xlsx');
                        
                $pathToFile = storage_path('app/category_list_export.xlsx');

                return response()->file($pathToFile);

        }catch (\Exception $e) {
                Log::critical($e->getMessage());
                return response()->json(['status' => "902",'message' => "Internal Server Error "]);
        }

    }

    public function exportDepreciationList(/*Request $request*/) {

        try{

                       
                $data = \Excel::store(new DepreciationMethodsListExport(), 
                                        'depreciation_list_export.xlsx');
                        
                $pathToFile = storage_path('app/depreciation_list_export.xlsx');

                return response()->file($pathToFile);

        }catch (\Exception $e) {
                Log::critical($e->getMessage());
                return response()->json(['status' => "902",'message' => "Internal Server Error "]);
        }

    }

    public function exportSubUsersList(Request $request) {
       
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:userdetails,company'
        ]);

            
        $company_id = $request->input('company_id');
        $location_id = $request->input('location_id');
        $usertype = $request->input('usertype');

        if($validator->fails()) {
            return response()->json(['status'=>"901",'message' => $validator->errors()]);
        }

        try{

                       
                $data = \Excel::store(new SubUsersListExport($company_id,$location_id,$usertype), 
                                        'subusers_list_export.xlsx');
                        
                $pathToFile = storage_path('app/subusers_list_export.xlsx');

                return response()->file($pathToFile);

        }catch (\Exception $e) {
                Log::critical($e->getMessage());
                return response()->json(['status' => "902",'message' => "Internal Server Error "]);
        }

    }

    public function exportAssetMovementReport(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:userdetails,company'
        ]);

        $company_id = $request->input('company_id');
        $movement_by = $request->input('movement_by');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');

            if($validator->fails()) {
                return response()->json(['status'=>"901",'message' => $validator->message()]);
            }

            try{

                        
                    $data = \Excel::store(new AssetMovementListExport($company_id,$movement_by,$from_date,$to_date), 
                                            'asset_movement_list_export.xlsx');
                            
                    $pathToFile = storage_path('app/asset_movement_list_export.xlsx');

                    return response()->file($pathToFile);

            }catch (\Exception $e) {
                    Log::critical($e->getMessage());
                    return response()->json(['status' => "902",'message' => "Internal Server Error "]);
            }
    }

    public function exportEachAssetVerificationReport(Request $request)
    {
        $company_id = $request->input('company_id');
        $city = $request->input('city');
        $state = $request->input('state');
        $country = $request->input('country');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $asset_map_uuid = $request->input('asset_map_uuid');
        $location_id = $request->input('location_id');

        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:userdetails,company'
        ]);

                if($validator->fails()) {
                    return response()->json(['status'=>"901",'message' => $validator->message()]);
                }

                try{

                            
                        $data = \Excel::store(new AssetVerificationListExport(
                                                    $company_id,
                                                    $city,
                                                    $state,
                                                    $country,
                                                    $from_date,
                                                    $to_date,
                                                    $asset_map_uuid,
                                                    $location_id,
                                                ), 
                                                'asset_verification_list_export.xlsx');
                                
                        $pathToFile = storage_path('app/asset_verification_list_export.xlsx');

                        return response()->file($pathToFile);

                }catch (\Exception $e) {
                        Log::critical($e->getMessage());
                        return response()->json(['status' => "902",'message' => "Internal Server Error "]);
                }
    }


    public function exportAssetDiscardedReport(Request $request)
    {
       $validator = Validator::make($request->all(), [
        'company_id'  =>'required|exists:asset_info,company',
        'discarded_by' => 'nullable',
        'location_id' => 'nullable|exists:locations,loc_id'
    
        ]);

        $company_id =$request->input('company_id');                    
        $from_date =$request->input('from_date');
        $to_date = $request->input('to_date');                 
        $discarded_by = $request->input('discarded_by');                   
        $location_id = $request->input('location_id');  
        $city = $request->input('city');  
        $state = $request->input('state');
        $country = $request->input('country');  

        if ($validator->fails()) {

            return response()->json(['status' => "901",'message' => $validator->messages()]);

        }else{

        try{

            $data = \Excel::store(new AssetDiscardedListExport(
                                        $company_id,
                                        $city,
                                        $state,
                                        $country,
                                        $from_date,
                                        $to_date,
                                        $discarded_by,
                                        $location_id,
                                    ), 
                                    'asset_discarded_list_export.xlsx');

                $pathToFile = storage_path('app/asset_discarded_list_export.xlsx');

              return response()->file($pathToFile);
            } catch (\Exception $e) {
            Log::critical($e->getMessage());
              return response()->json(['status' => "902",'message' => "Internal Server Error "]);
            }
        }
    }

    public function bulkAssetUploadSampleFileDownload() {
        $pathToFile = storage_path('app/bulk_assets_sample.xlsx');

              return response()->file($pathToFile);
    }

    public function exportListofAssetBreakdownTicketReport(Request $request) {

        $company_id = $request->input('company_id');    

        $asset_filters_arr = $request->input('asset_filters_arr');
        $location_filters_arr = $request->input('location_filters_arr');
        $location_id = $request->input('location_id');


        $validator = Validator::make($request->all(), [
                                  'company_id'  => 'required|exists:userdetails,company'
                                  ]);


              if ($validator->fails()) {

                  return response()->json(['status' => "901",'message' => $validator->messages()]);

              }else{

                try{
        
                    $data = \Excel::store(new AssetBreakdownListExport(
                                                $company_id,
                                                $asset_filters_arr,
                                                $location_filters_arr,
                                                $location_id
                                            ), 
                                            'asset_maintenance_list_export.xlsx');
        
                        $pathToFile = storage_path('app/asset_maintenance_list_export.xlsx');
        
                      return response()->file($pathToFile);
                    } catch (\Exception $e) {
                    Log::critical($e->getMessage());
                      return response()->json(['status' => "902",'message' => "Internal Server Error "]);
                    }
                }



    }

    /*get all  excel together*/

    public function exportUserGuide(Request $request) {

        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:userdetails,company',
            'added_by' => 'required'
        ]);

            
        $company_id = $request->input('company_id');
        $added_by = $request->input('added_by');

        if($validator->fails()) {
            return response()->json(['status'=>"901",'message' => $validator->errors()]);
        }

        try{

            

                       
                $data = \Excel::store(new UserguideExport($company_id,$added_by), 
                                        'vendor_list_export.xlsx');
                        
                $pathToFile = storage_path('app/vendor_list_export.xlsx');

                return response()->file($pathToFile);

        }catch (\Exception $e) {
                Log::critical($e->getMessage());
                return response()->json(['status' => "902",'message' => "Internal Server Error "]);
        }

    }
    /**/

    

}
