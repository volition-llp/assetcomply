<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Userdetails;
use App\User;
use App\SubUser;
use App\AssetInfo;
use App\Locations;
use App\AssetBreakdown;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class StatisticsController extends Controller
{
    public function verifyCompanyIdWithUsername($company_id,$username="") {

         $verify_cmp = Userdetails::where('company',$company_id)
                                    ->where(function($query) use ($username){
                                      if($username != ''){
                                        $query->where('username',$username);
                                          }
                                     })
                                    ->count();
         if($verify_cmp > 1 || $verify_cmp < 1){
             return 0;
         }else{
             return 1;
         }
           
    }
    public function numbersOfAssets(Request $request) {
          
      $company_id= $request->input('company_id');
      $username =$request->input('username');
      $location_id =$request->input('location_id');

        //varify company and username
        $res_verify_cmp_uname = $this->verifyCompanyIdWithUsername($company_id,$username);

          if($res_verify_cmp_uname) {

            $count_of_assets = AssetInfo::where('status',1) 
                                         ->where('company',$company_id)
                                         ->where(function($query) use ($location_id){
                                              if($location_id != ''){
                                                $query->where('location',$location_id);
                                              }
                                         })
                                         ->count();
            
              return response()->json(['status' => "900",
                                         'data' => $count_of_assets,
                                       ]);                          


          }else{
                           
            return response()->json(['status' => "901",
                                    'message' => "Company information is mismatched",
                                    ]);
          }
    }

    public function numbersOfAssetsLocationwise(Request $request) {
          
      $company_id= $request->input('company_id');
      $username =$request->input('username');
      $location_id =$request->input('location_id');

        //varify company and username
        $res_verify_cmp_uname = $this->verifyCompanyIdWithUsername($company_id,$username);

          if($res_verify_cmp_uname) {

            $count_of_assets = DB::table('asset_info')
                                         ->join('locations','locations.loc_id','=','asset_info.location')
                                         ->where('asset_info.status',1) 
                                         ->groupBy('locations.location_name')
                                         ->selectRaw('COUNT(asset_info.asset_id) as assets,locations.location_name')
                                         ->where('asset_info.company',$company_id)
                                         ->cursor();
            
              return response()->json(['status' => "900",
                                         'data' => $count_of_assets,
                                       ]);                          


          }else{
                           
            return response()->json(['status' => "901",
                                    'message' => "Company information is mismatched",
                                    ]);
          }
    }


    public function numbersOfLocations(Request $request) {
          
      $company_id= $request->input('company_id');

        //varify company and username
        $res_verify_cmp_uname = $this->verifyCompanyIdWithUsername($company_id);

          if($res_verify_cmp_uname) {

            $count_of_loc = Locations::where('company_id',$company_id)
                                         ->count();
            
              return response()->json(['status' => "900",
                                         'data' => $count_of_loc,
                                       ]);                          


          }else{
                           
            return response()->json(['status' => "901",
                                    'message' => "Company information is mismatched",
                                    ]);
          }
    }

    public function numbersOfSubusers(Request $request) {
          
      $company_id= $request->input('company_id');
      $username =$request->input('username');
      $location_id =$request->input('location_id');

        //varify company and username
        $res_verify_cmp_uname = $this->verifyCompanyIdWithUsername($company_id,$username);

          if($res_verify_cmp_uname) {

            $count_of_subuser = SubUser::where('company',$company_id)
                                        ->where('added_by',$username)
                                        ->where(function($query) use ($location_id){
                                          if($location_id != ''){
                                            $query->where('location',$location_id);
                                          }
                                          })
                                        ->count();
            
              return response()->json(['status' => "900",
                                         'data' => $count_of_subuser,
                                       ]);                          


          }else{
                           
            return response()->json(['status' => "901",
                                    'message' => "Company information is mismatched",
                                    ]);
          }
    }

    public function categoryWisePriceChart(Request $request) {

          $location_id = $request->input('location_id');
          $company_id = $request->input('company_id');  

          $records_category_wise = DB::table('asset_info')
                                        ->groupBy('asset_info.category')
                                        ->selectRaw('SUM(asset_info.purchase_price) as price,categories.category_id as category')
                                        ->join('categories','categories.category_id','=','asset_info.category')
                                        ->where(function($query) use ($location_id){
                                          
                                                if($location_id != ''){
                                                  $query->where('location',$location_id);
                                                }
                                        })
                                        ->where('asset_info.company',$company_id)
                                        ->get();
       
          return response()->json($records_category_wise);
    }


    public function locationWiseAssetsChart(Request $request) {


      $company_id = $request->input('company_id');  

      $records_location_wise = DB::table('asset_info')
                                    ->groupBy('asset_info.location')
                                    ->selectRaw('count(asset_info.asset_id) as assets,locations.loc_id')
                                    ->join('locations','locations.loc_id','=','asset_info.location')
                                    ->where('asset_info.company',$company_id)
                                    ->get();
   
      return response()->json($records_location_wise);
     }

     public function countOfAssetMaintenance(Request $request) {
        $company_id = $request->input('company_id');  
        $location_id = $request->input('location_id');  
        $issue_status = $request->input('issue_status');  

          $maintenance_records = DB::table('asset_breakdown')
                                                ->where('company_id',$company_id)
                                                ->where('issue_status',$issue_status)
                                                ->where(function($query) use ($location_id){
                                                    if($location_id != ''){
                                                         $query->where('location_id',$location_id);
                                                      }
                                                  })
                                                  ->count();
          return response()->json($maintenance_records);                                      
     }


}
