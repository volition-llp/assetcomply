<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubUser;
use Validator;
use App\User;
use App\Userdetails;
use Mail;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\VerifyUser;
use App\Mail\VerifyMail;
use Carbon\Carbon;
use App\UserTypeAccess;
use App\ResetPasswordLog;
use App\SysConfig;

class SubUserController extends Controller
{
    /* Sub User login*/
	public function subUserLogin(Request $request)
	{

		$username=$request->input('username');
		$password=$request->input('password');
        $company=$request->input('company');


		 $validator = Validator::make($request->all(), [
            		    'username' =>'required',
                        'password' =>'required',
                        'company' => 'required'                    
                 
			]);

            
        if ($validator->fails()) {
    
    	          	return response()->json(['status' => "901",'message' => $validator->messages()]);
          	
       		}else{

                $login_check = User::where('email', $username)->first();

                if(!empty($login_check))
                {
                      if(Hash::check($password,$login_check->password))
                      {
                                $check_cmpny = SubUser::where('company', strtolower($company))
                                                        ->where('username',$username)
                                                            ->first();
                                
                                if(!empty($check_cmpny)){

                                     $token = $login_check->createToken('Sub User Auth Token')->accessToken;
                                     $usertype=$check_cmpny['user_type'];

                                     $user_type_data = UserTypeAccess::where('company_id',$company)
															        ->where('user_type',$usertype)
																    ->get();
                                      $sysConfig = SysConfig::where('company_id',$company)
                                                                    ->first();
                                        $access_menus = array();
                                        foreach($user_type_data as $utd){

                                            if($utd['view'] != 0){
                                                $access_menus[] = $utd['menu_name'];
                                            }
                                        }
                                                                         
                                     Log::info("Logged In ". $username);

                                     return response()->json(['status' => "900",
                                                                'message' => "login successfull",
                                                                'token' => "Bearer ".$token,
                                                                'username'=>$check_cmpny['username'],
                                                                'company'=>$company,
                                                                'tagtype' => $sysConfig->asset_scan_output,
                                                                'access_menu' => $access_menus,
                                                                'usertype' => $usertype,
                                                                'location_id' => $check_cmpny['location']]);
                                }else{

                                     return response()->json(['status' => "906",'message' => "Company name is not valid"]); 
                                }
                                 

                        }
                        else
                        {
                            return response()->json(['status' => "906",'message' => "Wrong username or password"]); 
                        }

                      
                  }else{

                       return response()->json(['status' => "906",'message' => "Wrong username or password"]); 
                  }
                }
    }


/* End Sub User login*/
    public function resetSubUserPassword(Request $request)
    {
    $old_password = $request->input('old_password');
    $new_password =$request->input('new_password');
    $reconf_password =$request->input('reconf_password');

    $username =$request->input('username');
    $company_id =$request->input('company_id');
    
    $validator = Validator::make($request->all(), [
                            'old_password' =>'required',
                            'new_password' =>'required',
                            'reconf_password' => 'required',
                            'username' => 'required|exists:sub_users,username',
                            'company_id' => 'required|exists:sub_users,company'                    
                        
                    ]);


                if ($validator->fails()) {

                        return response()->json(['status' => "901",'message' => $validator->messages()]);

                }else{
                    
                        $user_old_pass_check = User::where('email', $username)
                                                    ->first();
                        $old_password_db = $user_old_pass_check->password;

                        if(Hash::check($old_password,$old_password_db))
                        {
                        $user_password_update = User::where('email', $username)
                                                        ->update(['password'=>Hash::make($new_password)]);
                            // set log of reset password customer
                        $user_reset_pass_log = new ResetPasswordLog();
                        $user_reset_pass_log->username = $username;
                        $user_reset_pass_log->company = $company_id;
                        $user_reset_pass_log->save();

                        return response()->json(['status' => "900",
                                                    'message' => "New Password is updated"]);

                        }else{

                            return response()->json(['status' => "901",'message' => "Not matching with your Old Password"]);
                        }
                }
    
    
    
    }
}
