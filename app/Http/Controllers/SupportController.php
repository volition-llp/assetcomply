<?php

namespace App\Http\Controllers; 

use Illuminate\Http\Request;
use Validator;
use App\SupportTicket;
use Carbon\Carbon;
use App\Userdetails;
use App\Mail\SupportTicketMail;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
 
class SupportController extends Controller
{
    public function createTicketForUser(Request $request)
    {    
      $issue_type = $request->input('issue_type');
      $priority = $request->input('priority');
      $issued_by = $request->input('issued_by');
      $location = $request->input('location');
      $subject = $request->input('subject');
      $msg = $request->input('msg');
      $company_id = $request->input('company_id');

          $validator = Validator::make($request->all(), [
                  'issue_type'  => 'required', //technical or financial
                  'priority'  =>'required',
                  'issued_by'  =>'required', // user's name/username
                  'location'  => 'required',
                  'subject'  => 'required',
                  'msg'  => 'required',
                  'company_id' => 'required',
                  // 'attachment'  =>'nullable|mimes:jpeg,jpg|max:1000', // No PNG is acceptable  / 1000 KB = 1 MB          
              ]);
        try{
          
        if ($validator->fails()) {
    
                return response()->json(['status' => "901",'message' => $validator->messages()]);
          
         }else {

              if($request->hasFile('attachment')){

                //  $tempimg = $request->file('attachment'); 
                //  $img_name = time() . '.' . $tempimg->getClientOriginalExtension();
                //  $tempimg->move('supportAttachements',$img_name);

                 $tempimg    = $request->file('attachment');
					       $img_name =  time() . '.' . $tempimg->getClientOriginalExtension();
					       $path = '/supportAttachements/';
					       $tempimg->storeAs($path,$img_name, 'local'); // saving the file in temp storage
		

              }else{
                 
                $img_name = '';
                         
              }
                $format1 = 'Y-m-d';
                $format2 = 'H:i:s';
                $date = Carbon::now()->format($format1);
                $time = Carbon::now()->format($format2);
                
                 $new_support_ticket = new SupportTicket();
                 $new_support_ticket->issue_type = $issue_type;
                 $new_support_ticket->priority = $priority;
                 $new_support_ticket->issued_by = $issued_by;
                 $new_support_ticket->location = $location;
                 $new_support_ticket->subject = $subject;
                 $new_support_ticket->msg = $msg;
                 $new_support_ticket->attachment = $img_name;
                 $new_support_ticket->date = $date;
                 $new_support_ticket->time = $time;
                 $new_support_ticket->status = 0;
                 $new_support_ticket->company = $company_id;
                 $new_support_ticket->save();


                  // Mail support ticket information to admin customer
                    $customer_data = Userdetails::where('company',$company_id)->first();
                    
                    $name=$customer_data->firstname.' '.$customer_data->lastname;
                    $email=$customer_data->email;

                    \Mail::to($email)->send(new SupportTicketMail($name,$new_support_ticket->id, $msg));
                    


                  return response()->json(['status' => "900",'message' => "Ticket submitted successfully",
                                          'ticket_code' => $new_support_ticket->id ]);
          }
        }catch(\Exception $e){
             Log::critical('Support Ticket'.$e->getMessage());
             return response()->json(['status' => "907",'message' => "Internal Server Error"]);
        }
    }
    
    public function getTicketsListForUser(Request $request)
    { 
      $company_id = $request->input('company_id');

      $validator = Validator::make($request->all(), [
          'company_id' => 'required',
        ]);
        try{
            if ($validator->fails()) {

                  return response()->json(['status' => "901",'message' => $validator->messages()]);

            }else {

                $get_all_tkt = SupportTicket::where('company',$company_id)
                                              ->orderBy('created_at','desc')
                                              ->paginate(10);
                
                return response()->json(['status' => "900",'message' => "Records found",
                                          'data' => $get_all_tkt ]);
            }


          }catch(\Exception $e){
            Log::error($e->getMessage());
            return response()->json(['status' => "907",'message' => $e->getMessage()]);
       }

    }

    public function getSupportAttachmentFileLink($imgname){

   
      $path = storage_path('app/supportAttachements/'.$imgname);

      if (!\File::exists($path)) {
          abort(404);
      }
    
        $file = \File::get($path);

          $type = \File::mimeType($path);

          $response = \Response::make($file, 200);

          $response->header("Content-Type", $type);

          return $response;
    }



    /*admin view of customer tickets*/
    public function customerTicketList(Request $request){

      try {

        $search_filters=$request->search_filters;

        $data=SupportTicket::select('*')
                ->whereNested(function($query) use ($search_filters) {
                                                foreach ($search_filters as $key => $value)
                                                    {
                                                        if($value != ''){
                                                            $query->where($key,'LIKE','%'.$value.'%');
                                                        }
                                                    }
                                        })
                ->orderBy('created_at','desc')
                ->paginate(20);

       return response()->json(['status' => "900",'message' => "Records found",
                                          'data' => $data ]);


        
      } catch(\Exception $e){
            Log::error($e->getMessage());
            return response()->json(['status' => "907",'message' => $e->getMessage()]);
       }

      



    }
}
