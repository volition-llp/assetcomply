<?php

namespace App\Http\Controllers; 

use Illuminate\Http\Request;

use App\Userdetails;
use Mail;
use App\User;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\VerifyUser;
use App\Mail\ForgetPasswordMail;
use App\Mail\VerifyMail;
use Carbon\Carbon;
use App\ResetPasswordLog;
use App\SysConfig;
use Webpatser\Uuid\Uuid;
use App\QRBarCodeFields;
use Laravel\Passport\TokenRepository;
use Laravel\Passport\RefreshTokenRepository;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

	public function registerUser(Request $request)
	{
        $rules = [
                    'firstname' =>'required',
                    'lastname' =>'required',                    
                    'mobile' =>'required|digits:10|integer|min:0', #validating for negative value
                    'email' =>'required|email',
                    'company' =>'required',
                    'username' => 'required|unique:userdetails,username|unique:users,email',
                    'no_of_assets' => 'required|numeric|integer|min:0',#validating for negative value
                    'no_of_subusers' => 'required|numeric|integer|min:0',#validating for negative value
                    'expire_in_days' => 'required|numeric|integer|min:0'#validating for negative value

                  ];
         
        $response = array('response' => '', 'success'=>false);
        $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
               
                return response()->json(['status' => "901",'message' => $validator->messages()]);

            }else{

                $user_email_checking = Userdetails::where('email',$request->input('email'))->first();
                $user_mobile_checking = Userdetails::where('mobile',$request->input('mobile'))->first();
                $user_company_checking = Userdetails::where('company',strtolower($request->input('company')))->first();

                $msg = array();

                if(!empty($user_email_checking) || !empty($user_mobile_checking) || !empty($user_company_checking) ){

                     if(!empty($user_email_checking)){

                         $msg[] = "Email will be unique";

                     }

                      if(!empty($user_mobile_checking)){

                        $msg[] = "Mobile number will be unique";

                     }
                      if(!empty($user_company_checking)){

                        $msg[] = "Company name will be unique";

                     }

                    return response()->json(['status' => "901",'message' => $msg]);

                 }

                   else{

                      // Saving customer data to db
                      try{
                        $this->addCustomerDataToDB($request); 

                        $email=$request->input('email');
                        Log::info("Email send to this ".$email);
                        Log::info("Registration Successfull");
                       return response()->json(['status' => "900",'message' => "Registration Successfull & Pending for Email Verification"]);
                      
                     }catch (\Exception $e) {
                       Log::critical('Error in Customer adding'. $e->getMessage());
                       return response()->json(['status' => "902", 'message'=>"Exception Occured"]);
                     }
}    

            }

	}

// Adding customer data to DB
  public static function addCustomerDataToDB($request)
  { 
                    $mode = "admin";  
                    $redirect = "home";
                    $random = Str::random(10);
                    $pass=rand(1,999).$random.rand(1,999);
                    $password=str_shuffle($pass);

                    $start_date = Carbon::now();
                    $expire_in_date = $start_date->addDays($request->input('expire_in_days'));

                   // Customer Information
                     $userdetails = new Userdetails();
                     $userdetails->username = $request->input('username');
                     $userdetails->firstname=$request->input('firstname');
                     $userdetails->lastname=$request->input('lastname');
                     $userdetails->email=$request->input('email');
                     $userdetails->mobile=$request->input('mobile');
                     $userdetails->company=strtolower($request->input('company'));
                     $userdetails->payment_status=1;
                     $userdetails->source=$redirect;
                     $userdetails->no_of_assets=$request->input('no_of_assets');
                     $userdetails->no_of_subusers=$request->input('no_of_subusers');
                     $userdetails->expire_in_date= $expire_in_date;
                     $userdetails->save();

                     // Customer Login details
                     $credentials=new User();
                     $credentials->name=$request->input('firstname').' '.$request->input('lastname');
                     $credentials->email=$request->input('username');
                     $credentials->account_status=1;
                     $credentials->first_time_login_status=1;
                     $credentials->password=Hash::make($password);
                     $credentials->save();

                    // Customer company's system configuration
                     $sys_config_data = new SysConfig();
                     $sys_config_data->uuid = Uuid::generate()->string;
                     $sys_config_data->company_id = strtolower($request->input('company'));
                     $sys_config_data->time_zone = 'UTC';
                     $sys_config_data->asset_scan_output = 'qrcode';
                     $sys_config_data->height_barqrcode_output = "65";
                     $sys_config_data->width_barqrcode_output = "1.2";
                     $sys_config_data->qr_width = '100';
                     $sys_config_data->title_position_in_scan_output = 'Down';
                     $sys_config_data->barcode_color = '#212121';
                     $sys_config_data->qrcode_color = '#212121';
                     $sys_config_data->save();
                    
                     $insert_qr_fields = new QRBarCodeFields();
                     $insert_qr_fields->fields = ["asset_id",
                                                  "asset_name",
                                                  "category",
                                                  "sub_category",
                                                  "vendor_id",
                                                  "brand"];
                     $insert_qr_fields->company_id = strtolower($request->input('company'));
                     $insert_qr_fields->added_by = $request->input('username');
                     $insert_qr_fields->save();
                     
                    
                    // $token = sha1(time());
                    $token = "sd";

                    // $verifyUser = VerifyUser::create([
                    //   'username' => $request->input('username'),
                    //   'token' => $token
                    // ]);


                    // $name=$request->input('firstname').' '.$request->input('lastname');
                    $email=$request->input('email');

                     \Mail::to($email)->send(new VerifyMail($request->input('username'),$token,$password));
  

  }

  public function verifyUser($token)
  {
    $verifyUser = VerifyUser::where('token', $token)->first();
    $verified_check = User::where('email', $verifyUser['username'])->first();
      if(isset($verifyUser)){

             if(!$verified_check['email_verify_status']) {

                $email = $verifyUser['username'];
                $user_email_status = User::where('email', $email)->first();
                $user_email_status->email_verify_status = 1;
                $user_email_status->email_verified_at = Carbon::now();
                $user_email_status->save();

                return response()->json(['status' => "900",'message' => "Email Verified"]);
              } else {
                return response()->json(['status' => "900",'message' => "Email is already verified"]);
              }
        } else {
               return response()->json(['status' => "905",'message' => "Your email can't be identified'"]);
        }
      // return redirect('/login')->with('status', $status);
  }


	/*login*/

  /*user login is modified beasuse of expire date middleware*/
	public function userlogin(Request $request)
	{


    try {


    $username=$request->username;
    $password=$request->password;
    $company=$request->company;


     $validator = Validator::make($request->all(), [
                    'username' =>'required',
                    'password' =>'required',
                    'company' => 'required'                    
                 
      ]);

            
        if ($validator->fails()) {
    
                  return response()->json(['status' => "901",'message' => $validator->messages()]);
            
          }else{

                $login_check = User::where('email', $username)
                                      ->where('account_status',1)
                                      ->first();

                if(!empty($login_check))
                {
                      if(Hash::check($password,$login_check->password))
                      {
                                $check_cmpny = Userdetails::where('company', strtolower($company))
                                                            ->where('payment_status',1)
                                                            ->first();
                                
                                if(!empty($check_cmpny)){

                                     $token = $login_check->createToken('Auth Token')->accessToken;
                                     

                                     Log::info("Logged In ". $username);

                                     return response()->json(['status' => "900",'message' => "login successfull",
                                                              'token' => "Bearer ".$token,
                                                              'username'=>$check_cmpny->username,
                                                              'ftymlogin' => $login_check->first_time_login_status,
                                                              // if 1 then its is first time and for 0 its not first time
                                                              'timezone'=>config('app.timezone'),
                                                              'company'=>strtolower($company)]);
                                }else{

                                     return response()->json(['status' => "906",'message' => "Company name is not valid"]); 
                                }
                                 

                        }
                        else
                        {
                            return response()->json(['status' => "906",'message' => "Wrong username or password"]); 
                        }

                      
                  }else{

                       return response()->json(['status' => "906",'message' => "Account is suspended. Contact Support"]); 
                  }
                }





      
    } catch (Exception $e) {

      return response()->json(['status' => "502",'message' => "Internal server error"]);
      
    }



	}


  public function listOfRequestedCustomer()
  {
                try{

                    $customer_data =  \DB::table('userdetails')
                                        ->join('users','userdetails.username', '=', 'users.email')
                                        ->select('userdetails.*','users.email_verify_status','users.mobile_verify_status','users.account_status')
                                        ->orderBy('userdetails.created_at','desc')
                                        ->paginate(10);

                    return response()->json(['status' => "900",'data' => $customer_data]);

                }catch(\Exception $e){

                  return response()->json(['status' => "902",'message' => $e->getMessage()]);
                }   

  }

  public function singleCustomerInfo(Request $request)
  {
        $customer_username = $request->input('customer_username');
        $customer_company = strtolower($request->input('customer_company'));
        $usertype = $request->input('usertype');


        $validator = Validator::make($request->all(), [
                    'customer_username' =>'required|exists:userdetails,username',
                    'customer_company' =>'required|exists:userdetails,company'
                    ]);

                try{

                  if($usertype !== null){

                    $customer_data =  \DB::table('sub_users')
                                                  ->where('company',$customer_company)
                                                  ->where('username',$customer_username)
                                                  // ->where('user_type',$usertype)
                                                  ->join('locations', 'sub_users.location', '=', 'locations.loc_id')
                                                  ->join('users', 'sub_users.username', '=', 'users.email')
                                                  ->select(
                                                    'sub_users.*',
                                                    'locations.country',
                                                    'locations.state',
                                                    'locations.city',
                                                    'locations.location_name',
                                                    'users.account_status'
                                                  )
                                                  ->first();
                                                  
                  }else{

                    $customer_data =  Userdetails::where('company',$customer_company)
                                                ->where('username',$customer_username)
                                                ->join('users', 'userdetails.username', '=', 'users.email')
                                                ->select(
                                                  'userdetails.*',
                                                  'users.account_status'
                                                )
                                                ->first();
                  }
                   

                    return response()->json(['status' => "900",'data' => $customer_data]);

                }catch(\Exception $e){
                  Log::critical('Customer Profile'. $e->getMessage());
                  return response()->json(['status' => "902",'message' => "Internal Server Error"]);
                } 


  }

  public function updateCustomerAccountStatus(Request $request)
  {
     $account_status = $request->input('account_status');
     $username = $request->input('username');
     $company = strtolower($request->input('company'));

         try{

            $customer_account_status_update = Userdetails::where('company', $company)
                                                          ->where('username', $username)
                                                          ->first();

               if(!empty($customer_account_status_update)){

                     $customer_accs_update = User::where('email', $username)
                                                    ->update(['account_status'=>$account_status]);

                     return response()->json(['status' => "900",'message' => "Account status is updated", 'data'=>$customer_account_status_update]);
               }

         }catch (\Exception $e) {

           return response()->json(['status' => "902",'message' => $e->getMessage()]);
         }

  }

  public function updateCustomerFTimeLoginStatus(Request $request)
  {
  
     $username = $request->input('emp_id');
     $company = strtolower($request->input('company_id'));

         try{

            $customer_account_status_update = Userdetails::where('company', $company)
                                                          ->where('username', $username)
                                                          ->first();

               if(!empty($customer_account_status_update)){

                     $customer_accs_update = User::where('email', $username)
                                                    ->update(['first_time_login_status'=>0]);

                     return response()->json(['status' => "900",'message' => "Account status is updated"]);
               }

         }catch (\Exception $e) {

           return response()->json(['status' => "902",'message' => $e->getMessage()]);
         }

  }


    public function resetCustomerPassword(Request $request)
    {
      $old_password = $request->input('old_password');
      $new_password =$request->input('new_password');
      $reconf_password =$request->input('reconf_password');

      $username =$request->input('username');
      $company_id =$request->input('company_id');
      
      $validator = Validator::make($request->all(), [
                            'old_password' =>'required',
                            'new_password' =>'required',
                            'reconf_password' => 'required|same:new_password',
                            'username' => 'required|exists:userdetails,username',
                            'company_id' => 'required|exists:userdetails,company'                    
                        
                     ]);


                  if ($validator->fails()) {

                        return response()->json(['status' => "902",'message' => $validator->messages()]);

                  }else{
                    
                        $user_old_pass_check = User::where('email', $username)
                                                      ->first();
                        $old_password_db = $user_old_pass_check->password;

                        if(Hash::check($old_password,$old_password_db))
                        {
                           $user_password_update = User::where('email', $username)
                                                        ->update(['password'=>Hash::make($new_password)]);
                            // set log of reset password customer
                           $user_reset_pass_log = new ResetPasswordLog();
                           $user_reset_pass_log->username = $username;
                           $user_reset_pass_log->company = $company_id;
                           $user_reset_pass_log->save();

                           return response()->json(['status' => "900",
                                                    'message' => "New Password is updated"]);

                        }else{

                            return response()->json(['status' => "901",'message' => "Not matching with your Old Password"]);
                          }
                  }
      
      
      
    }

    public function updateCustomerProfileInfo(Request $request)
    {
          $username = $request->input('username');
          $company = strtolower($request->input('company'));
          $firstname = $request->input('firstname');
          $lastname = $request->input('lastname');
          $email = $request->input('email');
          $mobile = $request->input('mobile');
                        
              $validator = Validator::make($request->all(), [
                                        'username' => 'required|exists:userdetails,username',
                                        'company' => 'required|exists:userdetails,company',
                                        'mobile' => 'integer'                    
                                    
                                  ]);

         try{

            $customer_account_status_update = Userdetails::where('company', $company)
                                                          ->where('username', $username)
                                                          ->first();

               if(!empty($customer_account_status_update)){

                     $customer_accs_update = Userdetails::where('username', $username)
                                                         ->where('company', $company)
                                                         ->update([
                                                           'firstname'=>$firstname,
                                                           'lastname'=>$lastname,
                                                           'email'=>$email,
                                                           'mobile'=>$mobile
                                                           
                                                           ]);

                     return response()->json(['status' => "900",'message' => "Account is updated"]);
               }

         }catch (\Exception $e) {

           Log::critical('Customer Info Update: '.$e->getMessage() );
           return response()->json(['status' => "902",'message' => 'Internal Server Error']);
         }

    }

    public function forgetPasswordRecoveryCustomer(Request $request)
    {
      try{
        
        $username =$request->input('username');
        $company_id =$request->input('company_id');
      
        $validator = Validator::make($request->all(), [
                              'username' => 'required|exists:userdetails,username',
                              'company_id' => 'required|exists:userdetails,company' 
                      ]);

              if ($validator->fails()) {

                        return response()->json(['status' => "902",'message' => $validator->messages()]);

                  }
               $userdetails = DB::table('userdetails')->where('username',$username)->first();
              
                  
        $frgt_url = "http://localhost:4200/password/recovery"; 

        $hash2 = Hash::make($company_id.'-'.$username);
        
        $user_reset_pass_log = new ResetPasswordLog();
        $user_reset_pass_log->username = $username;
        $user_reset_pass_log->company = $company_id;
        $user_reset_pass_log->status = 0;
        $user_reset_pass_log->save();
                  
        $fpass_id = $user_reset_pass_log->id;

        Mail::to($userdetails->email)->send(new ForgetPasswordMail($frgt_url.'/'.$fpass_id.'/'.$username.'/'.$hash2));
      
        return response()->json(['status' => "900",'message' => "Password reset link sent to your registered email"]);
      
      }catch (\Exception $e) {

        Log::critical('Customer forget password: '.$e->getMessage() );
        return response()->json(['status' => "902",'message' => 'Internal Server Error']);
      }

    }

    public function forgetPasswordCheckingCustomer(Request $request)
    {
      try{
        
        $id =$request->input('id');
        $username =$request->input('username');
        $company_id =$request->input('company_id_hash');
        $password =$request->input('password');
      
        $validator = Validator::make($request->all(), [
                              'username' => 'required|exists:userdetails,username',
                              'id' => 'required|exists:reset_password_log,id',
                              'password' => 'required'
                      ]);

              if ($validator->fails()) {

                        return response()->json(['status' => "902",'message' => $validator->messages()]);

                  }

        $subuser_reset_log = DB::table('reset_password_log')->where('id',$id)->first();

         if($subuser_reset_log->status) {
             return response()->json(['status' => "901",'message' => "Reset Link is Invalid"]);
         }else{

             $created_timestamp = Carbon::parse($subuser_reset_log->created_at);
             $now_timestamp = Carbon::now();
               //checking 30mins validity of Reset Link
                if($now_timestamp->diffInMinutes($created_timestamp) > '30'){
                    
                  $reset_status_update = DB::table('reset_password_log')
                                              ->where('id',$id)
                                              ->update(['status'=>1]);
                    return response()->json(['status' => $now_timestamp->diffInMinutes($created_timestamp),'message' => "Reset Link is Expired"]);
                }else{


                             $reset_status_update = DB::table('reset_password_log')
                                                        ->where('id',$id)
                                                        ->update(['status'=>1]);
                             $password_update = DB::table('users')
                                                        ->where('email',$username)
                                                        ->update(['password'=> Hash::make($password)]);
                            
                            return response()->json(['status' => '900','message' => "Password has been reset"]);
                   
                }

         }



        return response()->json(['status' => "900",'message' => "Password reset link sent to your registered email"]);
      
      }catch (\Exception $e) {

        Log::critical('Customer forget password: '.$e->getMessage() );
        return response()->json(['status' => "902",'message' => 'Internal Server Error']);
      }

    }


    public function forgetPasswordRecoverySubCustomer(Request $request)
    {
      try{
        
        $username =$request->input('username');
        $company_id =$request->input('company_id');
      
        $validator = Validator::make($request->all(), [
                              'username' => 'required|exists:sub_users,username',
                              'company_id' => 'required|exists:userdetails,company' 
                      ]);

              if ($validator->fails()) {

                        return response()->json(['status' => "902",'message' => $validator->messages()]);

                  }
        $userdetails = DB::table('sub_users')->where('username',$username)->first();

                  
        $frgt_url = "http://localhost:4200/password/recovery"; 

        $hash2 = Hash::make($company_id.'-'.$username);
        
        $user_reset_pass_log = new ResetPasswordLog();
        $user_reset_pass_log->username = $username;
        $user_reset_pass_log->company = $company_id;
        $user_reset_pass_log->status = 0;
        $user_reset_pass_log->save();
                  
        $fpass_id = $user_reset_pass_log->id;

        Mail::to($userdetails->email)->send(new ForgetPasswordMail($frgt_url.'/'.$fpass_id.'/'.$username.'/'.$hash2));
      
        return response()->json(['status' => "900",'message' => "Password reset link sent to your registered email"]);
      
      }catch (\Exception $e) {

        Log::critical('Customer forget password: '.$e->getMessage() );
        return response()->json(['status' => "902",'message' => 'Internal Server Error']);
      }

    }
    public function forgetPasswordCheckingSubCustomer(Request $request)
    {
      try{
        
        $id =$request->input('id');
        $username =$request->input('username');
        $company_id =$request->input('company_id_hash');
        $password =$request->input('password');
      
        $validator = Validator::make($request->all(), [
                              'username' => 'required|exists:sub_users,username',
                              'id' => 'required|exists:reset_password_log,id',
                              'password' => 'required'
                      ]);

              if ($validator->fails()) {

                        return response()->json(['status' => "902",'message' => $validator->messages()]);

                  }

        $subuser_reset_log = DB::table('reset_password_log')->where('id',$id)->first();

         if($subuser_reset_log->status) {
             return response()->json(['status' => "901",'message' => "Reset Link is Invalid"]);
         }else{

             $created_timestamp = Carbon::parse($subuser_reset_log->created_at);
             $now_timestamp = Carbon::now();
               //checking 30mins validity of Reset Link
                if($now_timestamp->diffInMinutes($created_timestamp) > '30'){
                    
                  $reset_status_update = DB::table('reset_password_log')
                                              ->where('id',$id)
                                              ->update(['status'=>1]);
                    return response()->json(['status' => $now_timestamp->diffInMinutes($created_timestamp),'message' => "Reset Link is Expired"]);
                }else{


                             $reset_status_update = DB::table('reset_password_log')
                                                        ->where('id',$id)
                                                        ->update(['status'=>1]);
                             $password_update = DB::table('users')
                                                        ->where('email',$username)
                                                        ->update(['password'=> Hash::make($password)]);
                            
                            return response()->json(['status' => '900','message' => "Password has been reset"]);
                   
                }

         }



        return response()->json(['status' => "900",'message' => "Password reset link sent to your registered email"]);
      
      }catch (\Exception $e) {

        Log::critical('Customer forget password: '.$e->getMessage() );
        return response()->json(['status' => "902",'message' => 'Internal Server Error']);
      }

    }
    
    public function MasterUserlogout(Request $request)
      {
         $request->user()->token()->revoke();
        
        // $tokenRepository = app(TokenRepository::class);
        // $refreshTokenRepository = app(RefreshTokenRepository::class);

     
        // $tokenRepository->revokeAccessToken($tokenId);

        // $refreshTokenRepository->revokeRefreshTokensByAccessTokenId($tokenId);

        return response()->json(['status' => "900",'message' => "Logged Out"]);
      }












}
