<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendor;
use Validator;
use Illuminate\Validation\Rule;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;

class VendorController extends Controller
{
    public function addVendorDetails(Request $request)
    { 
        $added_by=$request->input('username');
        $company_id=$request->input('company_id');
        $vendor_name=$request->input('vendor_name');
        $address=$request->input('address');
        $primary_person=$request->input('primary_person');
        $primary_number=$request->input('primary_number');
        $email=$request->input('email');
        $state=$request->input('state');
        $country=$request->input('country');
        $city=$request->input('city');
        $postal_code=$request->input('postal_code');
        $pan=$request->input('pan');
        $gst_number=$request->input('gst_number');


		 $validator = Validator::make($request->all(), [
            		    'username' =>'required', //|exists:userdetails,username
                        'company_id' =>'required|exists:userdetails,company',
                        'vendor_name' => 'required|unique:vendors,vendor_name',  
                        'address' =>'',
                        'primary_person' =>'',
                        'primary_number' => 'nullable|digits:10',  
                        'email' =>'nullable|email',
                        'state' =>'',
                        'city' => '',  
                        'postal_code' =>'nullable|digits:6', 
                        'pan' =>'nullable|max:10|min:10',
                        'gst_number' => 'nullable|max:15|min:15',                    
                 
			]);

            
        if ($validator->fails()) {
    
    	          	return response()->json(['status' => "901",'message' => $validator->messages()]);
          	
       		}else{

                try{

                    $vendor_data = new Vendor();
                    $vendor_data->vendor_name = $vendor_name;
                    $vendor_data->address = $address;
                    $vendor_data->primary_person = $primary_person;
                    $vendor_data->primary_number = $primary_number;
                    $vendor_data->email = $email;
                    $vendor_data->state = $state;
                    $vendor_data->city = $city;
                    $vendor_data->country = $country;
                    $vendor_data->postal_code = $postal_code;
                    $vendor_data->pan = $pan;
                    $vendor_data->gst_number = $gst_number;
                    $vendor_data->company_id = $company_id;
                    $vendor_data->added_by = $added_by;
                    $vendor_data->save();

                    return response()->json(['status' => "900",'message' => "Vendor saved"]);


                }catch(\Exception $e){
                    Log::error("Vendor Module Exception",$e->getMessage());
                  return response()->json(['status' => "902",'message' => $e->getMessage()]);
                }   

            }


    }

    public function listOfVendors(Request $request)
    { 
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|exists:userdetails,company',
            'username' => 'required'                
        ]);

        $search_filters =  $request->input('search_filters');
        $company_id =$request->input('company_id');
        $username =$request->input('username');

        if($validator->fails()) {
			
            $message=$validator->errors();
            // $validator->messages();
            return response()->json(['status'=>"901",'message' => $message]);
        }

        try{
             $vendor_data = Vendor::where('company_id',$company_id)
                                            ->whereNested(function($query) use ($search_filters) {
                                                    foreach ($search_filters as $key => $value)
                                                        {
                                                            if($value != ''){
                                                                $query->where($key,'LIKE','%'.$value.'%');
                                                            }
                                                        }
                                            }, 'and');

                $vendor_data = $vendor_data->orderBy('created_at','desc')
                                            ->where('added_by',$username)
                                            ->paginate(10);

                 return response()->json(['status' => "900",'message'=> "Success",'data'=>$vendor_data]);

        }catch (\Exception $e) {
            Log::error($e->getMessage());
            return response()->json(['status' => "902",
                    'message' => "Exception while saving Location"],422);
        }

    }

    public function listOfVendorsWithoutPagination($company_id)
    { 
        $vendor_data = Vendor::where('company_id',$company_id)
                                ->get();

        return response()->json(['status' => "900",'message'=> "Success",'data'=>$vendor_data]);

    }

    public function infoOfSingleVendor($company_id,$id)
    {
        $vendor_data = Vendor::where('company_id',$company_id)
                                ->where('id',$id)
                                ->first();

        return response()->json(['status' => "900",'message'=> "Success",'data'=>$vendor_data]);
    }

    public function updateVendorInfo(Request $request)
    {
        $vendor_name=$request->input('vendor_name');
        $address=$request->input('address');
        $primary_person=$request->input('primary_person');
        $primary_number=$request->input('primary_number');
        $email=$request->input('email');
        $state=$request->input('state');
        $city=$request->input('city');
        $country=$request->input('country');
        $postal_code=$request->input('postal_code');
        $pan=$request->input('pan');
        $gst_number=$request->input('gst_number');

        $id=$request->input('id');


		 $validator = Validator::make($request->all(), [
                        'vendor_name' => 'required|unique:vendors,vendor_name,'.$request->id,
                        'address' =>'',
                        'primary_person' =>'',
                        'primary_number' => 'nullable|digits:10',  
                        'email' =>'nullable|email',
                        'state' =>'',
                        'city' => '',  
                        'postal_code' =>'nullable|digits:6', 
                        'pan' =>'nullable|max:10|min:10',
                        'gst_number' => 'nullable|max:15|min:15',
                        'id' => 'required'
                 
			]);

            
        if ($validator->fails()) {
    
    	          	return response()->json(['status' => "901",'message' => $validator->messages()]);
          	
       		}else{

                try{

                    $vendor_data = Vendor::find($id);
                    $vendor_data->vendor_name = $vendor_name;
                    $vendor_data->address = $address;
                    $vendor_data->primary_person = $primary_person;
                    $vendor_data->primary_number = $primary_number;
                    $vendor_data->email = $email;
                    $vendor_data->state = $state;
                    $vendor_data->city = $city;
                    $vendor_data->country = $country;
                    $vendor_data->postal_code = $postal_code;
                    $vendor_data->pan = $pan;
                    $vendor_data->gst_number = $gst_number;
                    $vendor_data->save();

                    return response()->json(['status' => "900",'message' => "Vendor updated"]);

                }catch(\Exception $e){
                    Log::error("Vendor Module Exception",$e->getMessage());
                  return response()->json(['status' => "902",'message' => $e->getMessage()]);
                }

            }

    }


    public function vendorDelete(Request $request)
    {
        $id=$request->input('id');
        $company_id =$request->input('company_id');
        $vendor_name=$request->input('vendor_name');

        $validator = Validator::make($request->all(), [
                                'vendor_name' => 'required|exists:vendors,vendor_name',
                                'company_id' =>'required|exists:userdetails,company',
                                'id' => 'required'
                        
                        ]);


                    if ($validator->fails()) {

                            return response()->json(['status' => "901",'message' => $validator->messages()]);
                    
                    }else{

                        try{
                            
                            $vendor_dependency_checking = \DB::table('asset_info')
                                                            ->where('vendor_id',$id)
                                                            ->where('company',$company_id)
                                                            ->count();
                            if($vendor_dependency_checking>0){
                                return response()->json(['status' => "901",'message' => "Vendor already in used. First delete those data"]);
                       
                            }                                
                            $vendor_data = Vendor::find($id);
                            $vendor_data->delete();



                            return response()->json(['status' => "900",'message' => "Vendor deleted"]);
                        }catch (\Exception $e) {
                            Log::error("Vendor Module Exception".$e->getMessage());
                            return response()->json(['status' => "902",'message' => "Internal Server Error"]);
                        }
                    }

    }
}
