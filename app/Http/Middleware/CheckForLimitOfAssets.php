<?php

namespace App\Http\Middleware;

use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use App\AssetInfo;
use App\Userdetails;
use Closure;    

class CheckForLimitOfAssets
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        // $company_id = app('request')->header('company');
        $company_id = $request->input('company');

        $count_asset = AssetInfo::where('company', $company_id)->count();

        $asset_limitation = Userdetails::select('no_of_assets')->where('company', $company_id)->first();

        if($count_asset<=$asset_limitation['no_of_assets']){
            return $next($request);
        }else{

            return response()->json(['status' => "901",'message' => "Asset Limit Exceed. Upgrade Your Account."]);
        }

    }
}
