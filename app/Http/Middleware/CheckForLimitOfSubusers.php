<?php

namespace App\Http\Middleware;

use Closure;
use App\SubUser;
use Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler; 
use Illuminate\Support\Facades\Log;
use App\Userdetails;

class CheckForLimitOfSubusers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $company_id = app('request')->header('company');
        $company_id = $request->input('company');

        $count_subuser = SubUser::where('company', $company_id)->count();

        $subuser_limitation = Userdetails::select('no_of_subusers')->where('company', $company_id)->first();

        if($count_subuser <= $subuser_limitation['no_of_subusers']){
            return $next($request);
        }else{

            return response()->json(['status' => "901",'message' => "Subuser Limit Exceed. Upgrade Your Account."]);
        }
    }
}
