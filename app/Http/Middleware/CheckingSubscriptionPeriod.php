<?php

namespace App\Http\Middleware;

use Closure;
use App\Userdetails;

class CheckingSubscriptionPeriod
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $username = $request->input('username');
        $company = $request->input('company');


        $check_user_subscription = Userdetails::where('company', strtolower($company))
                                    ->where('username',$username)
                                    ->first();

         if(!empty($check_user_subscription) && $check_user_subscription['payment_status']==1){
            return $next($request);
         }else{
            return response()->json(['status'=>"908",'message' =>"Your Subscription has end. Please contact with Customer Care"]);
         }
        
    }
}
