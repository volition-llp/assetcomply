<?php

namespace App\Http\Middleware;

use Closure;

class CompanyAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(1){
            return response()->json(['status'=>"900",'data' => $request->input('company_id')]);
        }else{
            return $next($request);
        }
           
    }
} 
