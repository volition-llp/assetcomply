<?php

namespace App\Http\Middleware;

use Closure;
use App\Userdetails;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
class Expirydatecheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        try {

           
                 $validator = Validator::make($request->all(), [
                                   
                                'company_id' => 'required|exists:userdetails,company'                                                
                                ]);

            
                if ($validator->fails()) {
            
                          return response()->json(['status' => "901",'message' => $validator->messages()]);
                    
                  }
                  else{



                $company = $request->company_id;

                $userdetails=Userdetails::where('company',$company)->first();

                 $start = Carbon::parse($userdetails->expire_in_date);

                $end = Carbon::now();

                if ($start >= $end) {
                    
                    return $next($request);
                }

                else{

                     return response()->json(['status' => "901",'message' => "Your account expired."]);
                }
          }


           
        } catch (Exception $e) {

            return response()->json(['status' => "502",'message' => "Internal server error"]);
            
        }






        
    }



}
