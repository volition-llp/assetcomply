<?php

namespace App\Http\Middleware;

use Closure;
use App\SysConfig;


class TimeZoneControl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $company=$request->input('company_id');
        $timezone = SysConfig::where('company_id',strtolower($company))
                                ->first()
                                ->time_zone;
                                
        \Config::set('app.timezone', $timezone);

        date_default_timezone_set($timezone);

        return $next($request);
    }
}
