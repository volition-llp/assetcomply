<?php

namespace App\Imports;

use App\TempAssetInfo;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\RemembersRowNumber;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Categories;
use App\Locations;
use App\AssetInfo;
use Webpatser\Uuid\Uuid;
use App\Vendor;

class AssetBulkImport implements ToModel, WithHeadingRow, ToCollection
{
     use Importable;
     use RemembersRowNumber;
     protected $added_by,$company_id;

     private $errors = array();
     private $errors_db =array();
     public function __construct($added_by,$company_id)
    {
        $this->added_by =  $added_by;
        $this->company_id = $company_id;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        //
    }
 
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $currentRowNumber = $this->getRowNumber();

        $category_id_data = Categories::where("name",$row['category'])->first();
        $subcategory_id = Categories::where("name",$row['sub_category'])->first();

        $check_categry = Categories::where('category_id', $subcategory_id->category_id)
                                        ->where('parent_id', $category_id_data->category_id)
                                        ->count();

        $check_asset_id = AssetInfo::where('company', $this->company_id)
                                    ->where('asset_id', $row['asset_id'])
                                    ->count();
        $check_vendor_id = Vendor::where('company_id',$this->company_id)
                                  ->where('vendor_name', $row['vendor_id'])
                                  ->count();
        
        $check_location_id = Locations::where('company_id',$this->company_id)
                                  ->where('loc_id', $row['location'])
                                  ->count();
        
        

        $this->errors_db = array();
        $this->errors = array();

        if($check_categry == 0 || $check_asset_id == 1 || $check_vendor_id == 0 || $check_location_id == 0){ 
            
            if($check_categry == 0){
                $this->errors[] = array('asset_id'=>$row['asset_id'],'error'=>"categoryid");
                $this->errors_db[] = array('error'=>"categoryid");
            }
            if($check_asset_id == 1){
                $this->errors[] = array('asset_id'=>$row['asset_id'],'error'=>"assetid");
                $this->errors_db[] = array('error'=>"assetid");
            }
            if($check_vendor_id == 0){
            $this->errors[] = array('asset_id'=>$row['asset_id'],'error'=>"vendorid");
            $this->errors_db[] = array('error'=>"vendorid");
            }
            if($check_location_id == 0){
                $this->errors[] = array('asset_id'=>$row['asset_id'],'error'=>"locationid");
                $this->errors_db[] = array('error'=>"locationid");
            }
              
            
            return new TempAssetInfo([
                'uuid'    => Uuid::generate()->string,
                'sub_category'  => $subcategory_id->category_id,
                'category' => $category_id_data->category_id,
                'company'    => $this->company_id,
                'asset_id'    => $row['asset_id'],
                'asset_type'    => $row['asset_type'],
                'asset_name'    => $row['asset_name'],
                'brand'    => $row['brand'],
                'serial_number'    => $row['serial_number'],
                'model'    => $row['model'],
                'description'    => $row['description'],
                'warranty_date'    => $row['warranty_date'],
                'condition'    => $row['condition'],
                'vendor_id'    => $row['vendor_id'],
                'po_number'    => $row['po_number'],
                'invoice_number'    => $row['invoice_number'],
                'invoice_date'    => $row['invoice_date'],
                'purchase_price'    => $row['purchase_price'],
                'purchase_date'    => $row['purchase_date'],
                'capitalization_price'    => $row['capitalization_price'],
                'end_of_life'    => $row['end_of_life'],
                'depreciation_rate'    => $row['depreciation_rate'],
                'depreciation_method'    => $row['depreciation_method'],
                'depreciation_for_the_year'    => $row['depreciation_for_the_year'],
                'total_depreciation'    => $row['total_depreciation'],
                'ownership'    => $row['ownership'],
                'capitalization_date'    => $row['capitalization_date'],
                'scrap_value'    => $row['scrap_value'],
                'department'    => $row['department'],
                'location'    => $row['location'],
                'unit'    => $row['unit'],
                'emp_id'    => $this->added_by,
                'date'    => $row['date'],
                'errors'  => $this->errors_db,
                'added_by'=> $this->added_by,
            ]);



        }else{

            
             return new AssetInfo([
                'sub_category'  => $subcategory_id->category_id,
                'category' => $category_id_data->category_id,
                'company'    => $this->company_id,
                'asset_id'    => $row['asset_id'],
                'asset_type'    => $row['asset_type'],
                'asset_name'    => $row['asset_name'],
                'brand'    => $row['brand'],
                'serial_number'    => $row['serial_number'],
                'model'    => $row['model'],
                'description'    => $row['description'],
                'warranty_date'    => $row['warranty_date'],
                'condition'    => $row['condition'],
                'vendor_id'    => $row['vendor_id'],
                'po_number'    => $row['po_number'],
                'invoice_number'    => $row['invoice_number'],
                'invoice_date'    => $row['invoice_date'],
                'purchase_price'    => $row['purchase_price'],
                'purchase_date'    => $row['purchase_date'],
                'capitalization_price'    => $row['capitalization_price'],
                'end_of_life'    => $row['end_of_life'],
                'depreciation_rate'    => $row['depreciation_rate'],
                'depreciation_method'    => $row['depreciation_method'],
                'depreciation_for_the_year'    => $row['depreciation_for_the_year'],
                'total_depreciation'    => $row['total_depreciation'],
                'ownership'    => $row['ownership'],
                'capitalization_date'    => $row['capitalization_date'],
                'scrap_value'    => $row['scrap_value'],
                'department'    => $row['department'],
                'location'    => $row['location'],
                'unit'    => $row['unit'],
                'status'    => 1,
                'emp_id'    => $this->added_by,
                'date'    => $row['date']
            ]);

        } 

        
    }

    public function geterrors()
    {   
        return $this->errors;
    }
}
