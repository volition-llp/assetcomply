<?php

namespace App\Imports;

use App\AssetInfo;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class AssetImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new AssetInfo([
               'asset_id'     => $row[0],
               'category'    => $row[1],
               'sub_category' => $row[2],
               'brand'     => $row[3],
               'model'    => $row[4],
               'manufacturer' => $row[5],
               'vendor_id' => $row[6],
        ]);
    }

    public function headingRow(): int
    {
        return 2;
    }
}
