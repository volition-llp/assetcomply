<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AssetBreakdownMail extends Mailable
{
    use Queueable, SerializesModels;
    public $name,$mail_type,$new_asset_maintenance_ticket,$problem_stat,$issued_by;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$mail_type,$new_asset_maintenance_ticket,$problem_stat,$issued_by)
    {
        $this->name = $name;
        $this->mail_type = $mail_type;
        $this->new_asset_maintenance_ticket = $new_asset_maintenance_ticket;
        $this->problem_stat = $problem_stat;
        $this->issued_by = $issued_by;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('assetBreakdownMail'); 
    }
}
