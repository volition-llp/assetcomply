<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AssetMovementNotification extends Mailable
{
    use Queueable, SerializesModels;
    public $asset_name,$asset_id,$category,$sub_category, $from_location_name,$to_location_name;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($asset_name,$asset_id,$category,$sub_category, $from_location_name,$to_location_name)
    {
        $this->asset_name = $asset_name;
        $this->asset_id = $asset_id;
        $this->category =$category;
        $this->sub_category = $sub_category;
        $this->from_location_name = $from_location_name;
        $this->to_location_name = $to_location_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('assetMovementHistory');
    }
}
