<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QRBarCodeFields extends Model
{
    protected $table= "qr_barcode_fields";

    protected $casts = [    
        'fields' => 'array',
    ];
}
