<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResetPasswordLog extends Model
{
    protected $table= "reset_password_log";
}
