<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupportTicket extends Model
{
    protected $table = 'support_ticket';

    protected $casts = [    
        'date' => 'date',
    ];
}
