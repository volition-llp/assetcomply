<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempAssetInfo extends Model
{
    protected $table='temp_asset_info';

    protected $fillable = [ 'uuid',
                            'sub_category',
                            'category',
                            'company',
                            'asset_id',
                            'asset_type',
                            'asset_name',
                            'brand',
                            'serial_number',
                            'model',
                            'description',
                            'warranty_date',
                            'condition',
                            'vendor_id',
                            'po_number',
                            'invoice_number',
                            'invoice_date',
                            'purchase_price',
                            'purchase_date',
                            'capitalization_price',
                            'end_of_life',
                            'depreciation_rate',
                            'depreciation_method',
                            'depreciation_for_the_year',
                            'total_depreciation',
                            'ownership',
                            'capitalization_date',
                            'scrap_value',
                            'department' ,
                            'location',
                            'units',
                            'emp_name',
                            'emp_id',
                            'date',
                            'errors',
                            'added_by'];
       protected $casts = [
                            'errors' => 'array',
                         ];
}
