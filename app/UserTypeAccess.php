<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTypeAccess extends Model
{
    protected $table= 'user_type_access';

    protected $casts = [    
        'view' => 'boolean',
        'add' => 'boolean',
        'edit' => 'boolean',
        'delete' => 'boolean',
        'export' => 'boolean',
    ];
}
