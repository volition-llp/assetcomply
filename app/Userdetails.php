<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Thomasjohnkane\Snooze\Traits\SnoozeNotifiable;

class Userdetails extends Model
{
    use Notifiable;
    // use SnoozeNotifiable;

    protected $table='userdetails'; 
    
}
