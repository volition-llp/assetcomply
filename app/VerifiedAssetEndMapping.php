<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifiedAssetEndMapping extends Model
{
    protected $table= "verified_asset_end_mapping";
}
