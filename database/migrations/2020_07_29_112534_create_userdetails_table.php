<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserdetailsTable extends Migration 
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userdetails', function (Blueprint $table) {
            $table->id();
            $table->string('username', 255);
            $table->string('firstname', 255);
            $table->string('lastname', 255);
            $table->string('email', 100);
            $table->string('mobile', 10);
            $table->string('subdomain',100)->nullable();
            $table->string('company', 100);
            $table->boolean('payment_status')->default(0);
            $table->string('source', 100);
            $table->string('no_of_assets',50)->nullable();
            $table->string('no_of_subusers',50)->nullable();
            $table->string('expire_in_date',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userdetails');
    }
}
