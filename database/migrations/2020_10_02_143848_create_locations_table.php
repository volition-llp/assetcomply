<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        Schema::create('locations', function (Blueprint $table) {
            $table->string('loc_id',150)->primary();
            $table->string('state', 200);
            $table->string('country',200);
            $table->string('city', 200);
            $table->string('location_name', 255);
            $table->string('company_id', 255);
            $table->string('added_by', 255);
            $table->boolean('delete_status', 255)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
