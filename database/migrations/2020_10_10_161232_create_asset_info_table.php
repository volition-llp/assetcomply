<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_info', function (Blueprint $table) {
            $table->string('asset_id',150);
            $table->string('asset_type',50)->nullable();
            $table->string('asset_name', 255)->nullable();
            $table->string('category', 100)->nullable();
            $table->string('sub_category', 255)->nullable();
            $table->string('brand', 100)->nullable();
            $table->string('serial_number', 190)->nullable();
            $table->string('model', 150)->nullable();
            $table->string('description',300)->nullable();
            $table->string('warranty_date',100)->nullable();
            $table->string('condition',100)->nullable();
            $table->string('vendor_id', 100)->nullable();
            $table->string('po_number', 100)->nullable();
            $table->string('invoice_number',100)->nullable();
            $table->string('invoice_date',100)->nullable();
            $table->string('purchase_price',100)->nullable();
            $table->string('purchase_date',100)->nullable();
            $table->string('capitalization_price',100)->nullable();
            $table->string('end_of_life',100)->nullable();
            $table->string('depreciation_rate',100)->nullable();
            
            $table->string('depreciation_method',100)->nullable();//Straight-Line Method (SLM), Written Down Value Method (WDV)
            $table->string('depreciation_for_the_year',100)->nullable();
            $table->string('total_depreciation',100)->nullable();
            $table->string('ownership',100)->nullable();//Owned,Asset Held on Lease,Asset Given on Lease

            $table->string('capitalization_date',100)->nullable();
            $table->string('scrap_value',100)->nullable();
            $table->string('department',100)->nullable();
            $table->string('location',100)->nullable();
            $table->string('unit',100)->nullable();
            $table->boolean('status')->nullable();
            $table->string('emp_id',100)->nullable();
            $table->string('date',100)->nullable();
            $table->string('company',100);
            $table->timestamps();

            $table->primary(['asset_id', 'company']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_info');
    }
}
