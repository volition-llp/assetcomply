<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupportTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_ticket', function (Blueprint $table) {
            $table->id();
            $table->string('issue_type', 155);
            $table->string('priority', 100);
            $table->string('issued_by', 150);
            $table->string('location', 100);
            $table->string('subject', 190);
            $table->string('msg', 255);
            $table->string('attachment', 255)->nullable();
            $table->string('date', 150);
            $table->string('time', 150);
            $table->string('status', 150);
            $table->string('company', 500);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_ticket');
    }
}
