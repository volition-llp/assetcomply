<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_menu', function (Blueprint $table) {
            $table->id();
            $table->string('menu_name', 155);
            $table->string('menu_slug', 100);
            $table->string('menu_url', 100);
            $table->string('parent_menu', 100);
            $table->string('activate_status', 1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_menu');
    }
}
