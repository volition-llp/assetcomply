<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->id();
            $table->string('vendor_name', 100);
            $table->string('address', 100)->nullable();
            $table->string('primary_person', 100)->nullable();
            $table->string('primary_number', 50)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('country', 100)->nullable();
            $table->string('state', 50)->nullable();
            $table->string('city', 50)->nullable();
            $table->string('postal_code', 50)->nullable();
            $table->string('pan', 50)->nullable();
            $table->string('gst_number', 50)->nullable();
            $table->string('company_id', 100);
            $table->string('added_by', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
