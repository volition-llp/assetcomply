<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetBreakDownTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_breakdown', function (Blueprint $table) {
            $table->id();
            $table->string('breakdown_dt',100);
            $table->string('asset_id', 200);
            $table->string('company_id', 100);
            $table->string('problem_stat', 200);
            $table->string('location_id', 100);
            $table->string('issued_by', 100);
            $table->string('issue_closed_by', 100)->nullable();
            $table->string('cost_for_breakdown', 100)->nullable();
            $table->string('img_before_fixed', 100)->nullable();
            $table->string('img_after_fixed', 100)->nullable();
            $table->boolean('issue_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_breakdown');
    }
}
