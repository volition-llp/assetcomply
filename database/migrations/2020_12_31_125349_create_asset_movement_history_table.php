<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetMovementHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_movement_history', function (Blueprint $table) {
            $table->id();
            $table->string('asset_id', 200);
            $table->string('company_id', 100);
            $table->string('from_location', 200);
            $table->string('to_location', 100);
            $table->string('remarks', 100);
            $table->string('date', 100);
            $table->boolean('email_notification');
            $table->string('movement_by', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_movement_history');
    }
}
