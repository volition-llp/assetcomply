<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQrBarcodeFieldsTable extends Migration 

{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qr_barcode_fields', function (Blueprint $table) {
            $table->id();
            $table->string('fields',100);
            $table->string('company_id',100);
            $table->string('added_by',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qr_barcode_fields');
    }
}
