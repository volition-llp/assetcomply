<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSysConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_config', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->string('company_id', 100);
            $table->string('time_zone', 100);
            $table->string('asset_scan_output', 100);
            $table->string('height_barqrcode_output', 50)->nullable();
            $table->string('width_barqrcode_output', 50)->nullable();
            $table->string('qr_width', 50)->nullable();
            $table->string('title_position_in_scan_output', 100);
            $table->string('barcode_color', 100)->nullable();
            $table->string('qrcode_color', 100)->nullable();
            
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_config');
    }
}
