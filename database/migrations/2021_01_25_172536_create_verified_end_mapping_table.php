<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVerifiedEndMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verified_end_mapping', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->string('dt',36);
            $table->string('location_id',100);
            $table->string('company_id',100);
            $table->boolean('verified_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verified_end_mapping');
    }
}
