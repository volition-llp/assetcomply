<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetDiscardedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_discarded', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->string('asset_id',100);
            $table->string('date',50);
            $table->string('company_id',100);
            $table->string('location_id',100);
            $table->string('discarded_by',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_discarded');
    }
}
