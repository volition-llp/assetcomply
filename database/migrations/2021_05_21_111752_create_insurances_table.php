<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurances', function (Blueprint $table) {
            $table->id();
            $table->string('insurance_id');
            $table->string('policy_no');
            $table->string('covered_by');
            $table->string('start_date');
            $table->string('end_date');
            $table->string('insured_value');
            $table->string('premium_amount');
            $table->string('remind_before_days');
            $table->string('frequency_of_reminder');
            $table->string('document_upload');
            $table->string('payment_remark');
            $table->string('record_added_by');
            $table->string('company');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurances');
    }
}
