<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColoumsToSysConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sys_config', function (Blueprint $table) {
            $table->string('currency', 100)->nullable();
            $table->string('company_logo', 100)->nullable();
            $table->string('country', 100)->nullable();
            $table->string('company_name', 100)->nullable();
            $table->string('accounting_year', 100)->nullable();
            $table->boolean('asset_id_type', )->default(0);
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sys_config', function (Blueprint $table) {
            //
        });
    }
}
