<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumsToVerifiedAssetEndMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('verified_asset_end_mapping', function (Blueprint $table) {
           
            $table->string('verification_name',100)->nullable()->after('uuid');
            $table->timestamp('proposed_start_date')->nullable()->after('dt');
            $table->timestamp('proposed_end_date')->nullable()->after('dt');
         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('verified_asset_end_mapping', function (Blueprint $table) {
            //
        });
    }
}
