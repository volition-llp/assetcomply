<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>{{$mail_type}} | assetcomply</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
    <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
        <tr>
            <td style="padding: 10px 0 30px 0;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
                    <tr>
                        <td align="center" bgcolor="#fff" style="padding: 0px 0 30px 0; color: #212121; font-size: 45px; font-weight: bold; font-family: 'Palanquin Dark', sans-serif;">
                            <h3> assetcomply </h3>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#ffffff" style="padding: 40px 30px 40px 70px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="color: #212121; font-family: 'Palanquin Dark', sans-serif; font-size: 20px;">
                                        Greetings,<strong> {{$name}}</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 20px 0 30px 0; color: #153643; font-family: nunito sans-serif; font-size: 16px; line-height: 20px;">
                                         Asset breakdown ticket mail for you. Ticket number <strong># {{$new_asset_maintenance_ticket}}</strong> & issued by <strong> {{$issued_by}} </strong>. 
                                         Problem with this asset:  <strong> {{$problem_stat}} </strong>
                                    </td>
                                </tr>
                                

                                <tr>
                                    <td>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td width="260" valign="top">
                                                    Regards,
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="260" valign="top">
                                                   <b>Team <a href="https://assetcomply.com/" target="_blank">assetcomply</a></b>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td bgcolor="#dd2c00" style="padding: 30px 30px 30px 30px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="color: #ffffff; font-family: nunito sans-serif; font-size: 14px;" width="75%">
                                        &reg; 2021, Asset Comply<br/>
                                    </td>
                                    <td align="right" width="25%">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td width="260" valign="top">
                                                  <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.fitness" target="_blank">
                                                  <img src="https://upload.wikimedia.org/wikipedia/commons/7/78/Google_Play_Store_badge_EN.svg" height="50" width="90" alt="assetcomply">
                                                  </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>