<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

<table>
 <th>Sub category</th>
 <th>Category</th>
 <th>Name</th>
 <th>Price</th>
 <th>Description</th>
 <th>Emp Id</th>
 <th>Action</th>

 <tbody>
 	@foreach($data as $d)
 	<tr>
 		<td>{{$d->sub_category}}</td>
 		<td>{{$d->category}}</td>
 		<td>{{$d->name}}</td>
 		<td>{{$d->price}}</td>
 		<td>{{$d->details}}</td>
 		<td>{{$d->emp_id}}</td>
 		<td>{{$d->wrong_status}}</td>
 	</tr>
 	@endforeach

 </tbody>
</table>

</body>
</html>