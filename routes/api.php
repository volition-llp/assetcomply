<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route; 

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
 
 
// activity
Route::group(['middleware' => ['cors']], function () {

Route::prefix('v1')->group(function(){


/*to check expire_in-date in userdetails table*/
/*Route::group(['middleware' => ['expirydatecheck']], function () {
*/
	// Client Login API
	Route::post('/user/login','UserController@userlogin')
		->middleware(['check_subscription_period_end','subscription_reminder']);



	// Sub Client Login API
	Route::post('/sub/users/login','SubUserController@subUserLogin');


	/*auth:api*/


// ,'timezone'
Route::group(['middleware' => ['auth:api']], function () {


		// User logout 
		Route::post('users/logout','UserController@MasterUserlogout');

		// dashboard statistic 
		Route::post('/stats/assets/count','StatisticsController@numbersOfAssets');
		Route::post('/stats/location/count','StatisticsController@numbersOfLocations');
		Route::post('/stats/subusers/count','StatisticsController@numbersOfSubusers');
		Route::post('/stats/per/category/price','StatisticsController@categoryWisePriceChart');
		Route::post('/stats/per/location/assets','StatisticsController@numbersOfAssetsLocationwise');
		Route::post('/stats/count/assets/maintenance','StatisticsController@countOfAssetMaintenance');
		// end dashboard statistic


		/*export user guide as excel*/
		Route::post('/user/guide/export','ReportsExportController@exportUserGuide');


		// System notifications
		Route::post('/check/assets/count','NotificationController@checkCountOfAssets');
		Route::post('/check/subusers/count','NotificationController@checkCountOfSubUsers');
		// End System Notifications

		//document id(asset id) setting 
		Route::post('/docummentid/save','DocumentIdController@saveDocumentIdPattern');
		//end document id(asset id) setting 

		// Asset Adding
		Route::group(['middleware' => ['assetlimit']], function () {

			Route::post('/asset/add','AssetController@addSingleAsset');

		});
		Route::post('/search/asset/','SearchController@searchAssetByAssetId');
		Route::post('/asset/id/list','SearchController@listAssetId');
		Route::post('/assets', 'AssetController@listOfAssetsByClauses');
		Route::post('/assets/like/list', 'AssetController@listOfAssetsByLikewise');
		Route::post('/assets/record', 'AssetController@getOneRecordOfAsset');
		Route::get('/assets/fields','AssetController@assetFieldList');
		Route::post('/asset/update','AssetController@editAsset');
		Route::post('/asset/delete','AssetController@deleteAsset');
		// Asset End



		// Bulk asset csv uploading action
		Route::post('/bulk/asset/upload','BulkAssetController@bulkAssetUpload');
		Route::post('/bulk/records','BulkAssetController@bulkAssetRecords');
		Route::post('/wrong/assets/list','BulkAssetController@getWrongEntryAssetInfo');
		// End Bulk asset csv uploading action


		// Asset Movement
		Route::post('/asset/move','AssetController@assetMovementInputs');
		Route::post('/asset/move/update','AssetController@assetMovementInputsUpdate');
		// Asset Movement History
		
		//  Asset Category
		Route::get('/category/list/sub','CategoriesController@listOfCategoriesWithSubcategory');
		Route::get('/category/list/parent','CategoriesController@listOfCategoriesWithParent');
		Route::post('/category/list/single/parent','CategoriesController@listOfCategoriesWithSingleParent');

		//End Asset Category End


		// application settings (Bar/QR code field set)
		Route::post('/asset/barqrcode/setting', 'ApplicationSettingsController@setFieldsForBarcodeQRcode');
		Route::post('/asset/barqrcode/setting/get', 'ApplicationSettingsController@getFieldsForBarcodeQRcode');
		// end application settings (Bar/QR Code field set).


		// Location adding
		Route::post('/location/add','ApplicationSettingsController@setLocationForCompany');
		Route::post('/location/show','ApplicationSettingsController@listLocationsByCompany');

				//->middleware('cmpauthenticate'); // * trying to implement it with frontend varification
		Route::get('/location/list/{company_id}','ApplicationSettingsController@listLocationsByCompanyWithoutPagination');
		Route::get('/location/delete/{location_id}/{company_id}','ApplicationSettingsController@deleteSingleLocation');
		Route::post('/location/update','ApplicationSettingsController@updateSingleLocation');
		// Location End

		// User type input
		Route::post('/user/type','ApplicationSettingsController@addUserTypeAccessForSubUsers');
		Route::get('/user/type/list/{company_id}','ApplicationSettingsController@listOfUserTypeAccess');
		Route::get('/usertype/list/{company_id}','ApplicationSettingsController@usertypeListOfUserTypeAccess');
		Route::post('/user/type/update','ApplicationSettingsController@updateUserTypeAccessForSubUsers');
		Route::post('/user/type/delete','ApplicationSettingsController@deleteUserTypeAccessForSubUsers');
		Route::get('/user/type/list/accesswise/{company_id}','ApplicationSettingsController@listOfUserTypeAccessWithViewAccess');
		Route::get('/user/type/list/accesswise/{company_id}/{user_type}','ApplicationSettingsController@listOfUserTypeAccessWithViewAccessUserType');
		Route::post('/user/type/single/record','ApplicationSettingsController@getUsertypeAccessSingleRecord');
		// User type input end

		// Asset Maintenance
		Route::post('/breakdown/ticket','BreakdownController@raiseMaintenanceAssetIssueTicket');
		Route::post('/breakdown/ticket/list','ReportController@getListofAssetMaintenanceTicketReport');
		Route::get('/asset/img/{fileName}','BreakdownController@getImageUrl');
		Route::post('/breakdown/ticket/update','BreakdownController@updateMaintenanceAssetIssueTicket');
		// Asset Maintenance End

		// Asset verification
		Route::post('/asset/verification/init','ProcessManagementController@initializeAssetVerification');
		Route::post('/asset/verification/list','ProcessManagementController@listOfActiveVerificationSchedule');
		Route::post('/asset/verification/check/status','ProcessManagementController@checkOngoingAssetVerificationStatus');
		Route::post('/asset/verification/status/update','ProcessManagementController@assetVerificationProcessStatusUpdate');
		Route::post('/asset/verification/end/status','ProcessManagementController@endAssetVerificationProcessStatus');
		Route::post('/asset/verification/counts','ProcessManagementController@totalAssetInVerificationQueue');
		// Asset verification End
		
		// Asset Discarded 
		Route::post('/asset/discard','ProcessManagementController@discardAssetByLocation');
		Route::post('/asset/discard/list','ReportController@getAssetDiscardedReport');
		
		// End Asset Discarded 
		// Master Customer Vendor Information
		Route::post('/user/vendor/','VendorController@addVendorDetails');
		Route::post('/user/vendor/list','VendorController@listOfVendors');
		Route::get('/user/vendors/{company_id}','VendorController@listOfVendorsWithoutPagination');
		Route::get('/user/vendor/info/{company_id}/{id}','VendorController@infoOfSingleVendor');
		Route::post('/user/vendor/update','VendorController@updateVendorInfo');
		Route::delete('/user/vendor/delete','VendorController@vendorDelete');
		// Master Customer Vendor Information End

		// Sub User creating 
		Route::group(['middleware' => ['subuserlimit']], function () {
			Route::post('/subusers/create','ApplicationSettingsController@subUserCreation');
		});
		
		Route::get('/subusers/list/{username}/{company_id}','ApplicationSettingsController@listOfSubUsers');
		Route::get('/subusers/list/loc/{location_id}/{username}/{company_id}','ApplicationSettingsController@listOfSubUsersLocationWise');
		Route::post('/subusers/update','ApplicationSettingsController@updateSubUsersInfo');
		// end Sub User creating

		// Reports
		Route::post('/report/asset/location','ReportController@locationWiseAssetReport');
		Route::post('/report/asset/verification','ReportController@assetVerificationReport');
		Route::post('/report/asset/each/verification','ReportController@getEachAssetVerificationReport');
		Route::post('/report/asset/movement','ReportController@assetMovementReport');
		// End Reports

		// Reports Export
		Route::post('/report/asset/location/export','ReportsExportController@exportLocationWiseAssetReport');
		Route::post('/location/list/export','ReportsExportController@exportLocationList');
/*		Route::get('/assettype/list/export','ReportsExportController@exportAssetTypeList');
*/		
		/*Route::post('/vendors/list/export','ReportsExportController@exportVendorsList');*/
		/*Route::get('/categories/list/export','ReportsExportController@exportCategoryList');*/
		/*Route::get('/depreciations/list/export','ReportsExportController@exportDepreciationList');*/
		Route::post('/subusers/list/export','ReportsExportController@exportSubUsersList');
		Route::post('/asset/movement/list/export','ReportsExportController@exportAssetMovementReport');
		Route::post('/asset/verification/list/export','ReportsExportController@exportEachAssetVerificationReport');
		Route::post('/asset/discarded/list/export','ReportsExportController@exportAssetDiscardedReport');
		Route::post('/asset/maintenance/list/export','ReportsExportController@exportListofAssetBreakdownTicketReport');
		Route::get('/bulk/asset/up/sample','ReportsExportController@bulkAssetUploadSampleFileDownload');
		// End Reports Export

		//Customer Profile
		Route::post('/customer/profile','UserController@singleCustomerInfo');
		Route::post('/customer/profile/update','UserController@updateCustomerProfileInfo');
		// Customer Profile end
		

		//System configuration

		Route::post('/sys/config/update','ApplicationSettingsController@systemConfiguration');
	


		Route::post('/sys/config/show','ApplicationSettingsController@getSystemConfigurationForCompany');
		// End System COnfiguration

		// User Help/Support
		Route::post('/support/ticket','SupportController@createTicketForUser');
		Route::post('/support/ticket/list','SupportController@getTicketsListForUser');
		Route::get('/support/attachment/link/{imgname}','SupportController@getSupportAttachmentFileLink');
		// End User Help/Support

		// Reset Password
		Route::post('/user/password/reset','UserController@resetCustomerPassword');
		Route::post('/subuser/password/reset','SubUserController@resetSubUserPassword');
		// End Reset Password 

		// Account update
		Route::post('/user/ft/login/update','UserController@updateCustomerFTimeLoginStatus');
		// End Account update


		// delete temp assets
		Route::post('/asset/temp/delete','AssetController@deleteSingleAssetFromTempTable');
		Route::post('/asset/temp/all/delete','AssetController@deleteAssetsFromTempTable');
		// End delete temp assets

		// Valuation 
		Route::post('/asset/valuation','AssetFinanceController@getGrossBlockData');

		// Valuation end

		/**/
		/*insurance*/
		Route::post('/insurance/create','InsuranceController@createNewInsurance');
		
		Route::post('/insurance/delete','InsuranceController@deleteInsurance');

		Route::post('/insurance/list','InsuranceController@insuranceList');
		Route::post('/insurance/edit','InsuranceController@editInsurance');
		
   
		Route::post('/insurance/document','InsuranceController@viewOrEditDoc');

		/*asset loggs*/
		Route::post('/asset/loggs','AssetController@assetLoggList');



        


	  }); // Middleware End


	/*auth:api end*/




/*});*/

/*expirydatecheck middleware end*/

	  	//forget password
		Route::post('/users/forget/password','UserController@forgetPasswordRecoveryCustomer');
		Route::post('/subusers/forget/password','UserController@forgetPasswordRecoverySubCustomer');
		Route::post('/subusers/forget/password/checking','UserController@forgetPasswordCheckingSubCustomer');
		Route::post('/users/forget/password/checking','UserController@forgetPasswordCheckingCustomer');

});   //Prefix End







// Admin Routes
Route::prefix('v1/admin')->group(function(){

	Route::post('/entry','AdminController@adminRegistration');
	Route::post('/login/service','AdminController@adminLogin');

	Route::get('/user/verify/{token}', 'UserController@verifyUser');

// Route::group(['middleware' => ['auth:api']], function () {

	// Customer Registration
	Route::post('/user/registration/','UserController@registerUser');
	// End Customer Registration
// Category 
	Route::post('/category/add','CategoriesController@addCategory');
	Route::get('/category/list/sub','CategoriesController@listOfCategoriesWithSubcategory');
	Route::get('/category/list/parent','CategoriesController@listOfCategoriesWithParent');
	Route::post('/category/list/single/parent','CategoriesController@listOfCategoriesWithSingleParent');
    Route::get('/category/list','CategoriesController@listOfCategories');
// Category End

// Menu 
	Route::post('/menu/add','MenuController@addMenuItems');
	Route::get('/menu/list','MenuController@menuItemList');
// Route::get('/menu/list/parent','CategoriesController@listOfCategoriesWithParent');
// Route::post('/menu/list/single/parent','CategoriesController@listOfCategoriesWithSingleParent');

// Menu End

// Customer 

	Route::get('/customer/list/','UserController@listOfRequestedCustomer');
	Route::post('/customer/single/info','UserController@singleCustomerInfo');
	Route::post('/customer/update/account-status','UserController@updateCustomerAccountStatus');

// Customer End

// Asset (Admin end need to complete ?)

	Route::post('/search/asset/','SearchController@searchAssetByAssetId');
	Route::post('/asset/id/list','SearchController@listAssetId');

// Asset End
	//   }); // Middleware End


	/*support ticket*/
	Route::post('/support/ticket/list','SupportController@customerTicketList');

});   //Prefix End



}); //CORS Validation end





